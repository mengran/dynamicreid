function debug_rankingshow(Itest, gIDtest,idx_gallery, rank)
%debug_rankingshow(Itest, gIDtest,idx_gallery, rank)
%   display all rank 1 results and worst 20% results
bestID = 1;
for i = 1:size(rank,1) %loop over all partitions
    figure(i)
    Igal = Itest(idx_gallery(i,:));
    gIDgal = gIDtest(idx_gallery(i,:));
    gIDprob = gIDtest(~idx_gallery(i,:));
    Iprob = Itest(~idx_gallery(i,:));

    subplot(2,1,1)
    showIDprob = gIDprob(rank(i,:) <= bestID);
    Ishow_prob = Iprob(rank(i,:) <= bestID);
    Ishow_gal = [];
    for p = 1:numel(showIDprob)
        Ishow_gal = [Ishow_gal,Igal(gIDgal==showIDprob(p))];
    end   
    imshow(detection_visual([Ishow_prob, Ishow_gal],numel(Ishow_prob),numel(Ishow_prob)*2));
    disp('gID of rank 1 resuluts:');
    disp(gIDprob(rank(i,:)==1));
    
    subplot(2,1,2)
    [~, showIDx] = sort(rank(i,:),'descend');
    showIDx = showIDx(1:round(size(rank,2)*0.2));
    showIDprob = gIDprob(showIDx);
    Ishow_prob = Iprob(showIDx);
    Ishow_gal = [];
    for p = 1:numel(showIDprob)
        Ishow_gal = [Ishow_gal,Igal(gIDgal==showIDprob(p))];
    end   
    imshow(detection_visual([Ishow_prob, Ishow_gal],numel(Ishow_prob),numel(Ishow_prob)*2));
    
    disp('gID of worst resuluts:');
    disp(showIDprob);
    title(sprintf('Partition %d',i));
    disp('>>>>>>>>>>>>>>>>>>>>>>>');
end

% for i = 1:size(rank,1) %loop over all partitions
%     figure(i+size(rank,1))
%     [~, showID] = sort(rank(i,:),'descend');
%     showID = showID(1:round(numel(gIDtest)*0.2));
%     imshow(detection_visual(Itest(showID),10,sum(showID)));
%     disp(gIDtest(rank(i,:)==1));
%     title(sprintf('Partition %d',i));
% end
