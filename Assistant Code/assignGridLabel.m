function [trj,label,labelmat] = assignGridLabel(trj, bbox, nr, nc)
% [label] = assignGridLabel(trj, bbox, nr, nc)
% Assign grid label for each trjectory based on the end point. Remove the
% ones outside the bounding boxes and translate all trajecotries s.t. the
% original point is the upper-left corner
% INPUT:
%         trj[n*(d+1)]---trajectory matrix, each row is a trajectory. 1st
%         dimension is frame number, followed by [x y x y ...]
%         bbox[nx5(4)]---bounding box for each frame. 1st dimension is
%         frame number
%         nr,nc---number of grids along row/col dimension
% OUTPUT:
%         label[nx1 cell]---each cell is a set of labels that trajecotry
%         belongs to
%         trj[n*(d+1)]---cleaned and translated trjactories
if size(bbox,2)<5 % bbox is fixed 
    ufnum = unique(trj(:,1));
    bbox = [ufnum,repmat(bbox,numel(ufnum),1)];
else % bbox varying from different frames
    ufnum = unique(bbox(:,1));    
end

bboxf = bbox(:,1);
trjf = trj(:,1);
label = cell(size(trj,1),1);
labelmat = [];
ID_val = [];
for f = 1:numel(ufnum)
    tmpf = ufnum(f);
    tmpBB = bbox(bboxf==tmpf,2:end);
    tmpid = find(trjf==tmpf);
    if isempty(tmpBB) || isempty(tmpid)
        continue;
    end
    tmpendP = trj(tmpid,end-1:end);
    % exclude the ones outside the bounding boxes
    tmpendP = round(bsxfun(@minus, tmpendP,tmpBB(1:2)));
    val_idx = tmpendP(:,1)>0 & tmpendP(:,2)>0 ...
        & tmpendP(:,1)<= tmpBB(3) & tmpendP(:,2)<= tmpBB(4);
    tmpendP = tmpendP(val_idx,:);
    tmpendP_idx = sub2ind(tmpBB(4:-1:3),tmpendP(:,2),tmpendP(:,1));
    
    [region_idx, ~, ~] = GenerateGridBBox_numP(tmpBB(4:-1:3), nr, nc);
    region_idx = repmat(region_idx,numel(tmpendP_idx),1);
    tmpendP_idx = kron(tmpendP_idx,ones(nr*nc,1));
    label_idx = cellfun(@ismember, mat2cell(tmpendP_idx,ones(1,numel(tmpendP_idx))), ...
        region_idx);
    label_idx = reshape(label_idx,nr*nc,[]);
    [idX,idY] = find(label_idx);
    tmplabel = mat2cell(idX,accumarray(idY,1,[size(label_idx,2),1]));
    label(tmpid(val_idx)) = tmplabel;
    labelmat = [labelmat; label_idx'];
    ID_val = [ID_val;tmpid(val_idx)];
    % calculate the relative coordinates
    trj(tmpid,2:2:end) = trj(tmpid,2:2:end) - tmpBB(1);
    trj(tmpid,3:2:end) = trj(tmpid,3:2:end) - tmpBB(2);
end
% val_idx = cellfun(@isempty, label);
% trj = single(trj(~val_idx,:));
% label = label(~val_idx);
trj = single(trj(ID_val,:));
label = label(ID_val);
labelmat = logical(labelmat);