function [ FV ] = FV_Wishart(G, w, sigma, Df )
%FV_WISHART Summary of this function goes here
%   Detailed explanation goes here
for k = 1:numel(sigma)
    [U,S,V] = svd(inv(sigma{k}));
    P{k} = U*sqrt(S)*V'; % inv(sigma) = P*P
end
d = size(G{1},1);
dimFV_k = d*(d+1)/2;
FV = zeros(numel(sigma)*dimFV_k,numel(G));
for n = 1:numel(G)    
    for k = 1:numel(sigma)
        mat_E(k) = w(k)*wishartPdf(G{n},sigma{k},Df);
    end
    mat_E = mat_E./sum(mat_E);
    tmpFV = zeros(numel(sigma)*dimFV_k,1);
    for k = 1:numel(sigma)
        tmpMat = mat_E(k)*(Df*pinv(P{k})-0.5*(P{k}*G{n}+G{n}*P{k}));
        tmpMat = triu(tmpMat);
        tmpFV((k-1)*dimFV_k+1:k*dimFV_k) = tmpMat(logical(triu(ones(d))));
    end
    FV(:,n) = tmpFV;
end
FV = sum(FV,2);
for k = 1:numel(sigma)
    FV((k-1)*dimFV_k+1:k*dimFV_k) = FV((k-1)*dimFV_k+1:k*dimFV_k)./sqrt(w(k));
end
    