function [w, sigma, Df, label] = EM_WpMM(G,num_c,Df)

% X: NXd 

% initialize
GVec = cellfun(@(x) x(:), G, 'UniformOutput', 0);
GVec = cell2mat(GVec);
[~,mat_E] = fcm(GVec',num_c,[nan,1000,nan,0]); % fazzy c-means inital
mat_E = mat_E';
logE = log(mat_E);
mat_E = bsxfun(@times, mat_E, 1./sum(mat_E,2));
E_v0 = sum(sum(mat_E.*logE));
w = sum(mat_E,1)./numel(G); % initial prior
% initial sigma
cell_E = mat2cell(mat_E,ones(1,size(mat_E,1)),ones(1,size(mat_E,2)));
for k = 1:num_c
    EG = cellfun(@(x,y) x*y,cell_E(:,k),G','UniformOutput',0);
    EG = cellfun(@(x) x(:), EG, 'UniformOutput',0);
    EG = cell2mat(EG');
    EGsum = reshape(sum(EG,2),sqrt(size(EG,1)),sqrt(size(EG,1)));
    term_norm = sum(mat_E(:,k).*Df,1);
    sigma{k} = EGsum/term_norm;
end    

iter = 1;
while iter <= 100
    % E step
    mat_E = zeros(numel(G),num_c);
    parfor i = 1:numel(G)
        for k = 1:num_c
            mat_E(i,k) = w(k)*wishartPdf(G{i},sigma{k},Df);
        end
    end
    mat_E = mat_E + realmin;
    logE = log(mat_E);
    mat_E = bsxfun(@times, mat_E, 1./sum(mat_E,2));
    E_v = sum(sum(mat_E.*logE));
    if abs(E_v - E_v0) < 1e-3 
        break;
    end
    fprintf('Iter %d; Expect value = %.3f\n',iter,E_v);
    % M step
    w = sum(mat_E,1)./numel(G); % update prior
    % update sigma
    cell_E = mat2cell(mat_E,ones(1,size(mat_E,1)),ones(1,size(mat_E,2)));
    for k = 1:num_c
        EG = cellfun(@(x,y) x*y,cell_E(:,k),G','UniformOutput',0);
        EG = cellfun(@(x) x(:), EG, 'UniformOutput',0);
        EG = cell2mat(EG');
        EGsum = reshape(sum(EG,2),sqrt(size(EG,1)),sqrt(size(EG,1)));
        term_norm = sum(mat_E(:,k).*Df,1);
        sigma{k} = EGsum/term_norm;
    end    
    iter = iter + 1;
    E_v0 = E_v;
end
[~,label] = max(mat_E,[],2);