% function pool_turner(mode)
function pool_turner
% A wrapper for matlabpool open & matlabpool close
% switch mode
%     case 'open'
        try 
            parpool;
        catch %#ok<CTCH>
            delete(gcp('nocreate'))
            parpool;
        end
%     case 'close'
%         try 
%             matlabpoo close
%         catch
%             ;
%         end
end