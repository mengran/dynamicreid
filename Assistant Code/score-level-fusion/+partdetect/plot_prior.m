function plot_prior(p,dims,root)
% Plot the Kinematic Prior
% Shows the body structure learnt fromt the training data.

import util.rototrasl util.plotcov2;
if nargin < 3, root = 1; end

% start with the vertex 1 centered at the origin
que = root;
cx(root) = 0;
cy(root) = 0;
theta(root) = 0;

while ~isempty(que)
  ii = que(1);
  que = que(2:end);
  
  % find the rectangle shape
  half = dims(ii,:)/2;
  x = [-half(1) half(1) half(1) -half(1) -half(1)];
  y = [-half(2) -half(2) half(2) half(2) -half(2)];
  
  % rotate the rectangle to angle theta and center it
  xy = rototrasl([x;y],cx(ii),cy(ii),theta(ii));
  
  plot(xy(1,:),xy(2,:),'k','LineWidth',2);
  hold on; axis ij equal; grid on;
  plot(cx(ii),cy(ii),'r+');
  text(cx(ii)+5,cy(ii),num2str(ii),'Color','red');
  
  % find the linked parts
  links = p.edges(p.edges(:,1)==ii,2);
  
  % process the links
  for jj = links'
    xij = p.xij(ii,jj);
    yij = p.yij(ii,jj);
    xji = -p.xji(ii,jj);
    yji = -p.yji(ii,jj);
    thij = p.theta_ij(ii,jj) *180/pi;
    
    rotxy = rototrasl([xij xij+xji;yij yij+yji],cx(ii),cy(ii),theta(ii));
    
    mx = rotxy(1,1);
    my = rotxy(2,1);
    cx(jj) = rotxy(1,2);
    cy(jj) = rotxy(2,2);
    theta(jj) = theta(ii) + thij;
    
    x = [cx(ii) mx cx(jj)];
    y = [cy(ii) my cy(jj)];
    plot(x,y,'b');
    plot(mx,my,'ro');
    plotcov2([mx,my],...
      [p.sigma2_x(ii,jj) 0;0 p.sigma2_y(ii,jj)],'conf',0.5);
  end

  que = [que links'];
end
