function IntHOG = calc_inthog(A,par)
%
% This function calculates the integral image of the gradient histogram
% image from the input image A.
% A must be of double class with values in [0,1].
%
% Copyright (C) 2013 Cheng Dong Seon
%
% PARTDETECT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% PARTDETECT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PARTDETECT.  If not, see <http://www.gnu.org/licenses/>.

%% Preliminaries
% check the input image size
[height,width,chans] = size(A);

% setup binning
numbins = par.Orientations;
bandwidth = pi / numbins;
bins = 0:bandwidth:pi;
dims = [height width numbins+1];

% setup convolution with FFT
filter = [1 2 3 4 3 2 1] / 16;
fdims = [height+6 width+6];
ly = fft(filter',fdims(1),1);
lx = fft(filter,fdims(2),2);
Ly = ly(:,ones(1,width,1),ones(1,1,numbins));
Lx = lx(ones(height,1,1),:,ones(1,1,numbins));

%% Gamma/Color Normalization
% perform square root gamma correction
% An = sqrt(A);
An = A;

%% Gradient Computation
% calculate the gradients by convolution with the 1D centered mask
% it seems imfilter crashes with type single
mask = [-1 0 1];
Dx = single(imfilter(double(An),mask,'replicate','same','conv'));
Dy = single(imfilter(double(An),mask','replicate','same','conv'));

% calculate gradient magnitude and orientation (as 0-180 degrees)
W = sqrt(Dx.^2 + Dy.^2);
O = atan(-Dx./Dy);
O = O + pi/2;
O(Dy == 0) = 0;

% in case of color images, for each pixel select the orientation of the
% channel with the highest magnitude
if chans > 1
  [Wc,maxchan] = max(W,[],3);
  Oc = zeros(height,width,'single');
  for cc = 1:chans
    select = maxchan == cc;
    Oc = O(:,:,cc) .* select + Oc .* (1-select);
  end
else
  Wc = W;
  Oc = O;
end

%% Orientation Binning with Interpolation
% generate each cell gradient votes using bilinear interpolation into the
% given number of orientation bins
H = zeros(dims,'single');

% given a pair (x,w) from a pixel at O,W
% find x1 in bins such that x1 is the biggest value with x1 <= x
xvec = reshape(bins,[1 1 numbins+1]);
xmat = xvec(ones(height,1,1),ones(1,width,1),:);
omat = Oc(:,:,ones(1,1,numbins+1));
fmat = (omat - xmat) / bandwidth;

% store the weights that do not need interpolation because they fall
% exactly in one bin
fzero = find(fmat == 0);
[i1,i2,~] = ind2sub(dims,fzero);
H(fzero) = Wc(sub2ind([height width],i1,i2));

% store the partial weights by interpolation into two bins
fpos = find(fmat > 0 & fmat < 1);
fval = fmat(fpos);
[i1,i2,i3] = ind2sub(dims,fpos);
Wf = Wc(sub2ind([height width],i1,i2));
H(fpos) = Wf .* (1 - fval);
fnext = sub2ind(dims,i1,i2,i3+1);
H(fnext) = Wf .* fval;

% check that everything went ok
% MM = sum(H,3);
% figure;imagesc(M - MM);

% collapse 0 and 180 degrees together
H(:,:,1) = H(:,:,1) + H(:,:,end);
H(:,:,end) = [];

%% Spatial Bilinear Interpolation
% use convolution to spread the histograms to neighboring pixels
Hy = real(ifft( fft(H,fdims(1),1) .* Ly ,[],1));
Hy = Hy(4:height+3,:,:);
Hxy = real(ifft( fft(Hy,fdims(2),2) .* Lx ,[],2));
Hxy = Hxy(:,4:width+3,:);

% add an extra border of zeros at the left and top
IntHOG = zeros(height+1,width+1,numbins,'single');
IntHOG(2:end,2:end,:) = cumsum(cumsum(Hxy,1),2);

% figure;
% subplot(131); imagesc(H(:,:,1));
% subplot(132); imagesc(Hxy(:,:,1));
% subplot(133); imagesc(cumsum(cumsum(Hxy(:,:,1),1),2));
