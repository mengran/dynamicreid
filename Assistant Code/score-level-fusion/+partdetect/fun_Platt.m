function res = fun_Platt(AB,LAB,SCORE)

A = AB(1);
B = AB(2);

Pi = 1 ./ (1 + exp(A * SCORE + B));
res = -sum(LAB .* log(Pi)) - sum((1-LAB) .* log(1-Pi));
