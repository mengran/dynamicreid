function conn = est_kinprior(GT,train,part_dims)
%
% Estimate the kinematic prior from the given ground truth data.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% PARTDETECT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% PARTDETECT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PARTDETECT.  If not, see <http://www.gnu.org/licenses/>.

%% Setup output
% The parts indices in my new numbering:
%      3
%   4  2  6
%  5   1   7
%     8 10
%    9   11
%
nParts = GT.Parts;
nTrain = 2*train.ImageNum;

% Initialize results storage
conn.xij = zeros(nParts); %(c_ij in the paper)
conn.yij = zeros(nParts);
conn.xji = zeros(nParts);
conn.yji = zeros(nParts);
conn.sigma2_x = zeros(nParts);
conn.sigma2_y = zeros(nParts);
conn.sigma2_s = zeros(nParts);
conn.theta_ij = zeros(nParts);
conn.k = zeros(nParts);


%% Main Algorithm
% % retrieve the foreshortening of the parts by comparing with the canonical
% % size for the parts
% ScaleWid = GT.W ./ repmat(part_dims(:,1)',nTrain,1);
% ScaleLen = GT.L ./ repmat(part_dims(:,2)',nTrain,1);
% 
% % as a rule, choose as the part scaling the value that is different from
% % one, since usually, all parts have one parameterized dimension (either
% % length or width) that will always have scale 1, and one free dimension
% freedim = ScaleWid == 1;
% S = ScaleLen .* freedim + ScaleWid .* (1-freedim);

S = GT.FS;

%% 6.2: Parameters estimation for each pair
for ii = 1:nParts-1
  for jj = ii+1:nParts
    % retrieve parts data
    thi = GT.R(:,ii); thj = GT.R(:,jj);
    si  = S(:,ii); sj  = S(:,jj);
    xi  = GT.X(:,ii); xj  = GT.X(:,jj);
    yi  = GT.Y(:,ii); yj  = GT.Y(:,jj);
    
    % estimate the standard deviation of the scale parameter
    conn.sigma2_s(ii,jj) = sum((si - sj).^2) / (nTrain-1);
    
    % estimate the von Mises distribution over rotation angles
    z = exp( 1i*(thi-thj) );
    zm = mean(z); zm_mod = abs(zm);
    conn.theta_ij(ii,jj) = -angle(zm);
    conn.k(ii,jj) = partdetect.circ_kappa(zm_mod);
    
    % estimate the location of the joint between ii and jj, specified
    % by the two points (xij,yij) and (xji,yji), one in the coordinate
    % frame of each part
    ax = [si.*cos(thi) -sj.*cos(thj) -si.*sin(thi) sj.*sin(thj)];
    bx = (xj-xi);
    ay = [si.*sin(thi) -sj.*sin(thj) si.*cos(thi) -sj.*cos(thj)];
    by = (yj-yi);
    A = [ax;ay]; b = [bx;by]; XY = A\b;
    conn.xij(ii,jj) = XY(1);
    conn.xji(ii,jj) = XY(2);
    conn.yij(ii,jj) = XY(3);
    conn.yji(ii,jj) = XY(4);
    
    % estimate the variance of joint location
    conn.sigma2_x(ii,jj) = sum((ax*XY-bx).^2)/(length(bx)-1);
    conn.sigma2_y(ii,jj) = sum((ay*XY-by).^2)/(length(by)-1);
    
    % apply a min variance
    conn.sigma2_x(ii,jj) = max(conn.sigma2_x(ii,jj),train.MinJointVar);
    conn.sigma2_y(ii,jj) = max(conn.sigma2_y(ii,jj),train.MinJointVar);
    
    conn.sigma2_s(jj,ii) = conn.sigma2_s(ii,jj);
    conn.theta_ij(jj,ii) = -conn.theta_ij(ii,jj);
    conn.k(jj,ii) = conn.k(ii,jj);
    conn.xij(jj,ii) = conn.xji(ii,jj);
    conn.xji(jj,ii) = conn.xij(ii,jj);
    conn.yij(jj,ii) = conn.yji(ii,jj);
    conn.yji(jj,ii) = conn.yij(ii,jj);
    conn.sigma2_x(jj,ii) = conn.sigma2_x(ii,jj);
    conn.sigma2_y(jj,ii) = conn.sigma2_y(ii,jj);
    
    % plot the distribution of joint locations
%     figure(102); hold off;
%     scatter(ax*XY-bx,ay*XY-by,'kx'); hold on; axis equal;
%     axis([-20 20 -20 20]); grid on;
%     plotcov2([0,0],[sqrt(conn.sigma2_x(ii,jj)) 0;0 sqrt(conn.sigma2_y(ii,jj))]);
%     title(['joint ' num2str(ii) ' to ' num2str(jj)]);
%     1;
%     pause;
  end
end

% CHANGELOG
% 24 Feb 2013: Changed the way foreshortening is computed, now it correctly
%              takes into account foreshortening wrt the canonical size
