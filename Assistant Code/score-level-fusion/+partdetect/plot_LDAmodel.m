function plot_LDAmodel(model,cells,par)
import partdetect.plot_rhog;

nParts = length(model);
vlen = par.Orientations * 4;
for pp = 1:nParts
  % show the HOGs for the part
  util.bigsubplot(5,nParts,1,pp);
  ppHOG = reshape(model(pp).MUpart,[vlen cells(pp,2) cells(pp,1)]);
  plot_rhog(ppHOG,par,13);
  axis equal off;
  
  % show the HOGs for the part background
  util.bigsubplot(5,nParts,2,pp);
  bgHOG = reshape(model(pp).MUbase,[vlen cells(pp,2) cells(pp,1)]);
  plot_rhog(bgHOG,par,13);
  axis equal off;

  % show the positive factors in the difference between part and bg
  util.bigsubplot(5,nParts,3,pp);
  ddHOG = ppHOG - bgHOG;
  ddHOG = ddHOG .* (ddHOG > 0);
  plot_rhog(ddHOG,par,13);
  axis equal off;

  % show the positive weights
  util.bigsubplot(5,nParts,4,pp);
  W = model(pp).W .* (model(pp).W > 0);
  RH = reshape(W,[vlen cells(pp,2) cells(pp,1)]);
  plot_rhog(RH,par,13);
  axis equal off;

  % show the negative weights
  util.bigsubplot(5,nParts,5,pp);
  W = -model(pp).W .* (model(pp).W < 0);
  RH = reshape(W,[vlen cells(pp,2) cells(pp,1)]);
  plot_rhog(RH,par,13);
  axis equal off;

end
colormap gray;

% iSIGMA = inv(SIGMA);
% for kk = 1:cells(pp,1)*cells(pp,2)
%   yy((kk-1)*72+1:kk*72) = iSIGMA * xxxx((kk-1)*72+1:kk*72);
% end
% 
% figure;plot(yy);
% RH = reshape(yy,[vlen cells(pp,2) cells(pp,1)]);
% util.bigsubplot(5,nParts,5,pp);
% plot_rhog(RH,par,13);
% axis equal off;
