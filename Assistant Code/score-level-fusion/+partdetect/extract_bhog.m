function [B,bhei,bwid] = extract_bhog(I,par)
%
% This function extracts the HOG feature vectors for all blocks in the
% given image.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% PARTDETECT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% PARTDETECT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PARTDETECT.  If not, see <http://www.gnu.org/licenses/>.

import partdetect.*;

%% Preliminaries
% retrieve some info
[imheight,imwidth,~] = size(I); % image size
nBins = par.Orientations;

csize = par.CellSize; % HOG cell size
chalf = (csize - 1) / 2;

% resize the cell size based on scale
scale = par.Scale;
chalf = round(chalf * scale);
csize = 2 * chalf + 1;


%% Retrieve the HOG integral image
% calculate the HOG integral image
H = permute(calc_inthog(I,par),[3 1 2]);

  
%% Retrieve the vectors for each cell
% divide the entire image in 1-pixel overlapping cells
r1grid = 1:(csize-1):(imheight-csize);
r2grid = r1grid + csize;
c1grid = 1:(csize-1):(imwidth-csize);
c2grid = c1grid + csize;
nrows = length(r1grid);
ncols = length(c1grid);

% calculate the cell sums using the integral image trick for all
% detection windows in the entire image
C = H(:,r2grid,c2grid) + H(:,r1grid,c1grid) ...
  - H(:,r1grid,c2grid) - H(:,r2grid,c1grid);
CV = reshape(C,[nBins nrows*ncols]);

%% Retrieve the vectors for each block
vlen = nBins*4;
bhei = nrows - 1;
bwid = ncols - 1;
B = zeros(vlen,bhei*bwid,'single');

if strcmp(par.BlockNorm,'L1-sqrt')
  % calculate the features for each 2x2 block of cells, normalize the vectors
  % by using L1-sqrt. Sometimes the values calculated from the integral image
  % will be negative because of rounding errors, so use abs.
  for row = 1:bhei
    for col = 1:bwid
      bb = ([col col col+1 col+1] - 1) * nrows + [row row+1 row row+1];
      vset = abs(reshape(CV(:,bb),[vlen 1]));
      vnorm = sum(vset,1) + par.Epsilon;
      r1 = (col-1) * bhei + row;
      B(:,r1) = sqrt(vset ./ vnorm);
    end
  end
elseif strcmp(par.BlockNorm,'L2-hys')
  % L2-Hys normalization
  for row = 1:bhei
    for col = 1:bwid
      bb = ([col col col+1 col+1] - 1) * nrows + [row row+1 row row+1];
      vset = abs(reshape(CV(:,bb),[vlen 1]));
      vnorm = sqrt(sum(vset.^2,1) + par.Epsilon);
      r1 = (col-1) * bhei + row;
      B(:,r1) = min(vset ./ vnorm,par.Cutoff);
    end
  end
end
