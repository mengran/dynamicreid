function dist = MSCRmatch_SvsS_F(Blobs,gamma,ped)
%% MSCR distances computation

numImg = length(Blobs);
if nargin ==3
    % retrieve the list of probes and galleries
    probes = [];
    gallery = [];
    for ii = 1:length(ped)
        probes = [probes ped(ii).run(ped(ii).probes)];
        gallery = [gallery ped(ii).run(ped(ii).gallery)];
    end
else
%     warning('This only works for VIPeR');
%     probes=1:2:numImg;
%     gallery=2:2:numImg;
    warning('This only works for SvsM');
    probes=1:1:numImg;
    gallery=1:1:numImg;

end

hh = waitbar(0,'Retrieving MSCR features...');
reg=makecform('srgb2lab');
for i=1:numImg
    
    %%---- MSE analysis
    mser    =   Blobs(i);
    A = permute(mser.pvec, [3,2,1]);
    C = applycform(A, reg);
    colour = permute(C,[3,2,1]);

    dataf(i).Mmvec=mser.mvec;
    dataf(i).Mpvec=colour;

    waitbar(i/numImg,hh);
end
close(hh);


final_dist_y = zeros(numImg);
final_dist_color = zeros(numImg);

hh = waitbar(0,'Distances computation MSCR...');
for i = probes
	
    y_i = dataf(i).Mmvec(3,:);  % Mmvec{1}
    C_i = dataf(i).Mpvec; % Mpvec{1}
    if length(y_i)==1;
        y_i=[y_i y_i];
        C_i =[C_i C_i ];
    end
    num_i = size(y_i,2);

    for j = gallery

        y_j = dataf(j).Mmvec(3,:); % Mmvec{2}
        C_j = dataf(j).Mpvec; % Mpvec{2}
        if length(y_j)==1;
                y_j=[y_j y_j];
                C_j =[C_j C_j ];
        end
        num_j = size(y_j,2);
        max_info = max(num_i,num_j);

        % compute the distances from every blob in i to every
        % blob in j (faster)
        dist_y = abs( y_j(ones(num_i,1),:)' - y_i(ones(num_j,1),:) );
        Sxx = sum(C_j.^2,1);
        Syy = sum(C_i.^2,1);
        Sxy = C_j' * C_i;
        dist_color = sqrt( Sxx(ones(num_i,1),:)' + Syy(ones(num_j,1),:) - 2*Sxy);
        % dist matrices are in size num_j X num_i
        
        % transpose matrices, if needed, to get the biggest size as columns
        if num_j > num_i
            dist_y = dist_y';
            dist_color = dist_color';
        end
        
        %% check outliers
        % reject row-blobs that violate X25 rule 
        ref_y = min(dist_y); me_ref_y = mean(ref_y); std_ref_y = std(ref_y);
        ref_color = min(dist_color); me_ref_color = mean(ref_color); std_ref_color = std(ref_color);
        good = find((ref_y<(me_ref_y+3.5*std_ref_y))&(ref_color<(me_ref_color+3.5*std_ref_color)));

        if isempty(good)
            good = 1:max_info;
        end

        max_useful_info = length(good);
        dist_y2 = dist_y(:,good);
        dist_color2 = dist_color(:,good);

        %%normalize
        dist_y_n = dist_y./max(dist_y2(:));
        dist_color_n = dist_color./max(dist_color2(:));
        
        %%distance computation
        totdist_n = ( gamma*dist_y_n(:,good)  + (1-gamma)*dist_color_n(:,good) ) ;

        %%Minimization
        [min_dist,matching] = min(totdist_n);

        useful_i = sub2ind(size(totdist_n),matching',(1:max_useful_info)');
        final_dist_y(i,j)  = sum(dist_y2(useful_i))/max_useful_info;
        final_dist_color(i,j) = sum(dist_color2(useful_i))/max_useful_info;

        final_dist_y(j,i)  = final_dist_y(i,j);
        final_dist_color(j,i) = final_dist_color(i,j);
    end
    waitbar(i*j/numImg^2,hh);
    clear dist_y dist_color	dist_color_n dist_y_n

end
close(hh);

dist.MSCR_dist_y = final_dist_y;
dist.MSCR_dist_color = final_dist_color;

for i = probes
	dist.MSCR_dist_y(i,:)       = final_dist_y(i,:)./max(final_dist_y(i,gallery));
	dist.MSCR_dist_color(i,:)   = final_dist_color(i,:)./max(final_dist_color(i,gallery));    
end