function SDALF_DISTs = MSCRmatch_MvsX(Blobs,gamma)
%% MSCR distances computation


hh = waitbar(0,'Retrieving MSCR features...');

reg=makecform('srgb2lab');

for i=1:length(Blobs)
    
    %%---- MSE analysis
        mser    =   Blobs(i); 
        A = permute(mser.pvec, [3,2,1]);
        C = applycform(A, reg);
        colour = permute(C,[3,2,1]);
                        
        dataf(i).Mmvec=mser.mvec;
        dataf(i).Mpvec=colour;  

        waitbar(i/length(Blobs),hh);
end
close(hh);



%% Compute the distances between images
lenDATAF = length(dataf);
final_dist_y = 1000 - 1000*eye(lenDATAF); % init as 0 on diag and 1 offdiag
final_dist_color = 1000 - 1000*eye(lenDATAF);

blobPos = cell(2,1);
blobCol = cell(2,1);

hh = waitbar(0,'Calculating MSCR distances...'); lenDATAF2 = lenDATAF^2;
for i = 1:2:lenDATAF
    
    blobPos{1} = dataf(i).Mmvec; % get position of blobs
    blobCol{1} = dataf(i).Mpvec; % get colors of blobs
    
    num1 = size(blobPos{1},2); % get number of blobs
    
   
    dist_y_n	= cell(lenDATAF*2,1);
    dist_color_n= cell(lenDATAF*2,1);
    
    for j = 2:2:lenDATAF
        blobPos{2} = dataf(j).Mmvec; % get position of blobs
        blobCol{2} = dataf(j).Mpvec; % get colors of blobs
        num2 = size(blobPos{2},2); % get number of blobs
        
        % compute the distances from every blob in i to every
        % blob in j
        dist_y = abs( blobPos{1}(2*ones(num2,1),:)' - blobPos{2}(2*ones(num1,1),:) );
        Sxx = sum(blobCol{1}.^2,1);
        Syy = sum(blobCol{2}.^2,1);
        Sxy = blobCol{1}' * blobCol{2};
        dist_color = sqrt( Sxx(ones(num2,1),:)' + Syy(ones(num1,1),:) - 2*Sxy);
        

                
        % remove outliers: j blobs whose min distance from i blobs
        % is too high
        ref_y = min(dist_y); me_ref_y = mean(ref_y); std_ref_y = std(ref_y);
        ref_color = min(dist_color); me_ref_color = mean(ref_color); std_ref_color = std(ref_color);
        good = find((ref_y<(me_ref_y+3.5*std_ref_y))&(ref_color<(me_ref_color+3.5*std_ref_color)));
        
        max_useful_info = length(good);
        dist_y2 = dist_y(:,good);
        dist_color2 = dist_color(:,good);
        
        %%normalize
        dist_y_n{j} = dist_y./max(dist_y2(:));
        dist_color_n{j} = dist_color./max(dist_color2(:));
        
        %%distance computation
        totdist_n = ( gamma*dist_y_n{j}(:,good)  + (1-gamma)*dist_color_n{j}(:,good) ) ;
        
        %%Minimization
        [unused,matching] = min(totdist_n);
        
        useful_i = sub2ind(size(totdist_n),matching',(1:max_useful_info)');
        final_dist_y(i,j)  = sum(dist_y2(useful_i))/max_useful_info;
        final_dist_color(i,j) = sum(dist_color2(useful_i))/max_useful_info;
    end
    waitbar(i*j/lenDATAF2,hh);
    clear dist_y dist_color dist_color_n dist_y_n;
    
end
close(hh);

% normalize each row of final_dist_y and final_dist_color by their max,
% avoiding to account for the initial 1000 values for unused cells
maskused = zeros(lenDATAF,lenDATAF); maskused(1:2:lenDATAF,2:2:lenDATAF) = 1;
maxvec = max(final_dist_y .* maskused,[],2);
SDALF_DISTs.MSCR_dist_y = final_dist_y ./ maxvec(:,ones(1,lenDATAF));
SDALF_DISTs.MSCR_dist_y(SDALF_DISTs.MSCR_dist_y>1) = 1;
maxvec = max(final_dist_color .* maskused,[],2);
SDALF_DISTs.MSCR_dist_color = final_dist_color ./ maxvec(:,ones(1,lenDATAF));
SDALF_DISTs.MSCR_dist_color(SDALF_DISTs.MSCR_dist_color>1) = 1;
