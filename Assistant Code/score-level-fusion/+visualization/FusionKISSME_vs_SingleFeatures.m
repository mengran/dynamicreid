%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%
KISSMEconcat = load('+visualization/CMC_KISSME_ConcatFeat.mat','cmc','nAUC');
KISSMEfused = load('../results/VIPeR_Exp001/transformedTablesML/LearnTest_VIPeR_FAR_KISSME.mat', 'cmc','AUC');
KLFDAconcat = load('+visualization/CMC_kLFDA_ConcatFeat.mat','cmc','nAUC');
LikelihoodRatio = load('../results/VIPeR_Exp001/transformedTablesFinal/LearnTest_VIPeR_LikelihoodRatio.mat','cmc','AUC');
% Tables_VIPeR_KISSME = util.loadFromFile('../results/VIPeR_Exp001/matching/Tables_VIPeR_KISSME.mat','Tables_VIPeR');
KISSMEpure = load('../results/VIPeR_Exp001/transformedTables_KISSME_pure/LearnTest_VIPeR_FAR_KISSME_pure.mat', 'cmc','AUC');

%%
figure
hold on;
hKISSMEfused = plot(KISSMEfused.cmc*100,'-','LineWidth',2,'Color',[0,204,0]/255);
hKLFDAconcat = plot(KLFDAconcat.cmc*100,'-','LineWidth',2,'Color',[204,20,0]/255);
hLRFus = plot(LikelihoodRatio.cmc*100,'--','LineWidth',2,'Color',[0,204,0]/255);
hKISSMEconcat = plot(KISSMEconcat.cmc*100,'b-','LineWidth',2);
hKISSMEpure = plot(KISSMEpure.cmc*100,'b-.','LineWidth',2);
axis([1 50 0 100])
grid on

xlabel('Rank');
ylabel('Recognition Percentage');
title('Cumulative Match Characteristic - VIPeR (p=316)');

[~, test] = util.getPartitioningSDALF('datasets/VIPeRa', 10, 316, '.bmp');

%%
for i=1:8
    nRuns = size(Tables_VIPeR_KISSME,1);
    for run = 1:nRuns
        evalTables{run} = Tables_VIPeR_KISSME{run,i}(test(run,:),test(run,:));
    end
    cmc = util.benchmarking2('datasets/VIPeRa', test, evalTables, '.bmp',0);
    hMLsingle = plot(100*cmc,'Color',[168 168 255]/255,'LineWidth',1);
end
for i=9:12
    nRuns = size(Tables_VIPeR_KISSME,1);
    for run = 1:nRuns
        evalTables{run} = Tables_VIPeR_KISSME{run,i}(test(run,:),test(run,:));
    end
    cmc = util.benchmarking2('datasets/VIPeRa', test, evalTables, '.bmp',0);
    hADDsingle = plot(100*cmc,'Color',[168 168 168]/255,'LineWidth',1);
end
%%
legend([hKISSMEfused,hKISSMEpure,hKLFDAconcat,hLRFus,hKISSMEconcat,hMLsingle,hADDsingle],...
    ['KISSME + add. feat. + Fusion (FAR, PROPER) [' num2str(KISSMEfused.AUC,'%4.3f') ']'],...
    ['KISSME + Fusion (FAR, PROPER) [' num2str(KISSMEpure.AUC,'%4.3f') ']'],...
    ['kLFDA on concatenated features [' num2str(KLFDAconcat.nAUC,'%4.3f') ']'],...
    ['Fusion (LR, PROPER) on 84 features w/o ML [' num2str(LikelihoodRatio.AUC,'%4.3f') ']'],...
    ['KISSME on concatenated features [' num2str(KISSMEconcat.nAUC,'%4.3f') ']'],...
    'Single features with KISSME',...
    'Additional non-ML-features',...
    'Location','SouthEast')
plot(KISSMEconcat.cmc*100,'b-','LineWidth',2)

%%
xSize = 20; ySize = 19;
% set(gcf,'PaperPositionMode','auto')
paperSizeX = 18;
paperSizeY = 18;
set(gcf,'PaperSize',[paperSizeX paperSizeY]);
xLeft = (paperSizeX-xSize)/2; yTop = (paperSizeY-ySize)/2;
set(gcf,'PaperUnits','centimeters');
set(gcf,'PaperPosition',[xLeft yTop xSize ySize])
% Screen position and size
X = 1100;
Y = 100;
set(gcf,'Position',[X Y xSize*50 ySize*50])
% print(['../images/KISSME_kLFDA_FUSION.pdf'],'-dpdf');
% figure,imshow(imread('../images/LinearRegression.png'))