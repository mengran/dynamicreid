%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialization
% visualization.Init;

%% get genuine and imposter scores
genuineMask = mask;
realGenuines = genuineMask;
realGenuines(eye(size(mask,1)) == 1) = 0;

genuinesMSCR_raw = tables{1}(realGenuines);
genuinesMSCR_raw = genuinesMSCR_raw(:)';
impostersMSCR_raw = tables{1}(~genuineMask);
impostersMSCR_raw = impostersMSCR_raw(:)';

genuinesWCH_raw  = tables{2}(realGenuines);
genuinesWCH_raw  = genuinesWCH_raw(:)';
impostersWCH_raw  = tables{2}(~genuineMask);
impostersWCH_raw  = impostersWCH_raw(:)';

%% perform kernel density estimation
nSupportingPoints = 151;

supportingPointsWCH = linspace(0,max(impostersWCH_raw(:)),nSupportingPoints);
kdeImpostersWCH = fusion.KDE(impostersWCH_raw,supportingPointsWCH);
kdeGenuinesWCH = fusion.KDE(genuinesWCH_raw,supportingPointsWCH);
mxWCH = max([kdeImpostersWCH(:); kdeGenuinesWCH(:)]);

supportingPointsMSCR = linspace(0,max(impostersMSCR_raw(:)),nSupportingPoints);
kdeImpostersMSCR = fusion.KDE(impostersMSCR_raw,supportingPointsMSCR);
kdeGenuinesMSCR = fusion.KDE(genuinesMSCR_raw,supportingPointsMSCR);
mxMSCR = max([kdeImpostersMSCR(:); kdeGenuinesMSCR(:)]);

%% get lookup-tables (Likelihood Ratio)
lookupMSCR = parametersPLR{1}{1};
lookupWCH = parametersPLR{1}{2};

%% get function Logistic Regression
aMSCR = parametersLR{1}(1,1);
bMSCR = parametersLR{1}(2,1);
matchingRatioMSCR = exp(aMSCR * supportingPointsMSCR + bMSCR);
transformationFunctionMSCR = 1 - matchingRatioMSCR ./ (matchingRatioMSCR + 1);

aWCH = parametersLR{1}(1,2);
bWCH = parametersLR{1}(2,2);
matchingRatioWCH = exp(aWCH * supportingPointsWCH + bWCH);
transformationFunctionWCH = 1 - matchingRatioWCH ./ (matchingRatioWCH + 1);

matchRatioLR = @(x,a,b) exp(a * x + b);
normScoreLR = @(x,a,b) 1 - matchRatioLR(x,a,b) ./ (matchRatioLR(x,a,b) + 1);

%% score of person 1 img 1 <-> person 1 img 2
scoreMSCR = genuinesMSCR_raw(1,1);
scoreNormMSCR = normScoreLR(scoreMSCR,aMSCR,bMSCR);
scoreWCH = genuinesWCH_raw(1,1);
scoreNormWCH = normScoreLR(scoreWCH,aWCH,bWCH);

%% plot for wHSV
close all
figure
hold on;
% plot genuines/imposters/overlap
area(supportingPointsWCH,kdeImpostersWCH./mxWCH,'FaceColor',[255 128 128]/255) % red - imposter
area(supportingPointsWCH,kdeGenuinesWCH./mxWCH,'FaceColor',[128 255 128]/255) % green - genuines
area(supportingPointsWCH,min(kdeGenuinesWCH,kdeImpostersWCH)./mxWCH,'FaceColor',[255 185 149]/255) % orange - overlap
% plot(supportingPointsWCH,kdeGenuinesWCH./(kdeGenuinesWCH+kdeImpostersWCH),'b','LineWidth',2)
% plot transformation function
plot(supportingPointsWCH,transformationFunctionWCH,'b','LineWidth',3);
% mark selected score
scatter(scoreWCH,scoreNormWCH,50,'filled','ko','LineWidth',2)
% dashed lines to axis
plot([0 scoreWCH],[scoreNormWCH scoreNormWCH],'k--','LineWidth',1.5)
plot([scoreWCH scoreWCH],[0 scoreNormWCH],'k--','LineWidth',1.5)
xlabel('s_i');
ylabel('s_i^{norm}');
print([imageFolder 'GenImpTrans_wHSV.png'],'-dpng');

% plot for MSCR
figure
hold on;
% plot genuines/imposters/overlap
area(supportingPointsMSCR,kdeImpostersMSCR./mxMSCR,'FaceColor',[255 128 128]/255) % red - imposter
area(supportingPointsMSCR,kdeGenuinesMSCR./mxMSCR,'FaceColor',[128 255 128]/255) % green - genuines
area(supportingPointsMSCR,min(kdeGenuinesMSCR,kdeImpostersMSCR)./mxMSCR,'FaceColor',[255 185 149]/255) % orange - overlap
% plot(supportingPointsMSCR,kdeGenuinesMSCR./(kdeGenuinesMSCR+kdeImpostersMSCR),'b','LineWidth',2)
% plot transformation function
plot(supportingPointsMSCR,transformationFunctionMSCR,'b','LineWidth',3);
% mark selected score
scatter(scoreMSCR,scoreNormMSCR,50,'filled','ko','LineWidth',2)
% dashed lines to axis
plot([0 scoreMSCR],[scoreNormMSCR scoreNormMSCR],'k--','LineWidth',1.5)
plot([scoreMSCR scoreMSCR],[0 scoreNormMSCR],'k--','LineWidth',1.5)
xlabel('s_j');
ylabel('s_j^{norm}');
print([imageFolder 'GenImpTrans_MSCR.png'],'-dpng');
