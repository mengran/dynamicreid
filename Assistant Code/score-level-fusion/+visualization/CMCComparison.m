function legendEntries = CMCComparison(dataset,expNum,suffix)
%CMCCOMPARISON Wrapper to plot CMCs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 2
    suffix = '';
end
% dataset = 'VIPeR';
% suffix = 'Final';
% close all

%     hold on;
folder = ['../results/' dataset '_Exp' expNum '/transformedTables' suffix '/'];
fnames = dir([folder 'LearnTest_*']);

order = [4 8 6 7 3 1 2 5];

C = [[  0,   0, 255]; % Decimal
     [  0,   0,  44]; % Double Sigmoid
     [255,  20, 147]; % FAR
     [  0, 204,   0]; % Likelihood Ratio
     [255, 150,   0]; % Logistic Regression
     [  0, 235, 235]; % Min-Max
     [255,   0,   0]; % Mu-Sigma
     [  0,   0, 255]] / 255; % Tanh
 
% C = colormap(colorcube(length(fnames)+1));
% C(1,:) = [  0,   0, 255]/255;

C  = C(order,:);
 
style = {{'--','LineWidth',2}; % Decimal
         {'-','LineWidth',2}; % Double Sigmoid
         {'-','LineWidth',2}; % FAR
         {'-','LineWidth',2}; % Likelihood Ratio
         {'-','LineWidth',2}; % Logistic Regression
         {'-','LineWidth',2}; % Min-Max
         {'--','LineWidth',2}; % Mu-Sigma 
         {'-','LineWidth',2}};% Tanh

style = style(order);

fnames = {fnames(order).name};

%% plot all cmcs
figure
% legendEntries = cell(0,0);
results = cell(length(fnames),1);
for i=1:length(fnames)
    results{i} = load([folder fnames{i}],'cmc','AUC');
    plot(results{i}.cmc*100,style{i}{:},'Color',C(i,:));
    hold on;
%     nameParts = strsplit(fnames{i},'_');
%     legendEntries = [legendEntries; nameParts{3}(1:end-4)];
end
legendEntries = {['Likelihood Ratio [' num2str(results{1}.AUC,'%4.3f') ']'],...
                 ['Tanh Estimators [' num2str(results{2}.AUC,'%4.3f') ']'],...
                 ['Z-Score [' num2str(results{4}.AUC,'%4.3f') ']'],...
                 ['Min-Max [' num2str(results{3}.AUC,'%4.3f') ']'],...
                 ['False Acceptance Rate [' num2str(results{5}.AUC,'%4.3f') ']'],...
                 ['Decimal Scaling [' num2str(results{6}.AUC,'%4.3f') ']'],...
                 ['Double Sigmoid [' num2str(results{7}.AUC,'%4.3f') ']'],...
                 ['Logistic Regression [' num2str(results{8}.AUC,'%4.3f') ']']};

AUCs = zeros(1,length(results));
for i=1:length(results)
    AUCs(i) = results{i}.AUC;
end
% [~,idx] = sort(AUCs,'descend');
% legendEntries = legendEntries(idx);
% legend(legendEntries,'Location','SouthEast')
grid on
axis([1 25 10 90])
xlabel('Rank');
ylabel('Recognition Percentage');

% C(4,:) = [185, 185, 185] / 255;
% 
% %% plot PDF based transformations
% figure
% legendEntrys = cell(0,0);
% for i=[4 3 5]
%     plot(results{i}.cmc*100,style{i}{:},'Color',C(i,:));
%     hold on;
%     nameParts = strsplit(fnames(i).name,'_');
%     legendEntrys = [legendEntrys; nameParts{3}(1:end-4)];
% end
% legend(legendEntrys,'Location','SouthEast')
% grid on
% axis([1 50 20 90])
% xlabel('Rank');
% ylabel('Recognition Percentage');
% title('Cumulative Match Characteristics - PDF-Based Transformations (CMC)');
% 
% %% plot non-linear transformations
% figure
% legendEntrys = cell(0,0);
% for i=[4 2 8]
%     plot(results{i}.cmc*100,style{i}{:},'Color',C(i,:));
%     hold on;
%     nameParts = strsplit(fnames(i).name,'_');
%     legendEntrys = [legendEntrys; nameParts{3}(1:end-4)];
% end
% legend(legendEntrys,'Location','SouthEast')
% grid on
% axis([1 50 20 90])
% xlabel('Rank');
% ylabel('Recognition Percentage');
% title('Cumulative Match Characteristics - Non-Linear Transformations (CMC)');
% 
% %% plot linear transformations
% figure
% legendEntrys = cell(0,0);
% for i=[4 1 6 7]
%     plot(results{i}.cmc*100,style{i}{:},'Color',C(i,:));
%     hold on;
%     nameParts = strsplit(fnames(i).name,'_');
%     legendEntrys = [legendEntrys; nameParts{3}(1:end-4)];
% end
% legend(legendEntrys,'Location','SouthEast')
% grid on
% axis([1 50 20 90])
% xlabel('Rank');
% ylabel('Recognition Percentage');
% title('Cumulative Match Characteristics - Linear Transformations (CMC)');

end