function dist = calc_distance_gc(dist,par,skip_parts)
% 
% Calculate the distances between the given Gray-Color histograms.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

% check if we need to skip calculating part distances, and displaying
% updates
if nargin < 3, skip_parts = false; end
skipDisplay = isfield(par,'NoDisplay') && par.NoDisplay;

avoid0 = eps;
weights = par.WeightParts;
nParts = length(weights);
normB = log(dist.BhattaNorm);

[nImages,histLen] = size(dist.X); % X should be nImages x histLen

partLen = histLen / nParts;
partVec = 1:partLen;

% weight the individual parts histograms before the global normalization
W = kron(weights,ones(nImages,partLen));
Fd = single(dist.X .* W);
normVec = avoid0 + sum(Fd,2);
normFd = Fd ./ normVec(:,ones(1,histLen));

switch par.DistMeasure
  case 'Hellinger'
    % calculate the Bhattacharyya distance between the histograms
    hists_i = sqrt(normFd);
    dist.F = 2*real(sqrt(1 - hists_i * hists_i'));

    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        hists_i = sqrt(dist.X(:,partVec+(pp-1)*partLen));
        dist.Fp(:,:,pp) = 2*real(sqrt(1 - hists_i * hists_i'));
      end
    end
    
  case 'Bhattacharyya'
    % calculate the Bhattacharyya distance between the histograms
    hists_i = sqrt(normFd);
    coeff = hists_i * hists_i';
    dist.F = -log(coeff);
    dist.F(coeff == 0) = 10;

    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        hists_i = sqrt(dist.X(:,partVec+(pp-1)*partLen));
        coeff = hists_i * hists_i';
        dmat = -log(coeff);
        dmat(coeff == 0) = 10;
        dist.Fp(:,:,pp) = dmat;
      end
    end

  case 'Bhattacharyya(A)'
    % calculate the Bhattacharyya distance between the histograms
    sqA = sqrt(dist.A);
    hists_i = sqrt(normFd);
    coeff = partmult(hists_i,sqA,nParts,partLen) * hists_i';
    dist.F = -log(coeff) + normB;
    dist.F(coeff == 0) = 10;

    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        hists_i = sqrt(dist.X(:,partVec+(pp-1)*partLen));
        coeff = (hists_i * sqA) * hists_i';
        dmat = -log(coeff) + normB;
        dmat(coeff == 0) = 10;
        dist.Fp(:,:,pp) = dmat;
      end
    end
    
  case 'Chi-Squared'
    % Chi-squared distance
    if ~skipDisplay, hwait = waitbar(0,'Computing Chi^2..'); end

    dist.F = zeros(nImages,nImages,'single');
    for ii = 1:nImages-1
      P = normFd(ii*ones(nImages-ii,1),:);
      Q = normFd(ii+1:nImages,:);
      Z = P + Q; 
      Z(Z == 0) = 1;
      D = (P - Q).^2  ./ Z;
      distvec = 0.5 * sum( D,2 );
      dist.F(ii,ii+1:end) = distvec;
      dist.F(ii+1:end,ii) = distvec;
      
      if ~skipDisplay, waitbar(ii/(nImages+nParts),hwait); end
    end
    
    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        for ii = 1:nImages-1
          P = Fp(ii*ones(nImages-ii,1),:);
          Q = Fp(ii+1:nImages,:);
          Z = P + Q;
          Z(Z == 0) = 1;
          D = (P - Q).^2 ./ Z;
          distvec = 0.5 * sum( D,2 );
          dist.Fp(ii,ii+1:end,pp) = distvec;
          dist.Fp(ii+1:end,ii,pp) = distvec;
        end
        if ~skipDisplay, waitbar((nImages+pp)/(nImages+nParts),hwait); end
      end
    end
    if ~skipDisplay, close(hwait); drawnow; end
    
  case 'QCN(I)'
    % Quadratic-Chi distance, QCN(I) version
    % vectorialized for speed
    if ~skipDisplay, hwait = waitbar(0,'Computing QCN(I)..'); end

    dist.F = zeros(nImages,nImages,'single');
    for ii = 1:nImages-1
      P = normFd(ii*ones(nImages-ii,1),:);
      Q = normFd(ii+1:nImages,:);
      Z = P + Q; 
      Z(Z == 0) = 1;
      D = (P - Q) ./ (Z.^0.9);
      distvec = sqrt(sum( D.^2,2 ));
      dist.F(ii,ii+1:end) = distvec;
      dist.F(ii+1:end,ii) = distvec;
      
      if ~skipDisplay, waitbar(ii/(nImages+nParts),hwait); end
    end
    
    if ~skip_parts
      % do the same on individual parts histograms
      %A = binSimilarity(partLen);
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        for ii = 1:nImages-1
          P = Fp(ii*ones(nImages-ii,1),:);
          Q = Fp(ii+1:nImages,:);
          Z = P + Q;
          Z(Z == 0) = 1;
          D = (P - Q) ./ (Z.^0.9);
          distvec = sqrt(sum( D.^2,2 ));
          dist.Fp(ii,ii+1:end,pp) = distvec;
          dist.Fp(ii+1:end,ii,pp) = distvec;
        end
        if ~skipDisplay, waitbar((nImages+pp)/(nImages+nParts),hwait); end
      end
    end
    if ~skipDisplay, close(hwait); drawnow; end
    
  case 'QCN'
    % Quadratic-Chi distance, QCN version with bin-sim matrix A
    % vectorialized for speed
    if ~skipDisplay, hwait = waitbar(0,'Computing QCN..'); end

    dist.F = zeros(nImages,nImages,'single');
    for ii = 1:nImages-1
      P = normFd(ii*ones(nImages-ii,1),:);
      Q = normFd(ii+1:nImages,:);
      Z = partmult((P + Q),dist.A,nParts,partLen);
      Z(Z == 0) = 1;
      D = (P - Q) ./ (Z.^0.9);
      distvec = sqrt( sum(partmult(D.^2,dist.A,nParts,partLen),2) );
      dist.F(ii,ii+1:end) = distvec;
      dist.F(ii+1:end,ii) = distvec;
      
      if ~skipDisplay, waitbar(ii/(nImages+nParts),hwait); end
    end
    
    if ~skip_parts
      % do the same on individual parts histograms
      %A = binSimilarity(partLen);
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        for ii = 1:nImages-1
          P = Fp(ii*ones(nImages-ii,1),:);
          Q = Fp(ii+1:nImages,:);
          Z = (P + Q) * dist.A;
          Z(Z == 0) = 1;
          D = (P - Q) ./ (Z.^0.9);
          distvec = sqrt( sum(D.^2 * dist.A,2) );
          dist.Fp(ii,ii+1:end,pp) = distvec;
          dist.Fp(ii+1:end,ii,pp) = distvec;
        end
        if ~skipDisplay, waitbar((nImages+pp)/(nImages+nParts),hwait); end
      end
    end
    if ~skipDisplay, close(hwait); drawnow; end
    
  case 'L2'
    % Euclidean distance
    PQ = normFd * normFd';
    P2 = diag(PQ);
    P2mat = P2(:,ones(1,nImages));
    D = P2mat + P2mat' - 2 * PQ;
    D(D < 0) = 0;
    dist.F = sqrt(D);
    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        PQ = Fp * Fp';
        P2 = diag(PQ);
        P2mat = P2(:,ones(1,nImages));
        D = P2mat + P2mat' - 2 * PQ;
        D(D < 0) = 0;
        dist.Fp(:,:,pp) = sqrt(D);
      end
    end
    
  case 'L1'
    % City block distance
    if ~skipDisplay, hwait = waitbar(0,'Computing L1..'); end

    dist.F = zeros(nImages,nImages,'single');
    for ii = 1:nImages-1
      P = normFd(ii*ones(nImages-ii,1),:);
      Q = normFd(ii+1:nImages,:);
      distvec = sum(abs(P - Q),2);
      dist.F(ii,ii+1:end) = distvec;
      dist.F(ii+1:end,ii) = distvec;
      
      if ~skipDisplay, waitbar(ii/(nImages+nParts),hwait); end
    end
    
    if ~skip_parts
      % do the same on individual parts histograms
      %A = binSimilarity(partLen);
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        for ii = 1:nImages-1
          P = Fp(ii*ones(nImages-ii,1),:);
          Q = Fp(ii+1:nImages,:);
          distvec = sum(abs(P - Q),2);
          dist.Fp(ii,ii+1:end,pp) = distvec;
          dist.Fp(ii+1:end,ii,pp) = distvec;
        end
        if ~skipDisplay, waitbar((nImages+pp)/(nImages+nParts),hwait); end
      end
    end
    if ~skipDisplay, close(hwait); drawnow; end
   
  case 'Mahalanobis'
    % with diagonal covariance
    V = max(var(normFd,[],1),1e-5);
    C = diag(V);
    D = util.sqdistance(normFd',normFd',C);
    D(D < 0) = 0;
    dist.F = sqrt(D);
    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen)';
        C = diag(V(partVec+(pp-1)*partLen));
        D = util.sqdistance(Fp,Fp,C);
        D(D < 0) = 0;
        dist.Fp(:,:,pp) = sqrt(D);
      end
    end
    
  case 'KL-Symmetric'
    % calculate the Kullback-Leibler divergence
    normFd(normFd == 0) = eps;
    lP = log(normFd);
    lP(normFd == 0) = -30;
    PlQ = lP * normFd';
    KL = 0.5 * bsxfun(@minus,diag(PlQ),PlQ);
    dist.F = log(1 + KL + KL');
    
    if ~skip_parts
      % do the same on individual parts histograms
      dist.Fp = zeros(nImages,nImages,nParts,'single');
      for pp = 1:nParts
        Fp = dist.X(:,partVec+(pp-1)*partLen);
        PlQ = lP(:,partVec+(pp-1)*partLen) * Fp';
        KL = bsxfun(@minus,diag(PlQ),PlQ);
        dist.Fp(:,:,pp) = KL + KL';
      end
    end
    
    % EMD is way too slow to process in any dataset
%   case 'EMD'
%     % Earth-Mover distance
%     D = groundDistance(histLen);
%     dist.F = zeros(nImages,nImages,'single');
%     for ii = 1:nImages-1
%       hist_i = double(normFd(ii,:)');
%       for jj = ii+1:nImages
%         hist_j = double(normFd(jj,:)');
%         dist.F(ii,jj) = emd_hat_gd_metric_mex(hist_i,hist_j,D,-1);
%         dist.F(jj,ii) = dist.F(ii,jj);
%       end
%     end
%     
%     if ~skip_parts
%       % do the same on individual parts histograms
%       D = groundDistance(partLen);
%       dist.Fp = zeros(nImages,nImages,nParts,'single');
%       for pp = 1:nParts
%         Fp = Fd(:,partVec+(pp-1)*partLen);
%         for ii = 1:nImages-1
%           hist_i = double(Fp(ii,:)');
%           for jj = ii+1:nImages
%             hist_j = double(Fp(jj,:)');
%             dist.Fp(ii,jj,pp) = emd_hat_gd_metric_mex(hist_i,hist_j,D,-1);
%             dist.Fp(jj,ii,pp) = dist.Fp(ii,jj,pp);
%           end
%         end
%       end
%     end

end

end

function D = groundDistance(N)
THRESHOLD = 3;
D = ones(N,N).*THRESHOLD;
for i=1:N
  for j=max([1 i-THRESHOLD+1]):min([N i+THRESHOLD-1])
    D(i,j)= abs(i-j);
  end
end
end

function A = binSimilarity(N)
THRESHOLD = 3;
A = sparse(N,N);
for i=1:N
  for j=max([1 i-THRESHOLD+1]):min([N i+THRESHOLD-1])
    A(i,j)= 1-(abs(i-j)/THRESHOLD);
  end
end
end

function C = partmult(B,A,num,vlen)
% assume B must be multiplied with blkdiag(A,A,A,A,...), where A must be
% repeated num times. It is a waste of memory, so do each piece separately
vec = 1:vlen;
C = zeros(size(B));
for ii = 1:num
  C(:,vec+(ii-1)*vlen) = B(:,vec+(ii-1)*vlen) * A;
end
end
