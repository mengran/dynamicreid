function [mask,pmask] = calc_segmentations(BP,H,W,ped,conf)
%
% Calculate mask segmentations.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import reidaam.*;

numped = length(ped);

% check if we are segmenting an entire dataset or just a partition of it
if isfield(ped,'run')
  num = length([ped.run]);
  mask = cell(num,1);
  pmask = cell(num,conf.nParts);
else
  mask = cell(conf.nImages,1);
  pmask = cell(conf.nImages,conf.nParts);
end


for pedidx = 1:numped
  globals = ped(pedidx).global;
  if isfield(ped,'run')
    runnums = ped(pedidx).run;
  else
    runnums = globals;
  end
  
  [M,PM] = calc_masks(BP(:,:,runnums),H,W,conf);
  for jj = 1:length(runnums)
    mask{runnums(jj)} = M{jj};
    
    for pp = 1:conf.nParts
      pmask{runnums(jj),pp} = PM{jj,pp};
    end
  end
end
