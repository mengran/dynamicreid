function [Mu,Cv] = model_appearances(rgb,MAP,ped,conf,par)
% [Mu,Cv] = model_appearances(rgb,ped,conf,parts)
%
% Calculates the Gaussian appearance model
% 
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import reidaam.*;

% this is the size of the patch where local segmentation is included in the
% computation of the Gaussian parameters; bandweights defines a Gaussian
% neighborhood of importance giving 1 to the middle pixel and roughly half
% weight midway to the border
bandwidth = 9; % bandwidth for mean-shift and patch side for neighborhoods
bandmid = (bandwidth-1)/2 + 1;
bandweights = fspecial('gaussian',[bandwidth bandwidth],(bandwidth-1)/4);
bandweights = bandweights / bandweights(bandmid,bandmid);
BW = bandweights(:);

numped = length(ped);
numparts = conf.nParts;
minvar = par.MinVar;

Mu = cell(numped,2);
Cv = cell(numped,2);

%%
% collect the data necessary for parallel computing
PARC = struct([]);
for pedidx = 1:numped
  % retrieve global and testrun indices of picked images
  idxprob = ped(pedidx).probes;
  idxgall = ped(pedidx).gallery;
  probes = ped(pedidx).global(idxprob); % global indexes of probe images
  gallery = ped(pedidx).global(idxgall); %global indexes of gallery images
  runprob = ped(pedidx).run(idxprob);
  rungall = ped(pedidx).run(idxgall);

  % cut all the parts from the gallery and probe images
  PARC(pedidx).G = get_parts(rgb,gallery,MAP(:,:,rungall),conf);
  PARC(pedidx).P = get_parts(rgb,probes,MAP(:,:,runprob),conf);
end

%%
% parallel computation of the Gaussian models
parfor pedidx = 1:numped
  gMut = cell(numparts,1);
  gCvt = cell(numparts,1);
  pMut = cell(numparts,1);
  pCvt = cell(numparts,1);
  for pidx = 1:numparts
    % retrieve gallery set model
    [M,V] = estimateGaussian(PARC(pedidx).G{pidx},bandwidth,BW);
    gMut{pidx} = M;
    gCvt{pidx} = max(V,minvar);
    
    % retrieve probe set model
    [M,V] = estimateGaussian(PARC(pedidx).P{pidx},bandwidth,BW);
    pMut{pidx} = M;
    pCvt{pidx} = max(V,minvar);
  end
  
  Mu(pedidx,:) = {gMut, pMut};
  Cv(pedidx,:) = {gCvt, pCvt};
end


%%
function [M,V] = estimateGaussian(X,bandwidth,BW)
%
% Estimate Gaussian parameters.
% X = [height x width x channels x num]
%
warning('off','stats:kmeans:EmptyCluster');
warning('off','stats:kmeans:FailedToConverge');

padval = (bandwidth-1)/2; % padding needed for full part processing
patchsize = bandwidth*bandwidth; % size of patch neighborhood
onepatch = ones(patchsize,1);

[hei,wid,chs,nImages] = size(X); area = hei*wid;
Y = reshape(X,[area chs nImages]);   % vectorized images
Z = padarray(X,[padval padval 0 0]); % padded images

% create the Gaussian spatial 2D weights in the columned format
BW = BW(:,ones(1,area));

W = zeros(patchsize*nImages,area,chs);
mask = zeros(patchsize*nImages,area);
num = zeros(area,1);

for ii = 1:nImages
  rows = (1:patchsize) + patchsize * (ii-1);
  
  % segment the image into similar colored regions
  % retrieve the labeling image and pad it with zeros
  labels = kmeans(Y(:,:,ii),5,'emptyaction','drop','replicates',3);
  L = reshape(labels,[hei wid]);
  Lfull = padarray(L,[padval padval]);
  
  % create the sliding window column image
  Lwin = im2col(Lfull,[bandwidth bandwidth],'sliding');
  
  % create the matching matrix to keep only same-neighbors
  Lrow = reshape(L,[1 area]);
  Lmatch = Lrow(onepatch,:);
  Lmask = (Lwin == Lmatch) .* BW; % weighted mask
  
  % same work with the actual part image
  W(rows,:,1) = im2col(Z(:,:,1,ii),[bandwidth bandwidth],'sliding');
  W(rows,:,2) = im2col(Z(:,:,2,ii),[bandwidth bandwidth],'sliding');
  W(rows,:,3) = im2col(Z(:,:,3,ii),[bandwidth bandwidth],'sliding');
  
  num = num + sum(Lmask,1)'; % update number of samples (unweighted by scores)
  mask(rows,:) = Lmask;
  
end

% calc mean and variance
norm = sum(mask,1)';
XW = bsxfun(@times,W,mask);
M = bsxfun(@rdivide,squeeze(sum(XW,1)),norm);
V = bsxfun(@rdivide,squeeze(sum(bsxfun(@times,XW,W),1)),norm) - M.^2;

% num is always > 0, but fewer than 5 is bad (use the number unweighted by scores)
nums = num(:,ones(1,chs));
highvar = (nums < 5);
if any(highvar(:)), V(highvar) = 10000; end

% save mean and variance
M = reshape(M,[hei wid chs]);
V = reshape(V,[hei wid chs]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTES
% 18 Jan 13 - re-introducing Gaussian weighting so that neighboring pixels
%             become less important the farther they are from the middle
