function MAP = estimate_pose(ny,nx,ped,conf,par,tt)
%
% This function estimates the body pose.
% If the parameter tt is given, then it also uses the evidence maps.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
import reidaam.*;

util.chkmkdir(conf.PoseDir);
if nargin == 6 && tt > 0
    Do_LoadEvidence = true;
else
    Do_LoadEvidence = false;
end

% get the number of pedestrians and images
numped = length(ped);
numall = length([ped(:).global]);
numparts = conf.nParts;
detdir = [conf.DetectDir '/'];
appdir = [conf.AppearanceDir '/'];
minp = par.MinProb;

gx = 1:nx; gy = 1:ny;
gr = par.Rots;
nr = length(gr);
grad = gr*pi/180;

% min probability when turning detector responses to probabilities
% e0 = par.MinDetect;

%%
% re-scale the kinematic prior
scale = conf.parHOG.Scale; scale2 = scale^2;
prior = conf.prior;
prior.xij = prior.xij * scale;
prior.yij = prior.yij * scale;
prior.xji = prior.xji * scale;
prior.yji = prior.yji * scale;
prior.sigma2_x = prior.sigma2_x * scale2;
prior.sigma2_y = prior.sigma2_y * scale2;

% Get the structure of the prior: parents and children nodes.
nodes = get_nodes(prior.edges);

% Pre-calculate all the transformations mappings
Tmaps = calc_transformations(prior,gy,gx,gr);

% Pre-calculate all the Gaussian filters of the joints
Gfilters = calc_gaussians(prior,ny,nx,nr,1,1,grad);

%%
% process images and store the MAP body estimates (parallel split up)
if exist('matlabpool','file')
    nPARC = max(min(matlabpool('size'),4),1);
else
    nPARC = 1;
end
splitSize = ceil(numped/nPARC);
PARC = cell(nPARC,1);
for ps = 1:nPARC-1
    PARC{ps}.nums = (ps-1)*splitSize+1:(ps)*splitSize;
    PARC{ps}.len = length(PARC{ps}.nums);
    PARC{ps}.ped = ped(PARC{ps}.nums);
end
PARC{nPARC}.nums = (nPARC-1)*splitSize+1:numped;
PARC{nPARC}.len = length(PARC{nPARC}.nums);
PARC{nPARC}.ped = ped(PARC{nPARC}.nums);

res2 = cell(1,nPARC);
parfor (ps = 1:nPARC,nPARC)
    numsplit = PARC{ps}.len;
    res2{ps} = cell(1,numsplit);
    for splitidx = 1:numsplit
        %pedidx = PARC{ps}.nums(splitidx);
        pedpid = PARC{ps}.ped(splitidx).pid;
        globals = PARC{ps}.ped(splitidx).global;
        numimg = length(globals);
        
        % load the scores
        S = load([detdir num2str(pedpid,'%04d') '-scores.mat']);
        Mj = max(S.SCORE,minp);
        
        if Do_LoadEvidence
            locals = PARC{ps}.ped(splitidx).local;
            
            % load the evidence maps
            E = load([appdir num2str(pedpid,'%04d') '-evidence.mat']);
            
            % combine the evidence maps with the part detections using
            % probability theory rule: P(A U B) = P(A) + P(B) - P(A)*P(B)
            % less punitive than just multiplying them as in standard Bayes
            Mj = Mj(:,:,:,:,locals);
            Ej = exp(E.EVI - max(E.EVI(:)));
            Mj = Mj + Ej - Mj .* Ej;
        end
        
        % process each image and store the results
        pedres = zeros(3,numparts,numimg,'single');
        for jj = 1:numimg
            % retrieve the mPIMO part marginals
            pedres(:,:,jj) = ...
                run_pictorial(ny,nx,gr,Mj(:,:,:,:,jj),nodes,Tmaps,Gfilters,par);
        end
        res2{ps}{splitidx} = pedres;
    end
end
res = [res2{:}];

% collate all the matrices in a single one
MAP = zeros(3,numparts,numall,'single');
for pedidx = 1:numped
    globals = ped(pedidx).global;
    if isfield(ped(pedidx),'run')
        runnums = ped(pedidx).run;
    else
        runnums = globals;
    end
    MAP(:,:,runnums) = res{pedidx};
end
