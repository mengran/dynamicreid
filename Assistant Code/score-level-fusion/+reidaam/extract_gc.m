function dist = extract_gc(dist,data,pmskset,ped,par)
%
% Extracts Gray-Color features from the given images and parts.
% The Gray part is a histogram of the dark/white and colorless pixels.
% The Color part is a 2D histogram of the colored pixels (discarding their
% brightness levels).
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

skipBar = isfield(par,'NoDisplay') && par.NoDisplay;

% retrieve the image indices into the global dataset (used to access
% information in data)
globals = [ped.global];
nImages = length(globals);

% retrieve the image indices into the local run testset (used to access
% information in pmskset)
if isfield(ped,'run')
  runinds = [ped.run];
else
  runinds  = globals;
end

nParts = size(pmskset,2);
qlevels = par.NBINs;

scaleG = par.WeightGrays;
chromarea = qlevels(1)*qlevels(2);
histSize = qlevels(3) + chromarea;

GC = zeros(nImages,nParts*histSize);

if ~skipBar, hwait = waitbar(0,'Extracting GC histograms..'); end
for ii = 1:nImages
  
  % retrieve the map of gray values, the gray votes interpolated between
  % neighboring bins, the color votes bilinearly interpolated, the
  % saturation weights for dark colors
  G = data.G(:,globals(ii)); Cmap = ~G;
  V = histInterp2(data.V(:,globals(ii)),qlevels(3));
  T = histInterp2D(data.H(:,globals(ii)),data.S(:,globals(ii)),...
    qlevels(1),qlevels(2));
  WpropSat = data.WpropSat(:,globals(ii));
  
  hist = zeros(nParts * histSize,1);
  for pp = 1:nParts
    % count the graylevel pixels
    mask = pmskset{runinds(ii),pp}(:); % part mask
    gmask = G & mask;
    grays = accumarray([V{1}(gmask,1); qlevels(3)],[V{2}(gmask,1); 0])...
          + accumarray([V{1}(gmask,2); qlevels(3)],[V{2}(gmask,2); 0]);
    
    % add the residual colors from the gray pixels
    ww = WpropSat(gmask);
    colors = accumarray([T{1}(gmask,1); chromarea],[T{2}(gmask,1).*ww; 0])...
      + accumarray([T{1}(gmask,2); chromarea],[T{2}(gmask,2).*ww; 0])...
      + accumarray([T{1}(gmask,3); chromarea],[T{2}(gmask,3).*ww; 0])...
      + accumarray([T{1}(gmask,4); chromarea],[T{2}(gmask,4).*ww; 0]);
    
    % count the remaining color pixels
    cmask = Cmap & mask;
    if any(cmask)
      colors = colors ...
        + accumarray([T{1}(cmask,1); chromarea],[T{2}(cmask,1); 0]) ...
        + accumarray([T{1}(cmask,2); chromarea],[T{2}(cmask,2); 0]) ...
        + accumarray([T{1}(cmask,3); chromarea],[T{2}(cmask,3); 0]) ...
        + accumarray([T{1}(cmask,4); chromarea],[T{2}(cmask,4); 0]);
    end

    % normalize, apply the part weight and store
    indices = (pp-1)*histSize+1:pp*histSize;
    vector = [grays*scaleG; colors*(1-scaleG)];
    hist(indices) = vector ./ sum(vector);    
  end
  GC(ii,:) = hist;
  
  if ~skipBar, waitbar(ii/nImages,hwait); end
end
if ~skipBar, close(hwait); drawnow; end

% save the results and the bin-similarity matrix
dist.X = GC;
dist.A = data.BinSim;
dist.BhattaNorm = data.BhattaNorm;

end


function V = histInterp2(vvec,levels)
% calculate the histogram of the given values using interpolation to avoid
% aliasing
lmax = levels - 1;
num = numel(vvec);

binc = (1:lmax) - 0.5;
xmat = binc(ones(num,1),:);

V{1} = ones(num,2,'uint8');  % indices
V{2} = zeros(num,2,'single'); % fractions

% case 1 and 2
select = vvec < 0.5;
V{1}(select,1) = 1; V{2}(select,1) = 1; % vote bin 1 with weight 1
select = vvec >= levels-0.5;
V{1}(select,1) = levels; V{2}(select,1) = 1; % vote bin 'levels' with 1
% case 3
vmat = vvec(:,ones(1,lmax));
fmat = vmat - xmat;
[select,bin] = find(fmat == 0);
V{1}(select,1) = bin; V{2}(select,1) = 1; % vote 'bin' with 1
% case 4
fpos = find(fmat > 0 & fmat < 1);
fval = fmat(fpos);
[select,bin] = ind2sub([num lmax],fpos);
V{1}(select,1) = bin; V{2}(select,1) = 1-fval; % vote 'bin' with 1-fval
V{1}(select,2) = bin+1; V{2}(select,2) = fval; % vote 'bin+1' with fval

% % comparing interpolated (red) vs nn-quantized (black)
% grays = accumarray([V{1}(:,1); levels],[V{2}(:,1); 0])...
%   + accumarray([V{1}(:,2); levels],[V{2}(:,2); 0]);
% 
% figure(301);clf;plot(grays,'r-');
% ttt = histc(vvec,[0:9 10.1]);ttt(end)=[];
% hold on;plot(ttt,'k-');
end

function V = histInterp2D(rvec,cvec,height,width)
% calculate the histogram of the given values using interpolation to avoid
% aliasing - 2D version
rmax = height - 1; cmax = width - 1;
num = numel(rvec);

V{1} = ones(num,4,'uint16');  % indices
V{2} = zeros(num,4,'single'); % fractions

rbinl = floor(rvec); % row bin lower coordinate
cbinl = floor(cvec); % col bin lower coordinate
rbinu = rbinl + 1;   % row bin upper coordinate
cbinu = cbinl + 1;   % col bin upper coordinate
distr = rvec - rbinl;   % distance to start of row bin
distc = cvec - cbinl;   % distance to start of col bin

% fold the edges
rbinl(rbinl == -1) = 0;
cbinl(cbinl == -1) = 0;
rbinu(rbinu == height) = rmax;
cbinu(cbinu == width) = cmax;

% first quadrant (NW)
V{1}(:,1) = rbinl + cbinl * height + 1;
V{2}(:,1) = (1-distr) .* (1-distc);
% second quadrant (SW)
V{1}(:,2) = rbinu + cbinl * height + 1;
V{2}(:,2) = distr .* (1-distc);
% third quadrant (NE)
V{1}(:,3) = rbinl + cbinu * height + 1;
V{2}(:,3) = (1-distr) .* distc;
% third quadrant (NE)
V{1}(:,4) = rbinu + cbinu * height + 1;
V{2}(:,4) = distr .* distc;

end

% NOTES
%
% Mar 18 - discovered error, pmskset{ii,pp} instead of the
%          correct pmskset{globals(ii),pp}
