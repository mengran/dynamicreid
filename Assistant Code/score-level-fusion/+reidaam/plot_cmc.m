function plot_cmc(stats,expName,showEBar,calcGrps)
% Copyright (C) 2013 Cheng Dong Seon

if nargin < 4, calcGrps = 0; end;
if nargin < 3, showEBar = 0; end;

[nPed,~,nRun] = size(stats.matchmat);
normval = 100 / nPed;
mm = mean(double(stats.CMC),2) * normval;
se = std(double(stats.CMC),[],2) *normval /sqrt(nRun); % standard error
ma = mean(stats.nAUC);

figure;
if showEBar
  eh = errorbar(mm,se, 'r-', 'Linewidth',2.0);
else
  eh = plot(mm, 'r-', 'Linewidth',2.0);
end
switch expName
  case 'VIPeR'
    axis([0,100,0,100]);
  case 'ETHZ1'
    axis([1,7,0,100]);
  case 'ETHZ2'
    axis([1,7,0,100]);
  case 'ETHZ3'
    axis([1,7,0,100]);
  case 'iLIDS'
    axis([1,25,0,100]);
  case 'CAVIAR'
    axis([1,25,0,100]);
end
set(eh,'DisplayName',[expName ' (nAUC ' num2str(ma,'%.2f') ')']);
title('Cumulative Matching Characteristic (CMC)'); grid on;
if showEBar, util.errorbar_tick(eh); end
if isfield(stats,'pCMC')
  if calcGrps
    pnames = {'Torso','Bust','Torso+Thigs','Headless','Feetless'};
  else
    %         pnames = {'Leg','Thig','Thig','Leg','Head','Torso'};
    pnames = {'Torso','Shoulders','Head','LArm','LForearm',...
      'RArm','RForearm','LThigh','LLeg','RThigh','RLeg'};
  end
  ma = mean(stats.pnAUC,2);
  hold on;
  for pp = 1:size(stats.pCMC,2)
    thisCMC = double(squeeze(stats.pCMC(:,pp,:)));
    mm = mean(thisCMC,2) * normval;
    se = std(thisCMC,[],2) * normval /sqrt(nRun);
    if showEBar
      eh = errorbar(mm,se, 'b-', 'Linewidth',2.0);
    else
      eh = plot(mm, 'b-', 'Linewidth',2.0);
    end
    set(eh,'DisplayName',[pnames{pp} ' (nAUC ' num2str(ma(pp),'%.2f') ')']);
    if showEBar, util.errorbar_tick(eh); end
  end
end
if isfield(stats,'fCMC')
  mm = mean(double(stats.fCMC),2) * normval;
  se = std(double(stats.fCMC),[],2) *normval /sqrt(nRun);
  ma = mean(stats.fnAUC);
  if showEBar
    eh = errorbar(mm,se, 'm-', 'Linewidth',2.0);
  else
    eh = plot(mm, 'm-', 'Linewidth',2.0);
  end
  set(eh,'DisplayName',['BVT (nAUC ' num2str(ma,'%.2f') ')']);
  if showEBar, util.errorbar_tick(eh); end
end
if isfield(stats,'mCMC')
  mm = mean(double(stats.mCMC),2) * normval;
  se = std(double(stats.mCMC),[],2) *normval /sqrt(nRun);
  ma = mean(stats.mnAUC);
  if showEBar
    eh = errorbar(mm,se, 'g-', 'Linewidth',2.0);
  else
    eh = plot(mm, 'g-', 'Linewidth',2.0);
  end
  set(eh,'DisplayName',['MSCR (nAUC ' num2str(ma,'%.2f') ')']);
  if showEBar, util.errorbar_tick(eh); end
end

legend('Location','SouthEast');
