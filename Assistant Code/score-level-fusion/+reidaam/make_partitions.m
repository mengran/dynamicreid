function [ped,probes,gallery] = make_partitions(dset,expr,xval,LocalInd)
%
% Create a random partition of the dataset, based on the given type of
% cross-validation to be performed.
%
% Code a bit dated, haven't checked throughly for a while.
%
% Copyright (C) 2013 Cheng Dong Seon

if nargin < 4, LocalInd = false; end

nPed = length(dset); % total number of pedestrians
shotsNum = expr.Shots; % numer of shots

switch expr.Sett
  case {'CTSvsS','CTMvsM'}
    CameraSett = true;
  otherwise
    CameraSett = false;
end

if strcmp(expr.Name,'CAVIAR') && CameraSett
  % the dataset for CAVIAR in camera-based re-id must be pruned of the
  % pedestrians without a second camera
  numimg = zeros(nPed,1);
  for ii = 1:nPed
    numimg(ii) = length(dset(ii).global);
  end
  dset = dset(numimg == 20);
  nPed = length(dset);
end

switch xval.Type
  
  case 'VIPeR'
    % remove bad images from the partitions
    index = [1:247, 340:632];
    no = [440, 487, 540, 554, 591, 632];
    index = setdiff(index, no);
    
    numped = floor(nPed/2);
    randomized = randperm(length(index));
    chosen = index(randomized(1:numped));
    probes = zeros(numped,1);
    gallery = zeros(numped,1);
    for ii = 1:numped
      pid = chosen(ii);
      ped(ii).pid = pid;
      ped(ii).global = dset(pid).global;
      ped(ii).local = dset(pid).local;
      ped(ii).run = ii*2-1:ii*2;
      probes(ii) = dset(pid).global(1);
      gallery(ii) = dset(pid).global(2);
      ped(ii).probes = 1;
      ped(ii).gallery = 2;
    end
    
  case 'drawN'
    % randomly take shotsNum images for each pedestrian
    numtest = 0;
    ped(nPed).pid = 0;
    probes = cell(1,nPed);
    gallery = cell(1,nPed);
    for ii = 1:nPed
      numped = length(dset(ii).global); % no. of images of this ped
      randomized = randperm(numped); % randomize the indices
      num = min(numped, 2*shotsNum); % draw N images for probes and gall
      chosen = randomized(1:num);
      ped(ii).pid = dset(ii).pid;
      ped(ii).global = dset(ii).global(chosen);
      ped(ii).local = dset(ii).local(chosen);
      ped(ii).run = numtest + (1:num);
      numtest = numtest + num;
      
      nProbes = num - shotsNum; % no. of probes, might be less than N
      if nProbes <= 0
        fprintf('Pedestrian %d has only %d images, insufficient for N=%d. Taking 1 probe and rest gallery.\n',...
          ii,numped,shotsNum);
        nProbes = 1;
      end
      ped(ii).probes = 1:nProbes;
      ped(ii).gallery = nProbes+1:num;
      if LocalInd
        probes{ii} = ped(ii).run(ped(ii).probes);
        gallery{ii} = ped(ii).run(ped(ii).gallery);
      else
        probes{ii} = ped(ii).global(ped(ii).probes);
        gallery{ii} = ped(ii).global(ped(ii).gallery);
      end
    end
        
  case {'gall1', 'gong'} % added the gong
    % randomly take 1 image as gallery for each pedestrian and the
    % others are considered probes
    numtest = 0;
    for ii = 1:nPed
      numped = length(dset(ii).global); % no. of images of this ped
      randomized = randperm(numped); % randomize the indices
      ped(ii).pid = dset(ii).pid;
      ped(ii).global = dset(ii).global(randomized);
      ped(ii).local = dset(ii).local(randomized);
      ped(ii).run = numtest + (1:numped);
      numtest = numtest + numped;
      
      nProbes = numped - 1;
      probes{ii} = ped(ii).global(1:nProbes);
      gallery{ii} = ped(ii).global(numped);
      ped(ii).probes = 1:nProbes;
      ped(ii).gallery = numped;
    end
        
  case 'simple'
    % randomly pick 1 image as probe and 1 as gallery.
    % if the camera setting is on, then take them from different parts of
    % the image set, assuming the first half is the first camera
    ped(nPed).pid = 0;
    probes = zeros(1,nPed);
    gallery = zeros(1,nPed);
    for ii = 1:nPed
      globals = dset(ii).global;
      numimgs = length(globals);
      
      % randomly choose numShots images for probes and gallery sets
      if CameraSett
        numcamera = numimgs/2;
        probnums = randperm(numcamera);
        gallnums = numcamera + randperm(numcamera);
        chosen = [probnums(1) gallnums(1)];
      else
        randomized = randperm(numimgs);
        chosen = randomized(1:2);
      end

      % create the ped info
      ped(ii).pid = dset(ii).pid;
      ped(ii).global = globals(chosen);
      ped(ii).local = dset(ii).local(chosen);
      ped(ii).run = (ii-1)*2+1:ii*2;
      ped(ii).probes = 1;
      ped(ii).gallery = 2;
      
      if LocalInd
        probes(ii) = ped(ii).run(1);
        gallery(ii) = ped(ii).run(2);
      else
        probes(ii) = ped(ii).global(1);
        gallery(ii) = ped(ii).global(2);    
      end
    end
    
  otherwise
    error('Unrecognized type of cross-validation to generate partitions');
    
end
