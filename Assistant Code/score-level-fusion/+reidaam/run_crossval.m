function stats = run_crossval(stats,dist,ped,prob,gall,xval,t)
%
% dist.F contains VT distances
% dist.Fp contains VT distances between parts
% dist.MSCR contains MSCR distances
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

nParts = size(dist.Fp,3);
if nargin < 7, t = 1; end

switch xval.Type
  case {'50%','VIPeR','simple'}
    % random number of pedestrians, 50% of all pedestrians in the data
    % or just simple selection of matches from the whole matrix
    distF = dist.F(prob,gall);
    distFp = dist.Fp(prob,gall,:);
    distM = dist.M(prob,gall);
    
  case 'drawN'
    % probes and gallery have N shots, hence NxN distances
    % dist between ped i and j is = mindist between shots
    nPed = length(ped);
    distF = zeros(nPed,nPed);
    distFp = zeros(nPed,nPed,nParts);
    distM = zeros(nPed,nPed);
    for ii = 1:nPed
      idxprob = ped(ii).probes;
      run_ii = ped(ii).run(idxprob);
      for jj = 1:nPed
        idxgall = ped(jj).gallery;
        run_jj = ped(jj).run(idxgall);
        
        dd = dist.Fp(run_ii,run_jj,:);
        distFp(ii,jj,:) = min(min(dd,[],2),[],1); % min dist
        
        dd = dist.M(run_ii,run_jj);
        distM(ii,jj) = min(dd(:)); % min dist
        dd = dist.F(run_ii,run_jj);
        distF(ii,jj) = min(dd(:)); % min dist
      end
    end
        
  case 'gong'
    % all-probes vs 1-gallery, where the probe set is made of all the
    % images not selected as the 1-gallery
    % it is sort of a worst-case scenario simulation of a real 1-vs-1 case,
    % since the bad probe matches will bring the performances down (in
    % other words, a performance value close to the lower bound of what is
    % attainable). anyway, this is the standard used in the previous
    % literature...
    probid = [];
    for ii = 1:length(prob)
      probid = [probid ii*ones(1,length(prob{ii}))];
    end
    tprob = [prob{:}];
    tgall = [gall{:}];
    
    distF = dist.F(tprob,tgall);
    distFp = dist.Fp(tprob,tgall,:);
    distM = dist.M(tprob,tgall);

  otherwise
    error('Unrecognized cross-validation type.');
end

[numProbes,numGalleries] = size(distF);

% weighted average of color and MSCR distances
distFinal = xval.beta * distF + (1 - xval.beta) * distM;

if exist('probid','var')
  colvec = probid';
else
  colvec = (1:numProbes)';
end

compmat = colvec(:,ones(1,numGalleries));
normval = 100 / numel(distF);

% rank matches
[~,ordered] = sort(distFinal,2,'ascend');
matchmat = (ordered == compmat);

% calculate scores
CMC = cumsum(sum(matchmat,1));
AUC = sum(CMC);
nAUC = sum(CMC) * normval;

% store the info
stats.ped(:,t) = ped';
stats.CMC(:,t) = uint16(CMC)'; % Cumulative Matching Characteristic curve
stats.AUC(t)   = uint32(AUC); % Area Under the Curve
stats.nAUC(t)  = nAUC;% Normalized Area Under the Curve
stats.matchmat(:,:,t) = matchmat; % correct match position
stats.ordered(:,:,t) = uint16(ordered); % full matrix of matches

% repeat for parts, color and MSCR scores
pCMC = zeros(numGalleries,nParts);
for pp = 1:nParts
  [~,ordered2] = sort(distFp(:,:,pp),2,'ascend');
  matchmat2 = (ordered2 == compmat);
  
  pCMC(:,pp) = cumsum(sum(matchmat2,1))';
end

[~,ordered] = sort(distF,2,'ascend');
matchmat = (ordered == compmat);
fCMC = cumsum(sum(matchmat,1));
fnAUC = sum(fCMC) * normval;

[~,ordered] = sort(distM,2,'ascend');
matchmat = (ordered == compmat);
mCMC = cumsum(sum(matchmat,1));
mnAUC = sum(mCMC) * normval;

stats.pCMC(:,:,t) = pCMC;
stats.pnAUC(:,t) = sum(pCMC,1)' * normval;
stats.fCMC(:,t) = fCMC;
stats.mCMC(:,t) = mCMC;
stats.fnAUC(t) = fnAUC;
stats.mnAUC(t) = mnAUC;
