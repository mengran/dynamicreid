function dist = extract_mscr(dist,data,maskset,indset,parCPS,parMSCR,num)
%
% Extract MSCR features from the given images, under the assumption that
% only the parts inside the given masks are relevant.
%
% This is mostly legacy algorithms from SDALF, with a few optimizations.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import mscr.*;
import reidaam.*;

if isfield(parMSCR,'Verbose')
  verbose = parMSCR.Verbose; 
else
  verbose = 1;
end

fname = [parCPS.Dir '/MSCR_' num2str(num,'%03d') '.mat'];
if exist(fname,'file')
  % load the pre-computed features from file
  load(fname);
  if verbose, disp('-> MSCR features loaded from file.'); end
else
  if verbose, fprintf('-> Computing MSCR features...'); tic; end
  
  % retrieve the indices for the images to be processed
  % the global index is used to find the correct images in the data
  % the matching masks are retrieved sequentially from maskset
  glbind = [indset.global]; % get global index set
  nImages = length(glbind); % number of images to process
  
  % allocate output
  MSCR = struct('mvec',cell(1,nImages),'pvec',cell(1,nImages));
  
  for ii = 1:nImages
    % retrieve matching image and mask
    rgb = double(data.RGB(:,:,:,glbind(ii)));
    mask = maskset(:,:,ii);
    
    % normalize the illuminant in the image (some pixel values may go out
    % of range)
    [H,W,~] = size(rgb);
    means = reshape(mean(reshape(rgb,[H*W 3]),1),[1 1 3]);
    rgb = 98 * bsxfun(@rdivide,rgb,means);
    % the following code is faster but unfortunately creates different
    % results because of rounding errors
    %rgb = 98 * bsxfun(@rdivide,rgb,mean(mean(rgb,1),2));
    
    % apply the mask to remove irrelevant parts
    rgb = bsxfun(@times,rgb,mask);
    
    % perform histogram equalization on the brightness of the image
    [hue,sat,val] = rgb2hsv(uint8(rgb));
    val(mask) = histeq(val(mask));
    rgb = hsv2rgb(cat(3,hue,sat,val));
    
    % retrieve the MSCR features from the masked image
    [mvec,pvec,~,~] = detect_mscr_masked(255*rgb,double(mask),parMSCR);
    
    % filter the blobs
    [mvec,pvec] = filter_blobs(mvec,pvec,1000);
    
    % save the resulting features
    MSCR(ii).mvec = mvec;
    MSCR(ii).pvec = pvec / 255;
    
  end
  
  save(fname,'MSCR'); if verbose, toc; end
end

% set/overwrite the MSCR features in the dist structure
dist.MSCR = MSCR;
