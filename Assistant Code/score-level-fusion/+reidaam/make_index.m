function dset = make_index(data)
% dset = make_index(data)
%
% Examines the filenames contained in the given directory list and compiles
% a list of indices that identify unique pedestrians and their images.
%
% Copyright (C) 2013 Cheng Dong Seon

assert(~isempty(data.Files),'There are no images in the selected dataset');
nImages = length(data.Files);

% examine the first image
n = data.Files(1).name;
pid = str2double(n(1:4));
num = str2double(n(5:7));
assert(~isnan(pid) && ~isnan(num),'Wrong image filename in the dataset');

% create the first record in the database
dset(1).pid = pid;   % pedestrian identifier
dset(1).local = num; % list of images for this pedestrian (local index)
dset(1).global = 1;  % list of images for this pedestrian (global index)

% create a list of known identifiers
pidlist = pid;

for ii = 2:nImages
  
  % extract pedestrian identifier and image number from the filename
  n = data.Files(ii).name;
  pid = str2double(n(1:4)); % pedestrian identifier
  num = str2double(n(5:7)); % image identifier for this pedestrian
  assert(~isnan(pid) && ~isnan(num),'Wrong image filename in the dataset');
  
  % find the record for this pedestrian
  jj = find(pidlist == pid);
  if isempty(jj)
    % add the new pid to the list of known identifiers
    pidlist(end+1) = pid;
    
    % make a new record for this pedestrian and initialize its list
    jj = length(pidlist);
    dset(jj).pid = pid;
    dset(jj).local = num;
    dset(jj).global = ii;
  else
    % add the image to the list for the known pedestrian
    dset(jj).local(end+1) = num;
    dset(jj).global(end+1) = ii;
  end
end
