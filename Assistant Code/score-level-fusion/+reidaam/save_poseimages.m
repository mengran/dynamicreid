function save_poseimages(data,ped,par,rundata)
%
% Load the body pose estimates and overlay them on top of the dataset
% images to show the fit.
%
% Save all the composite images in the given directory.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

conf = reidaam.load_conf(par);

if nargin < 4
  % single global dataset run
  load([par.Dir '/pose_estimations/MAPestimates.mat']);

  % output directory
  imdir = [par.Dir '/pose_images'];
  util.chkmkdir(imdir);
else
  % partition run
  testid = rundata(1);  tstr = num2str(testid,'%03d');
  numiter = rundata(2); nstr = num2str(numiter);

  % load this partition run #numiter estimates
  load([conf.PoseDir '/MAPestimates' nstr '_' tstr '.mat']);
  
  % output directory
  imdir = [par.Dir '/pose_images_it' nstr '_' tstr '/'];
  util.chkmkdir(imdir);
end

numped = length(ped);
[height,width,~,~] = size(data.RGB);

% scale the part dimensions
scale = par.Scale;
scaledSize = ceil(scale * conf.part_dims)-1;
scaledCorner = -scaledSize / 2;

% process all images
for pedidx = 1:numped
  globals = ped(pedidx).global;
  numimg = length(globals);
  if isfield(ped,'run')
    runnums = ped(pedidx).run;
  else
    runnums = globals;
  end

  for jj = 1:numimg
    I = data.RGB(:,:,:,globals(jj));
    map = zeros(height,width);
    
    for pidx = 1:conf.nParts
      iy = MAP(1,pidx,runnums(jj));
      ix = MAP(2,pidx,runnums(jj));
      ridx = MAP(3,pidx,runnums(jj));
      rotAngle = par.Rots(ridx);
      
      % create a rectangle
      FM = ones(scaledSize(pidx,[2 1])+1);
      FM(2:end-1,2:end-1) = 0;
      
      % fold the part segmentation into the whole image
      if rotAngle
        FM = util.imrot_fftbased(FM,-rotAngle,0);
        FM = FM .* (FM > 0.4);
        
        % approximate coordinates, problem is with the exact
        % transformation during rotation
        %rect = ([ix; iy]-1) + Rmat' * scaledCorner(pidx,:)' - iTvec - [2;1];
        rect(3) = size(FM,2)-1;
        rect(4) = size(FM,1)-1;
        rect(1) = ix - round(rect(3)/2);
        rect(2) = iy - round(rect(4)/2);
      else
        rect = [[ix iy-1]+scaledCorner(pidx,:) scaledSize(pidx,:)];
        rect = round(rect);
      end
      % check that paste rect is within image boundaries
      [phei,pwid] = size(FM); prect = [1 1 pwid-1 phei-1];
      if rect(1) < 1
        offset = 1 - rect(1);
        prect(1) = prect(1) + offset;
        prect(3) = prect(3) - offset;
        rect(1) = 1;
        rect(3) = rect(3) - offset;
      end
      if rect(2) < 1
        offset = 1 - rect(2);
        prect(2) = prect(2) + offset;
        prect(4) = prect(4) - offset;
        rect(2) = 1;
        rect(4) = rect(4) - offset;
      end
      if rect(1)+rect(3) > width
        prect(3) = prect(3) + width - (rect(1)+rect(3));
        rect(3) = width - rect(1);
      end
      if rect(2)+rect(4) > height
        prect(4) = prect(4) + height - (rect(2)+rect(4));
        rect(4) = height - rect(2);
      end
      rows = rect(2):rect(2)+rect(4);
      cols = rect(1):rect(1)+rect(3);
      prows = prect(2):prect(2)+prect(4);
      pcols = prect(1):prect(1)+prect(3);
      
      map(rows,cols) = max(map(rows,cols),FM(prows,pcols));
    end
    
    % modify the original image with the colored rectangle
    pixels = find(map(:));
    I(pixels) = round(double(I(pixels)) .* (1-map(pixels)) + 255 * map(pixels));
    I(pixels + height*width) = 0;
    I(pixels + height*width*2) = 0;
    imwrite(I,sprintf('%s/%04d%03d.png',imdir,pedidx,ped(pedidx).local(jj)));
  end
end
