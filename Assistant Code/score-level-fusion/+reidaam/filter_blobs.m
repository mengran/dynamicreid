function [mv,pv] = filter_blobs(mvec,pvec,maxsize)
%
% Filter the given MSCR blobs, removing the big or too similar ones.
%
% Copyright (C) 2013 Cheng Dong Seon


% sort blobs by size
[area,idx] = sort(mvec(1,:),'ascend');
numb = length(idx);

% from the smallest to the biggest, decide if we keep or remove blobs
keeplist = [];
for ii = 1:numb
  % when we hit blobs bigger than the allowed size, we can stop counting
  % the blobs to keep
  if area(ii) > maxsize, break; end
  
  % centroid of the ii-th blob
  xyii = mvec(2:3,idx(ii));
  avgrad = sqrt(area(ii)/pi); % average radius
  
  keep = true; % assume we keep ii-th blob
  for jj = ii+1:numb
    if area(jj) > maxsize, break; end
    
    % ratio of size, area(jj) is always greater equal than area(ii)
    aratio = area(ii)/area(jj);
    if aratio <= 0.6
      % we are comparing against bigger blobs, so we can stop and
      % keep the ii-th blob
      break;
    end
    
    % centroid of the jj-th blob
    xyjj = mvec(2:3,idx(jj));
    
    % distance between centroids
    mdist = sqrt( sum((xyii - xyjj).^2) );
    
    % distance between colors
    cdist = sqrt( sum((pvec(:,idx(ii)) - pvec(:,idx(jj))).^2) );
    
    if mdist < 1.5*avgrad && cdist < 0.1*255
      % position and color match too closely, so reject the ii-th blob
      keep = false;
      break;
    end
  end
  
  if keep, keeplist = [keeplist idx(ii)]; end
end

mv = mvec(:,keeplist);
pv = pvec(:,keeplist);
