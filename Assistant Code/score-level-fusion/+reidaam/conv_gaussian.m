function alpha = conv_gaussian(beta,Tmap,Gfilter,minp)
%
% Perform these steps:
% - apply Tmap's first map to beta
% - perform the Gaussian convolution with Gfilter
% - apply Tmap's second map
% - normalize the output map
%
% This version uses non-clipping mappings
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
dims = size(beta);
fdims = 2*dims;
xdims = [dims(1) dims(2) fdims(3)];
crop = [Gfilter.halfy Gfilter.halfx Gfilter.halfr];

% Move the evidence map to the coordinate system of the joint
Tau = zeros(xdims);
Tau(Tmap.S1) = beta(Tmap.R1);

% Calculate the Gaussian convolution
Fy = real(ifft( fft(Tau,fdims(1),1) .* Gfilter.FNy2 ,[],1));
Fy = Fy(crop(1)+(1:dims(1)),:,:);

Fxy = real(ifft( fft(Fy,fdims(2),2) .* Gfilter.FNx2 ,[],2));
Fxy = Fxy(:,crop(2)+(1:dims(2)),:);

Gamma = real(ifft( fft(Fxy,fdims(3),3) .* Gfilter.FNr ,[],3));
% can't crop rot maps now because it will clip

% Calculate the upward message
alpha = zeros(dims,'single');
alpha(Tmap.S2) = Gamma(Tmap.R2);

% normalize the output maps
alpha(alpha<minp) = minp;
alpha = alpha / sum(alpha(:));
