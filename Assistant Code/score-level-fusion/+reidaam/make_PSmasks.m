function [msk,partmsk] = make_PSmasks(rgb,ped,par)
% [msk,partmsk] = make_PSmasks(rgb,ped,par)
%
% Retrieve pedestrian masks with the PS algorithm.
%
% Copyright (C) 2013 Cheng Dong Seon

import reidaam.*;

% create the config file and info
conf = load_conf(par);
[H,W,~,nImages] = size(rgb);
conf.nImages = nImages;

namefile = [par.Dir '/PSmasks.mat'];

if exist(namefile,'file') && ~par.IgnoreSavefiles
    load(namefile);
else
    % perform the part detection with filtering
    fprintf('Detect parts...');
    stime = clock;
    detect_all_parts(rgb,ped,conf,par);
    fprintf('OK (%s)\n',util.getDurationString(stime));
    
    % run the pictorial model body pose estimation
    fprintf('Estimate body pose...');
    stime = clock;
    MAP = estimate_pose(H,W,ped,conf,par);
    fprintf('OK (%s)\n',util.getDurationString(stime));
    save([conf.PoseDir 'MAPestimates.mat'],'MAP');
    
    % create the masks
    [msk,partmsk] = calc_segmentations(MAP,H,W,ped,conf);
    save(namefile,'msk','partmsk');
end
