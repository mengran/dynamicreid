function H = calc_gaussians(prior,ny,nx,nr,Dy,Dx,grad)
%
% Compute the Gaussian filters used at the joints in pose estimation.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

nParts = length(prior.names);
edges = prior.edges;
s2y = prior.sigma2_y;
s2x = prior.sigma2_x;
k = prior.k;

s2y(s2y<4) = 4;
s2x(s2x<4) = 4;

% process all joints
edges = sort(edges,2)';
H = cell(nParts,nParts);
for joint = edges
  i = joint(1);
  j = joint(2);
  
  % retrieve standard deviations
  sy = sqrt(s2y(i,j));
  sx = sqrt(s2x(i,j));
  sr = sqrt(1/k(i,j));% / par.RfilterStrength;

  % calculate the minimum filter size at 3*sigma
  numy = min( ceil(3*sy/Dy)*2+1,ny); halfy = floor(numy/2);
  numx = min( ceil(3*sx/Dx)*2+1,nx); halfx = floor(numx/2);
  numr = nr; halfr = floor(numr/2);

  % create the Y filter
  nn = (0:numy-1) - halfy;
  %Ny = normpdf(nn*Dy,0,sy)'; % stat toolbox required
  Ny = exp(-0.5 * ((nn*Dy)/sy).^2)' / sqrt(2*pi) / sy; % manual formula
  Ny = Ny ./ sum(Ny);
  %H{i,j}.Ny = Ny; %sum(Ny);
  
  % get the FT
  FNy1 = fft(Ny,2*ny,1);
  %H{i,j}.FNy = FNy1(:,ones(1,2*nx,1),ones(1,1,nr));
  H{i,j}.FNy2 = FNy1(:,ones(1,nx,1),ones(1,1,2*nr));
  %H{i,j}.FNy3 = FNy1(:,ones(1,2*nx,1),ones(1,1,2*nr));
  
  % create the X filter
  nn = (0:numx-1) - halfx;
  %Nx = normpdf(nn*Dx,0,sx); % stat toolbox required
  Nx = exp(-0.5 * ((nn*Dx)/sx).^2) / sqrt(2*pi) / sx; % manual formula
  Nx = Nx ./ sum(Nx);
  %H{i,j}.Nx = Nx; %sum(Nx);

  % get the FT
  FNx1 = fft(Nx,2*nx,2);
  %H{i,j}.FNx = FNx1(ones(2*ny,1,1),:,ones(1,1,nr));
  H{i,j}.FNx2 = FNx1(ones(ny,1,1),:,ones(1,1,2*nr));
  %H{i,j}.FNx3 = FNx1(ones(2*ny,1,1),:,ones(1,1,2*nr));
  
  % treat the von Mises distribution as an approximate Gaussian
  Nr = zeros(1,1,numr);
  %Nr(1,1,:) = normpdf(grad,0,sr); % stat toolbox required
  Nr(1,1,:) = exp(-0.5 * (grad/sr).^2) / sqrt(2*pi) / sr;
  Nr = Nr ./ max(Nr);
  %H{i,j}.Nr = Nr; %sum(Nr);
  
  % get the FT
  FNr1 = fft(Nr,2*nr,3);
  H{i,j}.FNr = FNr1(ones(ny,1,1),ones(1,nx,1),:);
  %H{i,j}.FNr3 = FNr1(ones(2*ny,1,1),ones(1,2*nx,1),:);
  
  % create the filter cube
  H{i,j}.Cube = repmat(Ny,[1 numx numr]) .* repmat(Nx,[numy 1 numr]) ...
             .* repmat(Nr,[numy numx 1]);
  H{i,j}.Cmid = [halfy; halfx; halfr];
  H{i,j}.Cdim = [numy; numx; numr];
  
  H{i,j}.numy = numy;
  H{i,j}.numx = numx;
  H{i,j}.numr = numr;
  H{i,j}.halfy = halfy;
  H{i,j}.halfx = halfx;
  H{i,j}.halfr = halfr;
end
