function X = get_parts(rgb,globals,params,conf)
% X = get_parts(rgb,globals,params,conf,par)
%
% retrieve scaled/rotated parts from the mode of the log posteriors
%
% this version assumes there is only 1 scale, but does not presume the
% caller is aware, to make it backward compatible
%
% Copyright (C) 2013 Cheng Dong Seon

numimgs = length(globals);
nParts = conf.nParts;
rots = conf.parHOG.Rots;
nRots = length(rots);

% extract the parts themselves
X = cell(nParts,1);
for jj = 1:numimgs
  scale = conf.parHOG.Scale;
  scaledSize = round(scale * conf.part_dims)-1;
  scaledCorner = -0.5 * scale * conf.part_dims;
  roundCorner = round(scaledCorner);

  % pre-compute image rotations for all used angles: if more than one part
  % is rotated the same amount, we gain in efficiency
  usedRots = unique(params(3,:,jj));
  preR = cell(nRots,1);
  Tvec = cell(nRots,1);
  for ridx = usedRots
    rotAngle = rots(ridx);
    if rotAngle
      % rotate the whole image
      [preR{ridx},Tvec{ridx}] = ...
        util.imrot_fftbased(rgb(:,:,:,globals(jj)),rotAngle);
      preR{ridx}(preR{ridx}<0) = 0; preR{ridx}(preR{ridx}>255) = 255;
    else % rotAngle == 0, so don't rotate
      preR{ridx} = double(rgb(:,:,:,globals(jj)));
    end
  end
  
  for pidx = 1:nParts
    
    iy = params(1,pidx,jj);
    ix = params(2,pidx,jj);
    ridx = params(3,pidx,jj);
    rotAngle = rots(ridx);

    % retrieve the RECT to crop
    if rotAngle
      Rmat = [cosd(rotAngle) sind(rotAngle); ...
             -sind(rotAngle) cosd(rotAngle)];
      corner = Rmat * [ix; iy] + Tvec{ridx} + roundCorner(pidx,:)';
      % ix,iy : position of middle of part
      % roundCorner : minus half-size of part
      rect = [round(corner)' scaledSize(pidx,:)]; % part bounding box
    else % rotAngle == 0, so don't rotate
      rect = [[ix iy]+roundCorner(pidx,:) scaledSize(pidx,:)];
    end
        
    % crop the desired RECT, and if it fails then PAD + CROP
    C = imcrop(preR{ridx},rect);
    pad = abs(size(C) - [scaledSize(pidx,[2,1])+1 3]);
    if any(pad)
      B = padarray(preR{ridx},pad,'symmetric','both');
      rect(1:2) = rect(1:2) + pad([2,1]);
      C = imcrop(B,rect);
    end
    
    % store the sub-image
    X{pidx}(:,:,:,jj) = C;
  end
end
