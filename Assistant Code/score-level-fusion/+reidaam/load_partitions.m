function [ped,probes,gallery] = load_partitions(dset,expr,xval)
%
% Load or create train/test randomized partitions.
%
% Copyright (C) 2013 Cheng Dong Seon

fname = ['MAT/_PARTITIONS_/PARTITIONS_' expr.Name '_' ...
  expr.Sett num2str(expr.Shots) '_' xval.Type '.mat'];

if expr.LoadPart && exist(fname, 'file')
  % use the saved reference partitions
  load(fname, 'ped','probes','gallery');
  disp('Partitions loaded from file.');
else
  fprintf('Creating new random partitions...');
  
  % create the output variables
  nPed = length(dset);
%   probes = cell(xval.Runs,nPed);
%   gallery = cell(xval.Runs,nPed);
  
  % create the specified number of randomized test runs
  for tt = 1:xval.Runs
    [thisped,thisprob,thisgall] = reidaam.make_partitions(dset,expr,xval);
    ped(tt,:) = thisped;
    probes(tt,:) = thisprob;
    gallery(tt,:) = thisgall;
  end

  % save the new partitions
  save(fname, 'ped','probes','gallery');
  disp('done.');
end
