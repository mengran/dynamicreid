function [msk,partmsk] = make_CPSmasks(rgb,ped,run,par)
% [msk,partmsk] = make_CPSmasks(rgb,ped,run,par)
%
% Retrieve pedestrian masks with the CPS algorithm.
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

import reidaam.*;

% create the config file and info
conf = load_conf(par);
[H,W,~,nImages] = size(rgb);
conf.nImages = nImages;

runstr = ['_' num2str(run,'%03d')];
namefile = [par.Dir '/CPSmasks' runstr '.mat'];
if exist(namefile,'file') && ~par.IgnoreSavefiles  && ~par.ContinueIters
  load(namefile);
else
  % starting iteration
  if isfield(par,'StartIter')
    startit = par.StartIter;
  else
    startit = 1;
  end
  
  if startit == 1
    % load starting pose estimations and select the relevant ones
    ALL = load([conf.PoseDir '/MAPestimates.mat']);
    MAP = ALL.MAP(:,:,[ped.global]);
  else
    % load previous pose estimations
    load([conf.PoseDir '/MAPestimates' num2str(startit-1) runstr '.mat']);
  end
  
  % total number of iterations
  if isfield(par,'Iters'), iters = par.Iters; else iters = 3; end
  
  conf.AppearanceDir = [conf.AppearanceDir runstr];
  util.chkmkdir(conf.AppearanceDir);
  
  % iterate between appearance modeling and object finder
  for it = startit:iters
    itstr = num2str(it);
    
    % calculate appearance model
    file1 = [conf.AppearanceDir '/paramsGaussian' itstr '.mat'];
    if exist(file1,'file') && ~par.IgnoreSavefiles
      load(file1);
    else
      fprintf('(It %d) Modeling appearance...',it); tic;
      [Mu,Cv] = model_appearances(rgb,MAP,ped,conf,par); toc
      save(file1,'Mu','Cv');
    end
    
    % calculate evidence maps (overwrite previous evidence)
    fprintf('(It %d) Calculating evidence...',it); tic
    map_evidence(rgb,Mu,Cv,ped,conf,par); toc
    
    % estimate body pose (use overwritten evidence)
    file2 = [conf.PoseDir '/MAPestimates' itstr runstr '.mat'];
    if exist(file2,'file') && ~par.IgnoreSavefiles
      load(file2);
    else
      fprintf('(It %d) Estimating body pose with appearance...',it); tic
      MAP = estimate_pose(H,W,ped,conf,par,itstr); toc
      save(file2,'MAP');
    end
  end
  
  % create the masks
  [msk,partmsk] = calc_segmentations(MAP,H,W,ped,conf);
  save(namefile,'msk','partmsk');
end
