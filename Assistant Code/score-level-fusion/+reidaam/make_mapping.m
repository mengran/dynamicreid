function [R,S,V] = make_mapping(Rdims,Sdims,xyshift,rshift,step,ROT)
%
% Create the R --> S mapping for matrix shifts
% It creates source and target locations representing a sort of circshift
% operation without the circular wrapping
%
% Copyright (C) 2013 Cheng Dong Seon

R = [];
S = [];
V = zeros(3,Rdims(3));
for rr = 1:Rdims(3) % for all rotations in the source domain
  % retrieve the spatial translation
  vector = ROT{rr} * xyshift;
  shift = [flipud(round(vector ./ step)); rshift]';
  
  % store the shift vector
  V(:,rr) = shift;
  
  % if the target domain coordinates are outside the available range, avoid
  % creating mappings
  if rr+rshift < 1, continue; end
  if rr+rshift > Sdims(3), continue; end
  
  % retrieve the source domain
  start = max(1, 1 - shift);
  finish = min(Rdims, Rdims - shift);
  
  temp = zeros(Rdims(1:2));
  temp(start(1):finish(1),start(2):finish(2)) = 1;
  [sub1,sub2] = find(temp);
  R = [R; sub2ind(Rdims,sub1,sub2,rr*ones(length(sub1),1))];
  
  % retrieve the corresponding target domain coordinates
  start = max(1, 1 + shift);
  finish = min(Sdims, Sdims + shift);
  
  temp = zeros(Sdims(1:2));
  temp(start(1):finish(1),start(2):finish(2)) = 1;
  [sub1,sub2] = find(temp);
  S = [S; sub2ind(Sdims,sub1,sub2,(rr+rshift)*ones(length(sub1),1))];
  
end
