function evimap = calc_evidence(rgb,Mu,Cv,MASK,flipMASK,conf,par)
%
% This function calculates the evidence maps obtained by evaluating the
% given Gaussians on each region of the given images.
%
% 180113 removed all handling of scales, probabilities are normalized
%        for each part
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.

[nRows,nCols,chans,numimg] = size(rgb);
nRots = conf.parHOG.nRots;
rots = conf.parHOG.Rots;
nParts = conf.nParts;
scaledSize = round(conf.parHOG.Scale * conf.part_dims);
extsize = round(max(sqrt(sum(scaledSize.^2,2))));
fftSize = [nRows,nCols] + extsize;
padval = 4; % extend image bounday by 4 pixels mirrored
minvar = par.MinVar;
area = prod(scaledSize,2) * chans; % number of pixels in the part

% allocate flipped matrices for FFT-based correlation
piCovFFT = zeros(fftSize(1),fftSize(2),chans,nParts,nRots,'single');
pMiCovFFT = zeros(fftSize(1),fftSize(2),chans,nParts,nRots,'single');
Nconst = zeros(nParts,nRots,'single'); % constant scalar for normpdf
midr = padval * ones(nParts,nRots); midc = midr;

for pidx = 1:nParts
  for ridx = 1:nRots
    rotAngle = rots(ridx);
    
    B = Mu{pidx};
    C = Cv{pidx};
    if rotAngle
      % rotate the mean and variances in the opposite direction
      B = util.imrot_fftbased(B,-rotAngle);
      C = util.imrot_fftbased(sqrt(C),-rotAngle);
      B(B<0) = 0; B(B>255) = 255;
      C = C .^ 2;
    end
    C(C<minvar) = minvar;
    flipMu = B(end:-1:1,end:-1:1,:);
    flipCv = C(end:-1:1,end:-1:1,:);
    
    % this is the constant part of the MVN pdf:
    %  -0.5*log(2piC) -0.5*(mu^2/C)
    % summed over all pixels in the mask and all channels
    Nconst(pidx,ridx) = -0.5*sum(MASK{pidx,ridx}(:) .* B(:).^2 ./ C(:))...
                        -0.5*sum(MASK{pidx,ridx}(:) .* log(2*pi*C(:)));
    flipiCv = flipMASK{pidx,ridx} ./ flipCv;
    flipMiCv = flipMASK{pidx,ridx} .* (flipMu ./ flipCv);
    
    % pre-compute FFT transforms
    piCovFFT(:,:,:,pidx,ridx) = fft(fft(flipiCv,fftSize(1),1),fftSize(2),2);
    pMiCovFFT(:,:,:,pidx,ridx) = fft(fft(flipMiCv,fftSize(1),1),fftSize(2),2);

    % calculate the cropping corner after the iFFT
    [nR,nC,~] = size(flipMu);
    midr(pidx,ridx) = midr(pidx,ridx)+floor(0.5*nR);
    midc(pidx,ridx) = midc(pidx,ridx)+floor(0.5*nC);
    
  end
end


evimap = zeros(nRows,nCols,nRots,nParts,numimg,'single');
for jj = 1:numimg
  % use the half of the longest diagonal of parts as the padding
  % value for FFT, making sure we need to calc the FFT only once here
  % and it will be valid for every part size
  AA = padarray(rgb(:,:,:,jj),[padval padval 0],'symmetric','both');
  fftX  = fft(fft(AA,fftSize(1),1),fftSize(2),2);
  fftXX = fft(fft(AA.^2,fftSize(1),1),fftSize(2),2);

  % multiply and sum simultaneously all parts and rotations
  XX = sum(bsxfun(@times,piCovFFT,fftXX),3);
  XM = sum(bsxfun(@times,pMiCovFFT,fftX),3);
  
  % reverse FFT
  SUMS = - 0.5 * real(ifft(ifft(XX,[],2),[],1)) ...
         + real(ifft(ifft(XM,[],2),[],1));

  for pidx = 1:nParts
    rotmap = zeros(nRows,nCols,nRots,'single');
    for ridx = 1:nRots
      %E = -0.5*sumXX + sumXM + Nconst{pidx,ridx}; % normpdf
      %E = SUMS(:,:,1,pidx,ridx) + Nconst(pidx,ridx);
      
      % extract only the valid region in the FFT
      %rotmap(:,:,ridx) = E(midr:midr+nRows-1,midc:midc+nCols-1) ./ area(pidx);
      rotmap(:,:,ridx) = ...
        SUMS(midr(pidx,ridx):midr(pidx,ridx)+nRows-1,...
             midc(pidx,ridx):midc(pidx,ridx)+nCols-1,1,pidx,ridx) ...
        + Nconst(pidx,ridx);
    end
    
    % reduce the numerical range of the results by normalizing by the
    % number of part pixels
    evimap(:,:,:,pidx,jj) = rotmap ./ area(pidx);
  end
end
