function [ hist ] = extractBVThist(Img_HSV, NBins, ValBlack, SatGray, WeightGrays)
%EXTRACTBVTHIST Extract BVT-histogram of a HSV image.
%   HIST = EXTRACTBVTHIST(IMG_HSV,NBINS,VALBLACK,SATGRAY,WEIGHTGRAYS) 
%   returns the BVT histogramm as shown by Cheng et. al. (2011)
%   
%   IMG_HSV has to be a HSV-color image of either MxNx3 or Mx3. NBINS has to be
%   three componenent vector specifying the number of bins per channel.
%   
%   Reference: D. S. Cheng, et al. "Custom Pictorial Structures for 
%   Re-identification." BMVC. Vol. 2. No. 5. 2011.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    validateattributes(Img_HSV,{'single','double','uint8'},...
        {'real','nonempty'},mfilename,'Img',1);
    
    if isinteger(Img_HSV)
        Img_HSV = single(Img_HSV)/255;
    end

    if ndims(Img_HSV) == 3
        Hue = Img_HSV(:,:,1);
        Sat = Img_HSV(:,:,2);
        Val = Img_HSV(:,:,3);
    elseif ndmis(Img_HSV) == 2 && size(Img_HSV,2) == 3
        Hue = Img_HSV(:,1);
        Sat = Img_HSV(:,2);
        Val = Img_HSV(:,3);
    end
    
    % Find pixels that are probably padded with zeros. These synthetic
    % clean black pixels would otherwise disturb the BVT hist.
    PadBlack = (Hue == 0) & (Sat == 0) & (Val == 0);
    PadBlack = PadBlack(:);
    
    % for the histogram no structural information is required
    Hue = Hue(:);
    Hue = Hue(~PadBlack);
    Sat = Sat(:);
    Sat = Sat(~PadBlack);
    Val = Val(:);
    Val = Val(~PadBlack);
    
    % correct the saturation of black pixels, so that it is never higher than
    % the brightness level, to avoid weird dark pixels strongly hued
    Black = Val < ValBlack;
    Sat(Black) = min(Sat(Black),Val(Black));
    
    % retrieve the map of pixels considered to be graylevel: the black pixels
    % + those with saturation less than the given threshold
    Gray = Black | (Sat < SatGray);
    Cmap = ~Gray;
    
    % pre-calculate certain weights based on the given cutoff threshold:
    % WpropSat is proportional to the saturation values with SatGray = 1, and
    % it is meant to be used only for values < SatGray
    WpropSat = Sat / SatGray;

    % store the rescaled HSV values
    Hue = Hue * NBins(1) - 0.5;
    Sat = Sat * NBins(2) - 0.5;
    Val = Val * NBins(3);
    
    %%
    chromarea = NBins(1)*NBins(2);
    
    % retrieve the map of gray values, the gray votes interpolated between
    % neighboring bins, the color votes bilinearly interpolated, the
    % saturation weights for dark colors
    V = histInterp2(Val,NBins(3));
    T = histInterp2D(Hue,Sat,NBins(1),NBins(2));
    
    % count the graylevel pixels
    gmask = Gray;
    grays = accumarray([V{1}(gmask,1); NBins(3)],[V{2}(gmask,1); 0])...
        + accumarray([V{1}(gmask,2); NBins(3)],[V{2}(gmask,2); 0]);
    
    % add the residual colors from the gray pixels
    ww = WpropSat(gmask);
    colors = accumarray([T{1}(gmask,1); chromarea],[T{2}(gmask,1).*ww; 0])...
        + accumarray([T{1}(gmask,2); chromarea],[T{2}(gmask,2).*ww; 0])...
        + accumarray([T{1}(gmask,3); chromarea],[T{2}(gmask,3).*ww; 0])...
        + accumarray([T{1}(gmask,4); chromarea],[T{2}(gmask,4).*ww; 0]);
    
    % count the remaining color pixels
    cmask = Cmap;
    if any(cmask)
        colors = colors ...
            + accumarray([T{1}(cmask,1); chromarea],[T{2}(cmask,1); 0]) ...
            + accumarray([T{1}(cmask,2); chromarea],[T{2}(cmask,2); 0]) ...
            + accumarray([T{1}(cmask,3); chromarea],[T{2}(cmask,3); 0]) ...
            + accumarray([T{1}(cmask,4); chromarea],[T{2}(cmask,4); 0]);
    end
    
    % normalize, apply the part weight and store
    vector = [grays*WeightGrays; colors*(1-WeightGrays)];
    hist = vector ./ sum(vector);
    
end

function V = histInterp2(vvec,levels)
% calculate the histogram of the given values using interpolation to avoid
% aliasing
    lmax = levels - 1;
    num = numel(vvec);

    binc = (1:lmax) - 0.5;
    xmat = binc(ones(num,1),:);

    V{1} = ones(num,2,'uint8');  % indices
    V{2} = zeros(num,2,'single'); % fractions

    % case 1 and 2
    select = vvec < 0.5;
    V{1}(select,1) = 1; V{2}(select,1) = 1; % vote bin 1 with weight 1
    select = vvec >= levels-0.5;
    V{1}(select,1) = levels; V{2}(select,1) = 1; % vote bin 'levels' with 1
    % case 3
    vmat = vvec(:,ones(1,lmax));
    fmat = vmat - xmat;
    [select,bin] = find(fmat == 0);
    V{1}(select,1) = bin; V{2}(select,1) = 1; % vote 'bin' with 1
    % case 4
    fpos = find(fmat > 0 & fmat < 1);
    fval = fmat(fpos);
    [select,bin] = ind2sub([num lmax],fpos);
    V{1}(select,1) = bin; V{2}(select,1) = 1-fval; % vote 'bin' with 1-fval
    V{1}(select,2) = bin+1; V{2}(select,2) = fval; % vote 'bin+1' with fval
end

function T = histInterp2D(rvec,cvec,height,width)
% calculate the histogram of the given values using interpolation to avoid
% aliasing - 2D version
    rmax = height - 1; cmax = width - 1;
    num = numel(rvec);

    T{1} = ones(num,4,'uint16');  % indices
    T{2} = zeros(num,4,'single'); % fractions

    rbinl = floor(rvec); % row bin lower coordinate
    cbinl = floor(cvec); % col bin lower coordinate
    rbinu = rbinl + 1;   % row bin upper coordinate
    cbinu = cbinl + 1;   % col bin upper coordinate
    distr = rvec - rbinl;   % distance to start of row bin
    distc = cvec - cbinl;   % distance to start of col bin

    % fold the edges
    rbinl(rbinl == -1) = 0;
    cbinl(cbinl == -1) = 0;
    rbinu(rbinu == height) = rmax;
    cbinu(cbinu == width) = cmax;

    % first quadrant (NW)
    T{1}(:,1) = rbinl + cbinl * height + 1;
    T{2}(:,1) = (1-distr) .* (1-distc);
    % second quadrant (SW)
    T{1}(:,2) = rbinu + cbinl * height + 1;
    T{2}(:,2) = distr .* (1-distc);
    % third quadrant (NE)
    T{1}(:,3) = rbinl + cbinu * height + 1;
    T{2}(:,3) = (1-distr) .* distc;
    % third quadrant (SE)
    T{1}(:,4) = rbinu + cbinu * height + 1;
    T{2}(:,4) = distr .* distc;
    
    T{1} = min(T{1},height*width);
    T{1} = max(T{1},1);
end

% function V = histInterp2(vvec,levels)
% % calculate the histogram of the given values using interpolation to avoid
% % aliasing
% lmax = levels - 1;
% num = numel(vvec);
% 
% binc = (1:lmax) - 0.5;
% xmat = binc(ones(num,1),:);
% 
% V{1} = ones(num,2,'uint8');  % indices
% V{2} = zeros(num,2,'single'); % fractions
% 
% % case 1 and 2
% select = vvec < 0.5;
% V{1}(select,1) = 1; V{2}(select,1) = 1; % vote bin 1 with weight 1
% select = vvec >= levels-0.5;
% V{1}(select,1) = levels; V{2}(select,1) = 1; % vote bin 'levels' with 1
% % case 3
% vmat = vvec(:,ones(1,lmax));
% fmat = vmat - xmat;
% [select,bin] = find(fmat == 0);
% V{1}(select,1) = bin; V{2}(select,1) = 1; % vote 'bin' with 1
% % case 4
% fpos = find(fmat > 0 & fmat < 1);
% fval = fmat(fpos);
% [select,bin] = ind2sub([num lmax],fpos);
% V{1}(select,1) = bin; V{2}(select,1) = 1-fval; % vote 'bin' with 1-fval
% V{1}(select,2) = bin+1; V{2}(select,2) = fval; % vote 'bin+1' with fval
% end
% 
% function V = histInterp2D(rvec,cvec,height,width)
% % calculate the histogram of the given values using interpolation to avoid
% % aliasing - 2D version
% rmax = height - 1; cmax = width - 1;
% num = numel(rvec);
% 
% V{1} = ones(num,4,'uint16');  % indices
% V{2} = zeros(num,4,'single'); % fractions
% 
% rbinl = floor(rvec); % row bin lower coordinate
% cbinl = floor(cvec); % col bin lower coordinate
% rbinu = rbinl + 1;   % row bin upper coordinate
% cbinu = cbinl + 1;   % col bin upper coordinate
% distr = rvec - rbinl;   % distance to start of row bin
% distc = cvec - cbinl;   % distance to start of col bin
% 
% % fold the edges
% rbinl(rbinl == -1) = 0;
% cbinl(cbinl == -1) = 0;
% rbinu(rbinu == height) = rmax;
% cbinu(cbinu == width) = cmax;
% 
% % first quadrant (NW)
% V{1}(:,1) = rbinl + cbinl * height + 1;
% V{2}(:,1) = (1-distr) .* (1-distc);
% % second quadrant (SW)
% V{1}(:,2) = rbinu + cbinl * height + 1;
% V{2}(:,2) = distr .* (1-distc);
% % third quadrant (NE)
% V{1}(:,3) = rbinl + cbinu * height + 1;
% V{2}(:,3) = (1-distr) .* distc;
% % third quadrant (NE)
% V{1}(:,4) = rbinu + cbinu * height + 1;
% V{2}(:,4) = distr .* distc;
% 
% end