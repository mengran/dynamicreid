function filter = getGarborFilter(nGaussianScales, nOrientations, scid, ...
    sizeFilter, sigma2, p)
%GABORFILTERBANK Generates a Gabor-filter based filterbank
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if nargin < 5
        % influence on gaussian envelope
        % mainly influences the initial width
        sigma2 = (2*pi)^2;
    end
    
    if nargin < 6
        % influence on gaussian envelope
        % mainly influences the rate of growth
        p = 4;
    end
    
    % influence on wavelength
    frequency = 0.4;

    kb = floor(sizeFilter/2);
    [X,Y] = meshgrid(-kb:kb);

    filter = cell(nOrientations,1);
    % scale of the gaussian envelope
    alpha = (2^(-((scid-1)/p+2)/2)) * pi;
    for mu=1:nOrientations
        % orientation of the complex plane wave
        phi = (mu-1)*pi/nOrientations;
        % wave vector
        beta = alpha*exp(1i*phi);
%         Xrot =  real(beta)*X+imag(beta)*Y;
%         Yrot = -imag(beta)*X+real(beta)*Y;
        Xrot =  cos(phi)*X+sin(phi)*Y;
        Yrot = -sin(phi)*X+cos(phi)*Y;
        Z = Xrot.^2+Yrot.^2;
        % final filter
        filter{mu} =  (abs(beta).^2)/sigma2 .*...           % prescaling
            exp(-(abs(beta)^2)*Z/(2*sigma2)) .*...          % gaussian envelope
                (exp(frequency*1i*alpha*Xrot)-exp(-sigma2/2));    % wave
    end

end
