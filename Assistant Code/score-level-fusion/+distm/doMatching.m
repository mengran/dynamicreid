function [matchingtables] = doMatching(feature,distfuns)
%DOMATCHING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    partnames = fieldnames(feature);
    
    nImages = length(feature.(partnames{1}));
    nFuns = length(distfuns);
    nParts = length(partnames);
    
    matchingtables = zeros(nImages,nImages,nParts,nFuns,'single');
       
    hh = waitbar(0,'');
    % for each part match each feature against all other parts of the same
    % type using the given distance functions in distfuns
    for partidx=1:nParts
        for galidx=1:nImages
            progress = ((partidx-1)*nImages+galidx)/nImages/nParts;
            waitbar(progress,hh,sprintf('Part %d of %d',partidx,nParts));
            % representation of gallery image
            galFeature = feature.(partnames{partidx}){galidx};
            for probidx=1:galidx-1
                % representation of probe image
                probFeature = feature.(partnames{partidx}){probidx};
                for funidx=1:nFuns
                    % distance computation between both representations
                    dist = abs(distfuns{funidx}(galFeature,probFeature));
                        if isnan(dist)
                           warning('Encountered NaN in distance computation with %s! ',...
                               func2str(distfuns{funidx})); 
                        end % NaN check
                    % symmetric matching table calculation
                    matchingtables(galidx,probidx,partidx,funidx) = single(dist);
                    matchingtables(probidx,galidx,partidx,funidx) = single(dist);    
                end % for each given distance function
            end % for each probe image
        end % for each gallery image
    end % for each part
    
%     for partidx=1:nParts
%         for k=1:nFuns
%             lowerTable = tril(matchingtables(:,:,partidx,k),-1);
%             matchingtables(:,:,partidx,k) = ...
%                 lowerTable + lowerTable';
%         end
%     end
    
    if ishandle(hh)
        close(hh)
    end

end

