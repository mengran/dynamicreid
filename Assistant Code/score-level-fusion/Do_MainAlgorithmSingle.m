%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Person Re-identification by Articulated Appearance Matching
%
% Common script run by all single-shot experiments,
% to launch the full re-id pipeline
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (C) 2013 Cheng Dong Seon
%
% This file is part of REIDAAM.
% REIDAAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% REIDAAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with REIDAAM.  If not, see <http://www.gnu.org/licenses/>.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% 
% This file has been modified to feature a broader width of feature
% extraction and matching techniques. Modifications are not limited to this
% file.
%                                                                         
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:   
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based  
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),  
% Killarney, Ireland, pp. 469-476, IEEE 2015.                             
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import reidaam.*;

fprintf('Started experiment #%s on %s-dataset.\n',...
    expr.Num,expr.Name);

% Directory to store all the experimental results
parCPS.Dir = ['../results/' expr.Name '_Exp' expr.Num]; % dir to save stuff

% Check if the directory to save all (intermediate) results exists
util.chkmkdir(parCPS.Dir);

% Experiment identifier
expr.MAT = ['_' expr.Name '_x' expr.Num '.mat'];

% diary(fullfile(parCPS.Dir,['Logfile' expr.MAT(1:end-4)]));

% Override warning state
warning('on','all');

% clear variables that will not be used any further
expr.clearMemory = 1;
if expr.clearMemory
    warning(['Unused variables will be cleared and are '...
        'not available for further experiments.']);
end


% Force the recalculation of the final matching and the correlation table
expr.RecalcFinal = 1;
warning('Final matching will be recalculated!');
% % prevent the calculation of feature SDC
% expr.SDCFeat = 0;
% warning('SDC not active!');

% check if we can use parallel processing and the user wants it
util.chkparallel(4);
% warning('Parallel Computing Toolbox disabled!');

% start time measure
startrun = clock;

%% Setup input/output
% retrieve the dataset pedestrian and image indices
nImages = length(data.Files);
dset    = make_index(data);
nPed    = length(dset);
parColor.NoDisplay = false;
if xval.TuneParams, xval.RecalcDistances = true; end
if ~isfield(xval,'prelfun'), xval.prelfun = @calc_gc; end

% load the dataset
util.chkmkdir('MAT/_DATASETS_');
fname = ['MAT/_DATASETS_/dataset_' expr.Name '_' num2str(W) 'x' ...
  num2str(H) '_f' num2str(data.Scale) '.mat'];
data.RGB = load_images(data,H,W,fname);
% get the file extension of the files
[~,~,data.Ext] = fileparts(data.Files(1).name);

% load test partitions
[ped,probes,gallery] = load_partitions(dset,expr,xval);

% pre-compute some GC auxiliary info
data = xval.prelfun(data,parColor);

% output file directories
% util.chkmkdir('ResultsMAT');
% util.chkmkdir('ResultsFIG');
util.chkmkdir([parCPS.Dir '/matching'])
util.chkmkdir([parCPS.Dir '/features'])
util.chkmkdir([parCPS.Dir '/part_images'])
util.chkmkdir([parCPS.Dir '/features'])

% mask for genuine scores in matching table
fname = [parCPS.Dir '/matching/genuineMask' expr.MAT];
if exist(fname,'file');
    fprintf('Genuine mask already computed.\n');
else
    fprintf('Generate mask for genuine scores...')
    genmask.(['genuineMask_' expr.Name]) = fusion.getGenuineMask(data.Dir,data.Ext);
    save(fname,'-struct','genmask');
    fprintf('OK\n');
end

%% Calculate masks with PS
% in the single shot case, the masks and subsequent features can be
% pre-computed because they do not depend on the data partitions
[maskset,pmskset] = xval.maskfun(data.RGB,dset,[],expr,parCPS,0);

% Save mask images to file
if isfield(expr,'SaveMask') && expr.SaveMask
    try
        fprintf('Save mask images to file...');
        util.save_maskimages(dset,parCPS,expr);
        fprintf('OK\n');
    catch err
        disp(['SaveMask: ' err.message]);
    end
end

% Save pose images to file
if isfield(expr,'SavePose') && expr.SavePose
    try
        fprintf('Save pose images to file...');
        save_poseimages(data,dset,parCPS);
        fprintf('OK\n');
    catch err
        disp(['SavePose: ' err.message]);
    end
end

%% Extract bodyparts for further processing
imdir = [parCPS.Dir '/part_images'];
fname = [imdir '/' 'bodyparts' expr.MAT];
if exist(fname,'file')
    % Load the already calculated parts from file
    fprintf('Load bodyparts from file...');
    load(fname,'bodyparts');
    fprintf('OK\n');
else
    stime = clock;
    fprintf('Bodypart extraction started...');
    % Extract the parts from the images
    bodyparts = util.extract_bodyparts(data,dset,parCPS);
    duration = util.getDurationString(stime);
    % Save to file
    util.chkmkdir(imdir);
    save(fname,'bodyparts');
    fprintf('OK\n');
end

% Save bodyparts to file
if isfield(expr,'SaveParts') && expr.SaveParts
    try
        fprintf('Save part images to file...');
        util.save_bodypartimages(bodyparts,dset,parCPS);
        fprintf('OK\n');
    catch err
        disp(['SaveParts: ' err.message]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Feature Extraction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nFeature extraction:\n')

% Calculate SDALF-features and image division
if isfield(expr,'SDALFFeat') && expr.SDALFFeat
%     try
        stime = clock;
        
        sdalf.SDALF_SvsS(data,expr,parCPS,parMSCR);
        
        fprintf('Time taken: %s\n',util.getDurationString(stime));
%     catch err
%         disp(['SDALF: ' err.message]);
%     end
end

% Features similar to "Unsupervised Salience Learning for Person
% Re-identification" by Zhao et. al.
if isfield(expr,'SDCFeat') && expr.SDCFeat
    fname = [parCPS.Dir '/matching/matching_SDC_full' expr.MAT];
    if exist(fname,'file')
        fprintf('-> Feature ''%s'' already computed.\n','SDC');
    else
        fprintf('-> Extract feature ''%s''...','SDC');
        try
            stime = clock;
            matching = sdc.Salience_SvsS(data,expr,parCPS);
            save(fname,'matching');
            fprintf('OK (%s)\n',util.getDurationString(stime));
        catch err
            disp(['SDC: ' err.message]);
        end
    end
end

% feature extraction per part
dirfeatures = [parCPS.Dir '/features'];
util.chkmkdir(dirfeatures);

if exist([dirfeatures '/features' expr.MAT],'file')
    load([dirfeatures '/features' expr.MAT],'featprops');
    fprintf('Loaded feature list from file.\n');
else
	% format: {Name,Type,Flag}
	% Name: BVT, ELF, MR8, LBP, Lab, wHSV, BiCov, SELF
	% Type: vec (=vector), hist (=histogram)
	% Flag: 0 (calculate for parts), 1 (calculate for whole image)
%     featprops = {{  'BVT','hist',0,1},{  'ELF','hist',0,3},...
% 				 {  'MR8','hist',0,5},{  'LBP','hist',0,1},...
% 				 { 'wHSV','hist',0,1},{  'Lab','hist',0,3},...
% 				 {'BiCov', 'vec',0,1},...
% 				 { 'SELF','hist',1,5},{'BiCov', 'vec',1,1}};
    % legend: BVT:   Bhattacharya     ELF:   Jensen-Shannon-Divergence
    %         MR8:   Manhattan        LBP:   Bhattacharya
    %         wHSV:  Bhattacharya     Lab:   Jensen-Shannon-Divergence
    %         SELF:  Manhattan        BiCov: Manhattan
    featprops = {{  'BVT','hist',0,1},{  'ELF','hist',0,3},...
				 {  'MR8','hist',0,5},{  'LBP','hist',0,1},...
				 { 'wHSV','hist',0,1},{  'Lab','hist',0,3},...
				 { 'SELF','hist',1,5}};
    % save list to file
    save([dirfeatures '/features' expr.MAT],'featprops');
    % This list will be used later to determine which features are going to
    % be loaded from file for computing of matching tables.
end

% Extract Lab, BVT, ELF, MR8, LBP and wHSV hists for all bodyparts as 
% well as their respective BiCov features. For the whole image compute 
% SELF hists and BiCov features.
for i=1:length(featprops)
    try
        if featprops{i}{3} == 1
            featname = [featprops{i}{1} '_full'];
        else
            featname = featprops{i}{1};
        end
        fname = [dirfeatures '/' featname expr.MAT];

        if exist(fname,'file')
            if expr.clearMemory
                fprintf('-> Feature ''%s'' already computed.\n',featname);
            else
                fprintf('-> Load feature ''%s'' from file...',featname);
                load(fname,featname);
                fprintf('OK\n');
            end
        else
            fprintf('-> Extract feature ''%s''...',featname);
            stime = clock;
            % compute feature
            feature.(featname) = ...
                extractAdditionalFeature(data,bodyparts,parCPS,parBVT,...
                parELF,parLab,parMR8,parColor,featprops{i});
            % save feature to file
            save(fname,'-struct','feature',featname)

            % print some information to console
            duration = util.getDurationString(stime);
            fprintf('OK (%s)\n',duration);
            clear feature;
        end
    catch err
        disp(['Feature ''' featname ''': ' err.message]);
    end
end

if expr.clearMemory
    clear bodyparts; % should not be needed afterwards
end
% 

% Load/Calculate distances w/ or w/o parameters tuning - Matching
% extract MSCR features
dist = extract_mscr([],data,maskset,dset,parCPS,parMSCR,0);

% if the entire dataset is going to be used for cross-validation, then
% it is faster to calculate all the features and distances first, then
% select the relevant ones later in each randomize partition
% if strcmp(xval.Type,'gong') || strcmp(xval.Type,'VIPeR')
fname = [parCPS.Dir '/distances_000.mat'];
if exist(fname,'file') && ~xval.RecalcDistances
    load(fname);
else
    dist = xval.featfun(dist,data,pmskset,dset,parColor);
    dist = xval.distfun(dist,parColor);
    dist = calc_distance_mscr(dist,parMSCR);

    % save the distances
    save(fname,'dist');
end
% end
if expr.clearMemory
    clear pmskset;
end
fprintf('\n');

data = rmfield(data,{'RGB','H','S','V'});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Matching
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dirmatch = [parCPS.Dir '/matching'];
util.chkmkdir(dirmatch)

% distance computation
% distance functions for histogram like features
histDistFuns = {@distm.bhattacharyya,@distm.chisquared,@distm.cosine,...
    @distm.jensenshannon,@distm.manhattan,@distm.euclidean};
% distance functions for vector like features
vecDistFuns = {@distm.manhattan,@distm.euclidean};

fprintf('Matching:\n');
startmatch = clock;

for i=1:length(featprops)
    try
        % get the name of the feature 
        if featprops{i}{3} == 1
            featname = [featprops{i}{1} '_full'];
        else
            featname = featprops{i}{1};
        end
        % name of the corresponding matching table
        fnamematch = [dirmatch '/matching_' featname expr.MAT];

        % Check if the table was already computed
        if exist(fnamematch,'file')
            if expr.clearMemory
                fprintf('-> Matching tables for ''%s'' already computed.\n',featname);
            else
                fprintf('-> Load matching table for ''%s'' from file...',featname);
                load(fnamematch,featname);
                fprintf('OK\n');
            end
        else
            fprintf('-> Compute matching tables for ''%s''...',featname);
            % load the feature from file
            fname = [dirfeatures '/' featname expr.MAT];
            feature = util.loadFromFile(fname,featname);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % ETHZ1 sampling
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if strcmp(expr.Name,'ETHZ1')
                partnames = fieldnames(feature);
                for partidx=1:length(partnames)
                    feature.(partnames{partidx}) = ...
                        feature.(partnames{partidx})(1:2:nImages);
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % ETHZ1 sampling (End)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            stime = clock;
            % match all vs. all of the given feature
            if strcmp(featprops{i}{2},'hist')
                if length(featprops{i}) < 4
                    % compute all matching function values
                    matching = ...
                        distm.doMatching(feature,histDistFuns);
                else
                    % a single matching function was selected
                    matching = ...
                        distm.doMatching(feature,histDistFuns(featprops{i}{4}));
                end
            else
                if length(featprops{i}) < 4
                    % compute all matching function values
                    matching = ...
                        distm.doMatching(feature,vecDistFuns);
                else
                    % a single matching function was selected
                    matching = ...
                        distm.doMatching(feature,vecDistFuns(featprops{i}{4}));
                end
            end
            % save matching table to file
            save(fnamematch,'matching');
            duration = util.getDurationString(stime);
            fprintf('OK (%s)\n',duration);
            clear matching feature;
        end
    catch err
        disp(['Matching ''' featname ''': ' err.message]);
    end
end
%% Store other matching tables
% Store matching of REIDAAM
fnamereid = [dirmatch '/matching_ReIdAAM' expr.MAT];
if ~exist(fnamereid,'file')
    fprintf('-> Save matching tables of ReIdAAM...');
    ReIdAAM.MSCR = dist.M;
    ReIdAAM.BVT = dist.F;
    ReIdAAM.Final = xval.beta * dist.F + (1 - xval.beta) * dist.M;
    ReIdAAM.BVT_parts = dist.Fp;
    save(fnamereid,'-struct','ReIdAAM');
    fprintf('OK\n');
else
    fprintf('-> Matching tables of ReIdAAM already saved.\n');
end

clear ReIdAAM;

% Store matching of SDALF
try
    if isfield(expr,'SDALFFeat') && expr.SDALFFeat
        fnameSDALF = [dirmatch '/matching_SDALF' expr.MAT];
        if ~exist(fnameSDALF,'file')
            % identifier of the dataset
            suffix = [expr.Name '_f' num2str(data.Scale) '_Exp' expr.Num '.mat'];
            % SDALF wHSV
            fnamewHSV = [parCPS.Dir '/matching/wHSVmatch_' suffix];
            if exist(fnamewHSV,'file')
                SDALF.wHSV = util.loadFromFile(fnamewHSV,'final_dist_hist');
            end
            
            % SDALF RHSP/Epitexture
            fnameRHSP = [parCPS.Dir '/matching/txpatchmatch_' suffix];
            if exist(fnameRHSP,'file')
                SDALF.RHSP = util.loadFromFile(fnameRHSP,'dist_epitext');
            end
            
            fprintf('-> Save matching tables of SDALF...');
            save(fnameSDALF,'-struct','SDALF');
            fprintf('OK\n');
        else
            fprintf('-> Matching tables of SDALF already saved.\n');
        end

        clear SDALF; % final_mscr_dist
    end
catch err
    disp(['Save SDALF: ' err.message]);
end

%% Create a stack of the selected matching tables for further computations
fname = [parCPS.Dir '/matching/Tables' expr.MAT];
if ~exist(fname,'file') || (isfield(expr,'RecalcFinal') && expr.RecalcFinal)
    fprintf('-> Compute final compilation of matching tables...\n')
    stime = clock;

    tables = util.extractTablesForExperiments(parCPS,expr,nImages);
    
    % Save to file
    save([parCPS.Dir '/matching/Tables' expr.MAT],'-struct','tables','-v7.3');
    fprintf('OK (%s)\n',util.getDurationString(stime));
    if expr.clearMemory
        clear tables;
    end
else
    fprintf('-> Final compilation already computed.\n')
end

%% Create correllation table for the chosen matching tables
% % if ~exist('tables','var')
% tables = load([parCPS.Dir '/matching/Tables' expr.MAT]);
% % end
% ctname = ['corrTable_' expr.Name];
% tname = genvarname(['Tables_' expr.Name]);
% 
% if isfield(tables,ctname) && ~(isfield(expr,'RecalcFinal') && expr.RecalcFinal)
%     fprintf('-> Correlation table already computed.\n');
% else
%     fprintf('-> Calculate correlation table...')
%     genuineMask = ...
%         util.loadFromFile([parCPS.Dir '/matching/genuineMask' expr.MAT],...
%         ['genuineMask_' expr.Name]);
%     genuineMask = logical(genuineMask - eye(nImages));
% %     genuineMask = fusion.getGenuineMask(data.Dir,data.Ext);
%     
%     % sampling for large tables because computing Kendall's rank
%     % correlation is time consuming for large datasets
%     switch(expr.Name)
%         case {'ETHZ1'}
% %             samples = 1:10:nImages; % full ETHZ1
%             samples = 1:4:floor(nImages/2); % sampled EHTZ1
%         case {'ETHZ2'}
%             samples = 1:3:nImages;
%         case {'ETHZ3'}
%             samples = 1:3:nImages;
% %         case {'CAVIAR'}
% %             samples = 1:2:nImages;
%         otherwise
%             % iLIDS is small enough and VIPeR consists of pictures not
%             % taken from direct secquences
%             samples = 1:nImages;
%     end
%     genuineMask = genuineMask(samples,samples);
%     
%     stime = clock;
%     
%     nTables = length(tables.(tname));
%     corrTable.(ctname) = ones(nTables,'single');
%     hh = waitbar(0,'Calculate correlation table');
%     for i=1:nTables
%         mtable1 = tables.(tname){i}(samples,samples);
%         mtable1 = mtable1(:);
%         mtable1 = mtable1(genuineMask(:));
%         for j=1:i-1
%             mtable2 = tables.(tname){j}(samples,samples);
%             mtable2 = mtable2(:);
%             mtable2 = mtable2(genuineMask(:));
%             corrTable.(ctname)(i,j) = corr(mtable1,mtable2,'type','Kendall');
%         end
%         waitbar(i/nTables,hh);
%     end
%     save([parCPS.Dir '/matching/Tables' expr.MAT],'-struct','corrTable','-append','-v7.3');
%     fprintf('OK (%s)\n',util.getDurationString(stime));
%     
%     if ishandle(hh)
%         close(hh)
%     end
% end
% duration = util.getDurationString(startmatch);
% fprintf('Time taken: %s\n\n',duration);

%% Calculate performance by cross-validation
% fprintf('\nPerforming %d test runs --------------\n',xval.Runs);
stats = [];
for tt = 1:xval.Runs
    thisped = ped(tt,:);
    thisprob = probes(tt,:);
    thisgall = gallery(tt,:);
    
    stats = xval.xvalfun(stats,dist,thisped,thisprob,thisgall,xval,tt);
end
% fprintf('avg normalized AUC: %f\n',mean(stats.nAUC));

%% Post experiment
% show the CMC curves for all features
% plot_cmc(stats,expr.Name,expr.showEBar);

% save the experiment parameters and results
save([parCPS.Dir '/res_and_param' expr.MAT],'stats','expr','xval','-regexp','par[A-z]+');

% Write final message to console
fprintf('Experiment #%s on %s-dataset has completed.\n',...
    expr.Num,expr.Name);
fprintf('Time taken by experiment: %s\n\n',util.getDurationString(startrun));

diary off;