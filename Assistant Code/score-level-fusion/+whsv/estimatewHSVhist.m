function [varargout] = estimatewHSVhist(I,varargin)
%ESTIMATEWHSVHIST Estimate the weighted HSV histogramm of I.
%   
%   HIST = ESTIMATEWHSVHIST(I) returns the weighted HSV color histogram as
%   featured in SDALF. By default there are 10 bins per channel. The
%   gaussian kernel is set to N(M/2,M/5), with M being the width of the
%   image I. HIST will be a 30x1 vector.
%   
%   HIST = ESTIMATEWHSVHIST(I,NBINS) returns the weighted HSV color 
%   histogram as featured in SDALF. NBINS has to be a three element vector
%   containing the number of bins for each channel. HIST will be a
%   sum(NBINS)x1 vector.
%   
%   HIST = ESTIMATEWHSVHIST(I,NBINS,C) returns the weighted HSV color
%   histogram as featured in SDALF. NBINS has to be a three element vector
%   containing the number of bins for each channel. The gaussian kernel is
%   set to N(C,M/5), with M being the width if the image I. HIST will be a 
%   sum(NBINS)x1 vector.
%   
%   HIST = ESTIMATEWHSVHIST(I,NBINS,C,V) returns the weighted HSV color
%   histogram as featured in SDALF. NBINS has to be a three element vector
%   containing the number of bins for each channel. The gaussian kernel is
%   set to N(C,V). HIST will be a sum(NBINS)x1 vector.
%   
%   [H,S,V] = ESTIMATEWHSVHIST(...) can be used with any of the previous
%   settings, returning the histograms for each channel seperately instead
%   of concatenating them into one single vector. The length of these
%   vectors is as specified in NBINS or 10x1 by default.
%   
%   See also NORMPDF.
%   
%   Reference: Farenzena, Michela and Bazzani, Loris and Perina, Alessandro
%   and Murino, Vittorio and Cristani, Marco: Person Re-Identification by 
%   Symmetry-Driven Accumulation of Local Features. IEEE Computer Society.
%   San Francisco, CA. 2010
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
	import whsv.*;
	
    % return at least one thing
    nout = max(nargout,1);
    
    [H,W,D] = size(I);
    if D ~= 3
       error('I is supposed to be a MxNx3 HSV color image.') 
    end
    
    [center,varW,NBins] = parseVarargin(W,D,varargin{:});
    
    weights = gau_kernel(center,varW,H,W);
    
    wHSVhist = cell(1,3);
    for i=1:3
        ch = I(:,:,i);
        wHSVhist{i} = whistcY(ch(:),weights,0:1/(NBins(i)-1):1);
    end
    
    if nout == 1
        varargout = cell(1);
        for i=1:3
            varargout{1} = [varargout{1}; wHSVhist{i}];
        end
    else
        varargout = wHSVhist;
    end

end

function map = gau_kernel(x,varW,H,W)

    map = repmat(normpdf(1:W,double(x),varW),[H,1]);

end

function [center,varW,binCount] = parseVarargin(W,D,varargin)
    % parameters for the gauss kernel
    center = W/2;
    varW = W/5;
    % 10 Bins per channel
    binCount = 10*ones(1,D);
    
    if nargin > 2
        if numel(varargin{1}) ~= D
            error('There has to be a number of bins for each channel.');
        else
            binCount = varargin{1};
            if any(binCount < 1)
                error('The number of bins for each channel has to be > 0.');
            end
        end
    end
    if nargin > 3
        center = varargin{2};
        if center > W
            error('The symmetry line is outside of the image boundaries.');
        end
    end
    if nargin > 4
        varW = varargin{3};
        if varW < 0
            error('The variance has to be positive.');
        end
    end
end