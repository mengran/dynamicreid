function evaluateAllML2()
%EVALUATEALLML2 evaluate all fusion algorithms with metric learning (kLFDA)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, A. Vorndran                           
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc

nRuns = 10;
plotColor = 0;

type{1} = 'Decimal';
type{2} = 'DoubleSigmoid';
type{3} = 'FAR';
type{4} = 'LikelihoodRatio';
type{5} = 'LogisticRegression';
type{6} = 'MinMax';
type{7} = 'MuSigma';
type{8} = 'Tanh';

dsName{1} = 'VIPeR';
path{1} = 'VIPeRa';
suffix{1} = '.bmp';
gSize{1} = 316;

% dsName{1} = 'BaLi';
% path{1} = 'BaLi_multi';
% suffix{1} = '.png';
% gSize{1} = 33;

% dsName{2} = 'iLIDS';
% path{2} = 'i-LIDS_Pedestrian/Persons';
% suffix{2} = '.jpg';
% gSize{2} = 50;
% 
% dsName{2} = 'CAVIAR';
% path{2} = 'CAVIAR4REID';
% suffix{2} = '.jpg';
% gSize{2} = 36;
% 
% dsName{4} = 'ETHZ2';
% path{4} = 'ETHZa/seq2';
% suffix{4} = '.png';
% gSize{4} = 17;
% 
% dsName{5} = 'ETHZ3';
% path{5} = 'ETHZa/seq3';
% suffix{5} = '.png';
% gSize{5} = 14;
% 
% dsName{6} = 'ETHZ1';
% path{6} = 'ETHZa/seq1';
% suffix{6} = '.png';
% gSize{6} = 41;

% expr.Algoname = ['KISSME' '_pMetric'];%'_NoPerm'];
expr.Algoname = ['kLFDA' '_3366'];%'_NoPerm'];

warning off MATLAB:polyfit:RepeatedPointsOrRescale

for ds = 1 : length(dsName)
    dataset = dsName{ds};
    disp(['------- DATASET ', dataset, ' -------']);
    datasetPath = ['datasets/', path{ds}];
    datasetFileSuffix = suffix{ds};
    gallerySize = gSize{ds};
    mask = util.loadFromFile(['../results/', dataset, '_Exp001/matching/genuineMask_', dataset, '_x001.mat'], ['genuineMask_', dataset]);
    tables = util.loadFromFile(['../results/', dataset, '_Exp001/matching/Tables_', dataset, '_' expr.Algoname '.mat'], ['Tables_', dataset]);

    for typeIdx = 1 : length(type)
        disp(['start experiment for dataset ', dataset, ' and transformation ', type{typeIdx}]);
%         folder = ['../results/', dataset, '_Exp001/transformedTables_' expr.Algoname '/'];
        folder = ['../results/', dataset, '_Exp001/transformedTablesML/'];
        file = ['LearnTest_', dataset, '_', type{typeIdx}, '_' expr.Algoname '.mat'];
        if exist([folder, file], 'file') ~= 0
            disp('experiment already done, nothing to do');
            continue;
        end
        stime = clock;
        try
            [cmc, AUC, ER, parameters, weights, fusedMatchingTables, partitionTest] = ...
                util.evaluationML2(tables, mask, datasetPath, datasetFileSuffix, type{typeIdx}, nRuns, gallerySize, plotColor);
            util.chkmkdir(folder);
            save([folder, file], 'cmc', 'AUC', 'ER', 'parameters', 'weights', 'fusedMatchingTables', 'partitionTest');
        catch err
            try
                for s = 1 : length(err.stack)
                    message = sprintf('%s (Line %d): %s',err.stack(s).name,...
                        err.stack(s).line,err.message);
                    disp(message);
                end
            catch errCatch
                disp(errCatch.message);
            end
        end
        duration = util.getDurationString(stime);
        disp(['experiment took ', duration]);
    end
end
