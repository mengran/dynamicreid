function savevars(namefile,varargin)
% Copyright (C) 2013 Cheng Dong Seon

% store the input variables with their outside workspace name as fields of
% a structure to be saved
varnum = size(varargin,2);
for ii = 1:varnum
  vars.(inputname(ii+1)) = varargin{ii};
end

save(namefile,'-struct','vars');

