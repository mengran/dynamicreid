function [train, test] = getPartitioning(imageFolder, nRuns, gallerySize, fileExtension, badImageRemovalHack)
%GETPARTITIONING
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, A. Vorndran                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% default parameters
if nargin < 5
    badImageRemovalHack = 0; % do not cheat (in contrast to others)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Index the dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
currentFolder = pwd;                        % remember current directory
cd(imageFolder);                            % go to dataset directory
fileList = dir;                             % get files of directory
nFiles = numel(fileList);                   % get number of files in directory
label = [];                                 % initialize person labels
fIDs = [];                                  % initialize per person frame IDs
for i = 1 : nFiles                          % for each file in directory
	if fileList(i).isdir == 0               % test if it is a file
		[pathstr,name,ext] = fileparts(fileList(i).name); % separate filename in path, name and extension
		if ext == fileExtension             % test if file is an image
			personID = str2num(name(1:4));	% get person ID from filename
			frameID = str2num(name(5:7));	% get frame ID from filename
			label = [label personID];       % add person ID to list of label
			fIDs = [fIDs frameID];          % add frame ID to list of per person frame IDs
        end % if
    end % if
end % for
cd(currentFolder);                          % go back to working directory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute random partitioning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
personIDs = unique(label);                  % get unique labels
nPersons = length(personIDs);               % count number of persons in dataset from unique labels
train = [];
test = [];
for r = 1 : nRuns
    if badImageRemovalHack == 1 % dataset manipulation !!!!!!!
        % as in [Farenzena et al. CVPR 2010], [Cheng et al. BMVC 2011]
        % do not use images 248-339, 440, 487, 540, 554, 591, 632 on VIPeR
        % dataset as gallery samples
    	index = [1:247, 340:632];
        no = [440, 487, 540, 554, 591, 632];
        index = setdiff(index, no);
        permutation = randperm(length(index));
        testDataset = index(permutation(1 : gallerySize));
        trainDataset = setdiff(1:nPersons, testDataset);
    else
        permutation = randperm(nPersons);
        testDataset = permutation(1 : gallerySize);
        trainDataset = setdiff(1:nPersons, testDataset);
    end
    
    testMask = ismember(label, personIDs(testDataset));
    trainMask = ismember(label, personIDs(trainDataset));
    
    train = [train; trainMask];
    test = [test; testMask];
end % for

train = train == 1;
test = test == 1;

end