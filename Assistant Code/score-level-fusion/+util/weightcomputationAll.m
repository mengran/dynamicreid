function weightcomputationAll()
%WEIGHTCOMPUTATIONALL compute different weights and compare
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc

nRuns = 10;
plotColor = 0;

% Normalization method
type{1} = 'Decimal';
type{2} = 'DoubleSigmoid';
type{3} = 'FAR';
type{4} = 'LikelihoodRatio';
type{5} = 'LogisticRegression';
type{6} = 'MinMax';
type{7} = 'MuSigma';
type{8} = 'Tanh';

% Weighting method
weightmethod{1} = 'EqualWeighted';
weightmethod{2} = 'perf_AUC';
weightmethod{3} = 'perf_rang1';
weightmethod{4} = 'perf_rang10';
weightmethod{5} = 'perf_EER';
weightmethod{6} = 'D-Prime';
weightmethod{7} = 'NCW';

% dsName{1} = 'VIPeR';
% path{1} = 'VIPeRa';
% suffix{1} = '.bmp';
% gSize{1} = 316;

dsName{1} = 'iLIDS';
path{1} = 'i-LIDS_Pedestrian/Persons';
suffix{1} = '.jpg';
% % gSize{2} = 50;

% dsName{1} = 'CAVIAR';
% path{1} = 'CAVIAR4REID';
% suffix{1} = '.jpg';
% gSize{1} = 36;

% dsName{4} = 'ETHZ2';
% path{4} = 'ETHZa/seq2';
% suffix{4} = '.png';
% gSize{4} = 17;
% 
% dsName{5} = 'ETHZ3';
% path{5} = 'ETHZa/seq3';
% suffix{5} = '.png';
% gSize{5} = 14;
% 
% dsName{6} = 'ETHZ1';
% path{6} = 'ETHZa/seq1';
% suffix{6} = '.png';
% gSize{6} = 41;

expr.Num = '001';

disp('Evaluating alternative weight computation methods');
for ds = 1 : length(dsName)
    dataset = dsName{ds};
    disp(['------- DATASET ', dataset, ' -------']);
    datasetPath = ['datasets/', path{ds}];
    datasetFileSuffix = suffix{ds};
%     gallerySize = gSize{ds};
    mask = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/matching/genuineMask_', dataset, '_x' expr.Num '.mat'], ['genuineMask_', dataset]);
    tables = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/matching/Tables_', dataset, '_x' expr.Num '.mat'], ['Tables_', dataset]);

    for typeIdx = 1 : length(type)
        if exist(['../results/', dataset, '_Exp' expr.Num '/transformedTablesFinal/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'file') == 0
%         if exist(['../results/', dataset, '_Exp' expr.Num '/transformedTables/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'file') == 0
            continue;
        end
        parameters = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/transformedTablesFinal/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'parameters');
        partitionTest = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/transformedTablesFinal/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'partitionTest');
        disp(['start experiment for dataset ', dataset, ' and transformation ', type{typeIdx}]);        
%         parameters = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/transformedTables/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'parameters');
%         partitionTest = util.loadFromFile(['../results/', dataset, '_Exp' expr.Num '/transformedTables/LearnTest_', dataset, '_', type{typeIdx}, '.mat'], 'partitionTest');
        folder = ['../results/', dataset, '_Exp' expr.Num '/otherWeightmethod/'];
        
        for wgtmethodIdx = 1 : length(weightmethod)
             file = ['OtherWeights_', dataset, '_', type{typeIdx}, '_', weightmethod{wgtmethodIdx},'.mat'];
             disp(['use weightmethod ', weightmethod{wgtmethodIdx}, ' for ', type{typeIdx}]); 
             if exist([folder, file], 'file') ~= 0
                 disp('experiment already done, nothing to do');
                 continue;
             end
            try
                [cmc_new, AUC_new, ER_new, weights_new, fusedMatchingTables_new] = fusion.weightcomputation(tables, mask, parameters, partitionTest, weightmethod{wgtmethodIdx}, datasetPath, datasetFileSuffix, type{typeIdx}, nRuns, plotColor);
                util.chkmkdir(folder);
                save([folder, file], 'cmc_new', 'AUC_new', 'ER_new', 'weights_new', 'fusedMatchingTables_new');
             catch err
                try
                    for s = 1 : length(err.stack)
                        message = sprintf('%s (Line %d): %s',err.stack(s).name,...
                            err.stack(s).line,err.message);
                        disp(message);
                    end
                catch errCatch
                    disp(errCatch.message);
                end
            end
        end
 
    end
end

end