function skip = chkskip(filename,ss)
% check if the file exists, if the user wants to overwrite it with new
% calculations
skip = false;
if exist(filename,'file')
  u = input(ss,'s');
  if isempty(u) || u(1) ~= 'y'
    % answer was not a "yes overwrite", so load the file
    skip = true;
  end
end
