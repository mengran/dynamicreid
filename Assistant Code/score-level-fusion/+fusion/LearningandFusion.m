function [parameters, wgts, FusedTable, learningTables_Trans] = ...
    LearningandFusion(learnTables, transTables, Mask, Type)
% Trigger learning of fusion parameters and perform fusion
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

message1 = 'LearningTables and TransTables should have the same size.';
assert(length(learnTables) == length(transTables), message1);

nTables = length(learnTables);

% get transformation parameters
switch Type
    case 'Decimal'
        [learningTables_Trans, Normfactor] = fusion.TransformationDecimal(learnTables, learnTables);
        parameters = Normfactor;
    case 'DoubleSigmoid'
        [learningTables_Trans, tau, alpha1, alpha2] = fusion.TransformationDoubleSigmoid(learnTables, learnTables, Mask);
        parameters = [tau; alpha1; alpha2];
    case 'FAR'
        [learningTables_Trans, LookupTable] = fusion.TransformationFAR(learnTables, learnTables, Mask);
        parameters = LookupTable;
    case 'LikelihoodRatio'
        [learningTables_Trans, LookupTable] = fusion.TransformationLikelihoodRatioWithCentering(learnTables, learnTables, Mask);
        parameters = LookupTable;
    case 'LogisticRegression'
        [learningTables_Trans, a, b] = fusion.TransformationLogisticRegression(learnTables, learnTables, Mask);
        parameters = [a; b];
    case 'MinMax'
        [learningTables_Trans, Min, Max] = fusion.TransformationMinMax(learnTables, learnTables);
        parameters = [Min; Max];
    case 'MuSigma'
        [learningTables_Trans, Mu, Sigma] = fusion.TransformationMuSigma(learnTables, learnTables);
        parameters = [Mu; Sigma];
    case 'Tanh'
        [learningTables_Trans, wMu, wSigma] = fusion.TransformationTanh(learnTables, learnTables, Mask);
        parameters = [wMu; wSigma];
end

% get weights for fusion
[wgts, fusedlearningTable] = fusion.NoGreedyfusion(learningTables_Trans, Mask, 'tree', 1);

% Transformation and Fusion
Tables_Trans = cell(1, nTables);
FusedTable = 0;
if strcmp(Type, 'FAR')
    Type = 'LikelihoodRatio';
end
for i = 1 : nTables
    switch Type
        case 'Decimal'
            Tables_Trans{i} = log10(1+transTables{i}) / Normfactor(i);
        case 'DoubleSigmoid'
            matchingTrans = zeros(size(transTables{i}));
            transTable = transTables{i};
            isSmaller = transTable < tau(i);
            matchingTrans(isSmaller) = 1 ./ (1 + exp(-2 .* (transTable(isSmaller) - tau(i)) ./ alpha1(i)));
            matchingTrans(not(isSmaller)) = 1 ./ (1 + exp(-2 .* (transTable(not(isSmaller)) - tau(i)) ./ alpha2(i)));
            Tables_Trans{i} = matchingTrans;
        case 'LikelihoodRatio'
            n = size(transTables, 1);
            nSupportingPoints = size(LookupTable{i}, 2);
            Table = LookupTable{i};
            L = zeros(n); U = nSupportingPoints * ones(n); index = zeros(n);
            transTable = transTables{i};
            tooSmall = transTable <= Table(1,1);
            tooBig = transTable >= Table(1,nSupportingPoints);

            maskRight = not(tooSmall) & not(tooBig);
            range = (max(Table(1,:)) - min(Table(1,:))) / (nSupportingPoints - 1);
            index(maskRight) = (ceil(transTable(maskRight) ./ range) * range - min(Table(1,:))) / range + 1;
            U(maskRight) = max(2,min(nSupportingPoints,round(index(maskRight))));   % upper index
            L(maskRight) = U(maskRight) - 1;                                        % lower index 

            matchingTrans = transTable;
            matchingTrans(tooSmall) = Table(2,1);
            matchingTrans(tooBig) = Table(2, nSupportingPoints);    
            slope = (Table(2,U(maskRight)) - Table(2,L(maskRight))) ./ (Table(1,U(maskRight)) - Table(1,L(maskRight)));
            matchingTrans(maskRight) = slope .* (transTable(maskRight)' - Table(1,L(maskRight))) + Table(2,L(maskRight));

            Tables_Trans{i} = matchingTrans;
        case 'LogisticRegression'
            matchingRatio = exp(a(i) * transTables{i} + b(i));
            Tables_Trans{i} = 1 - matchingRatio ./ (matchingRatio + 1);
        case 'MinMax'
            Tables_Trans{i} = (transTables{i} - Min(i)) / (Max(i) - Min(i));
        case 'MuSigma'
            Tables_Trans{i} = (transTables{i} - Mu(i)) / Sigma(i);
        case 'Tanh'
            Tables_Trans{i} = 0.5 * (tanh(0.01 * (transTables{i} - wMu(i)) / wSigma(i)) + 1);
    end
    FusedTable = FusedTable + wgts(i) * Tables_Trans{i};
end
    