function[Tables_TransMinMax, Min, Max] = TransformationMinMax(learnTables, transTables, lowerQuantile, upperQuantile) 
%TRANSFORMATIONMINMAX Learn parameters from learnTables and transform
%transTables to apply min-max normalization
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: A. Vorndran                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 2
    lowerQuantile = 0;
    upperQuantile = 1;
end
    
% Check the input
validateattributes(learnTables,{'cell'},{'nonempty'},mfilename,'learnTables',1);
validateattributes(transTables,{'cell'},{'nonempty'},mfilename,'transTables',2);
    
attributes = {'nonnegative','<',1};
validateattributes(lowerQuantile,{'numeric'},attributes,mfilename,'lowerQuantile',3);
    
attributes = {'nonzero','<=',1};
validateattributes(upperQuantile,{'numeric'},attributes,mfilename,'upperQuantile',4);

message1 = 'LearningTables and TransTables should have the same size.';
    assert(length(learnTables) == length(transTables), message1);

message2 = ['The lower quantile boundary should be less than the upper'...
        ' quantile boundary.'];
assert(lowerQuantile < upperQuantile,message2);

nTables = length(learnTables);
Tables_TransMinMax = cell(1, nTables);
n = size(learnTables{1},1);

for i=1:nTables

    % learn
    mask = ones(n)-eye(n);
    learnTable = learnTables{i};
    learnTable = learnTable(:);
    mat = learnTable(mask(:) == 1);

    lowerBound = quantile(mat,lowerQuantile); % equals min for 0
    upperBound = quantile(mat,upperQuantile); % equals max for 1
    
    mask = (mat >= lowerBound) & (mat <= upperBound);
    Min(i) = min(mat(mask));
    Max(i) = max(mat(mask));
    
    Tables_TransMinMax{i} = (transTables{i} - Min(i)) / (Max(i) - Min(i));
end


