function intersectionpoint = FindIntersectionpoint(genuineScores, imposterScores, eps)
%FINDINTERSECTIONPOINT: Find intersection point of genuine-impostor-PDFs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 2
    eps = 0.00001; % lower eps -> infinity loops possible
end

nSupportingPoints = 50;
lowerBound = quantile(imposterScores, 0.01);
upperBound = quantile(genuineScores, 0.99);
intersectionpoint = 0.5 * (upperBound + lowerBound);

alpha = eps * (upperBound - lowerBound);

loopCnt = 0;

while (upperBound - lowerBound) > alpha
    t = linspace(lowerBound, upperBound, nSupportingPoints);
    stepwidth = (upperBound - lowerBound) / (nSupportingPoints - 1);
    pGen = fusion.KDE(genuineScores, t);
    pImp = fusion.KDE(imposterScores, t);
    pIsGen = zeros(1, nSupportingPoints);
    sumP = pGen + pImp;
    pIsGen(sumP > 0) = pGen(sumP > 0) ./ sumP(sumP > 0);
    newlowerBound = lowerBound;
    newupperBound = lowerBound;
    for i = 1:nSupportingPoints
        if pIsGen(i) > 0.5
            newlowerBound = lowerBound + (i-1) * stepwidth;
        else % pIsGen(i) <= 0.5
            newupperBound = lowerBound + (i-1) * stepwidth;
            break;
        end
    end
    loopCnt = loopCnt + 1;
    if loopCnt > 8
        break;
    end
    lowerBound = newlowerBound;
    upperBound = newupperBound;
    intersectionpoint = 0.5 * (upperBound + lowerBound);
end

    



