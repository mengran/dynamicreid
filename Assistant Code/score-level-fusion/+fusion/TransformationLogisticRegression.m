function [Tables_TransLogisticRegression, a, b] = TransformationLogisticRegression(learnTables, transTables, Mask) 
% TRANSFORMATIONLOGISTICREGRESSION Learn parameters from learnTables and
% transform transTables using logistic regression normalization
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: J. Niebling                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nTables = length(learnTables);
Tables_TransLogisticRegression = cell(1, nTables);
n = size(learnTables{1},1);

for j=1:nTables 

    learnTable = learnTables{j};
    learnTable = learnTable(:);
    maskGenuine = Mask - eye(n);
    gen = learnTable(maskGenuine(:) == 1)';
    imp = learnTable( not(Mask(:)) )';
        
    minValue = quantile(imp, 0.02);
    maxValue = quantile(gen, 0.98);
    
    % small overlap exception handling
    if maxValue <= minValue % should not happen on real-world data ;-)
        % try it again if there is any overlap
        minValue = min(imp(:));
        maxValue = max(gen(:));
        
        if maxValue <= minValue % there is no overlap between genuine and imposters
            maxValue = minValue;
        end
    end
    
    nSupportingPoints = 1000;
    t = linspace(minValue, maxValue, nSupportingPoints);
    
    pGen = fusion.KDE(gen, t);
    pImp = fusion.KDE(imp, t);
    
    % division by zero exception handling
    epsilon = 0.00000001;
    pImp(pImp <= epsilon) = epsilon;
    
    % log(0) exception handling
    pGen(pGen <= epsilon) = epsilon;
    
    logLR = log(pGen ./ pImp);
    polynomial1 = polyfit(t, logLR, 1);
    a(j) = polynomial1(1);
    b(j) = polynomial1(2);
            
    % transform
    transTable = transTables{j};
    matchingRatio = exp(a(j) * transTable + b(j));
    matchingNorm = 1 - matchingRatio ./ (matchingRatio + 1);
   
    Tables_TransLogisticRegression{j} = matchingNorm;
end