function [wgtsTree, finalfusedTable] = treeFusion(Tables, Mask) % add parameters and return values
%TREEFUSION Tree-based fusion scheme used for pairwise weighting in PROPER
% method as proposed in paper. Pair-wise weight computation is done in
% getOptimalWeights (line 104).
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nElements = length(Tables); % TODO replace: set number of matching tables (use size-function)

% initialize helper tables
hlp = 1:nElements;
next2pow = 2; % next higher power of 2
while next2pow < nElements
    next2pow = next2pow * 2;
end
if nElements < next2pow
    hlp = [hlp zeros(1, next2pow - nElements)];
end
pairTab = [];
steps = [];
step = 1;
while step < next2pow
    pairs = hlp(1:step:next2pow);
    pairTab = [pairTab; [pairs zeros(1, next2pow - size(pairs, 2))]];
    steps = [steps; step];
    
    step = step * 2;
end

% get genuine and imposterscores
for i = 1 : nElements
    matching = Tables{i};
    n = size(matching,1);
    maskLearn = Mask - eye(n); % mask without main diagonal for genuine score
    gen{i} = matching(maskLearn(:) == 1)';
    imp{i} = matching( not(Mask(:)) )';
end

% initialize fusion
weights = ones(1, next2pow);
matchingTables = Tables; % TODO replace: use copy of cell array with tables to be fused

% do fusion
for iteration = 1 : size(steps, 1)
    step = steps(iteration);
    for j = 1 : 2 : (next2pow / step)
        idx1 = pairTab(iteration, j);
        idx2 = pairTab(iteration, j + 1);
        
        if (idx1 == 0) || (idx2 == 0) % no valid fusion
            break; % exit for loop (j)
        end
        
        table1 = matchingTables{idx1};
        table2 = matchingTables{idx2};
        gen1 = gen{idx1};
        gen2 = gen{idx2};
        imp1 = imp{idx1};
        imp2 = imp{idx2};        
        
        wgts = fusion.getOptimalWeights([gen1; gen2], [imp1; imp2]); % TODO replace: use genuine and imposter instead of tables
        fusedTable = wgts(1) * table1 + wgts(2) * table2; % TODO replace: do fusion of tables here (with weights)
        
        % save fused table (at index of first element)
        matchingTables{1, idx1} = fusedTable;
        
        % update weights
        weights(idx1 : (idx1 + step - 1)) = weights(idx1 : (idx1 + step - 1)) * wgts(1);
        weights(idx2 : (idx2 + step - 1)) = weights(idx2 : (idx2 + step - 1)) * wgts(2);
    end
end

% final fused table is stored at   matchingTables{1, 1}
finalfusedTable = matchingTables{1};
% final weights are stored at   weights(1 : nElements)
wgtsTree = weights(1 : nElements);
