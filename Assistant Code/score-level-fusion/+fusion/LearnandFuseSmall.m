%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: J. Niebling                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filter = [1:11, 68];
learnTables = Tables_iLIDS(filter);
transTables = Tables_iLIDS(filter);
Mask = genuineMask_iLIDS;

try
    [TauAlpha1Alpha2_iLIDSsmall, wgtsDoubleSigmoid_iLIDSsmall, FusedTableDoubleSigmoid_iLIDSsmall, Tables_iLIDS_TransDoubleSigmoidsmall] = fusion.LearningandFusion(learnTables, transTables, Mask, 'DoubleSigmoid');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_DoubleSigmoidsmall', 'TauAlpha1Alpha2_iLIDSsmall', 'wgtsDoubleSigmoid_iLIDSsmall', 'Tables_iLIDS_TransDoubleSigmoidsmall');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_iLIDS_DoubleSigmoidsmall', 'FusedTableDoubleSigmoid_iLIDSsmall');
catch err
    disp(err.message)
end

try
    [MinMax_iLIDSsmall, wgtsMinMax_iLIDSsmall, FusedTableMinMax_iLIDSsmall, Tables_iLIDS_TransMinMaxsmall] = fusion.LearningandFusion(learnTables, transTables, Mask, 'MinMax');
    save('../results/iLIDS_Exp001/transformedTables/LearniLIDS_MinMaxsmall', 'MinMax_iLIDSsmall', 'wgtsMinMax_iLIDSsmall', 'Tables_iLIDS_TransMinMaxsmall');
    save('../results/iLIDS_Exp001/transformedTables/FusionParameters_iLIDS_MinMaxsmall', 'FusedTableMinMax_iLIDSsmall');
catch err
    disp(err.message)
end
