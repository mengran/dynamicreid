function [weight] = minimizeER(Tables,benchparam,lowerLimit,upperLimit,step,nRecursions)
%MINIMIZEER Minimize Expected Rank
% weight = minimizeER(Tables,benchmarkParams,lowerLimit,upperLimit,step,nRecursions)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, A. Vorndran                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Calculate the weight with the given parameters
    ERmin = Inf;
    for w=lowerLimit:step:upperLimit
        [~, ~, ER] = util.benchmarking(benchparam{1:3},Tables{1} + w * Tables{2},benchparam{4},0);
        
        if ER < ERmin
            weight = w;
            ERmin = ER;
        end
    end
    
    if nRecursions > 1
        newstep = step/10;
        newlower = max(weight-5*newstep,lowerLimit);
        newupper = min(weight+5*newstep,upperLimit);
        weight = ...
            minimizeERHidden(Tables,benchparam,newlower,newupper,newstep,(nRecursions-1));
    end

end

function [weight] = minimizeERHidden(Tables,benchparam,lowerLimit,upperLimit,step,nRecursions)
    
    % Calculate the weight with the given parameters
    ERmin = Inf;
    for w=lowerLimit:step:upperLimit
        [~, ~, ER] = util.benchmarking(benchparam{1:3},Tables{1} + w * Tables{2},benchparam{4},0);
        
        if ER < ERmin
            weight = w;
            ERmin = ER;
        end
    end
    
    if nRecursions > 1
        newstep = step/10;
        newlower = max(weight-5*newstep,lowerLimit);
        newupper = min(weight+5*newstep,upperLimit);
        weight = ...
            minimizeERHidden(Tables,benchparam,newlower,newupper,newstep,(nRecursions-1));
    end

end