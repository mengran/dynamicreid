function[Tables_TransLikelihoodRatio_withcentering, LookupTable, gen, imp] = ...
    TransformationLikelihoodRatioWithCentering(learnTables, transTables, Mask)
% TRANSFORMATIONLIKELIHOODRATIOWITHCENTERING Learn lookuptable from
% learnTables and transform transTables using likelihood ratio
% normalization with previous zero-mean and standard-variance centering
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach, J. Niebling                                        
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nTables = length(learnTables);
Tables_TransLikelihoodRatio_withcentering = cell(1, nTables);
LookupTable = cell(1, nTables);
n = size(learnTables{1},1);

% Mu-Sigma-centering
[learnTables_trans, mu, sigma] = fusion.TransformationMuSigma(learnTables, learnTables);

 for j = 1 : nTables

    learnTable = learnTables_trans{j};
    learnTable = learnTable(:);
    maskGenuine = Mask - eye(n);
    gen = learnTable(maskGenuine(:) == 1)';
    imp = learnTable( not(Mask(:)) )';
    
    maskwithoutmain = ones(n) - eye(n);
    minValue = quantile(learnTable(logical(maskwithoutmain(:))), 0);
    maxValue = quantile(learnTable(logical(maskwithoutmain(:))), 0.99);
    nSupportingPoints = 100; % 1000
    
    [lowerquant, upperquant, upperquantI] = fusion.getQuantile(gen, imp, nSupportingPoints, 0.01);
        
    [t, pIsGen, polynomial] = fusion.genuineImposterPlotwithQuantile(gen, imp, minValue, maxValue, nSupportingPoints, lowerquant, upperquant, upperquantI, 0);
    
    % exception handling: raise in likelihood ratio
    range = ceil(length(t) / 5) : length(t);
    t2 = t(range);
    der = polyder(polynomial);
    tDeriv = polyval(der,t2(:));
    raise = tDeriv > 0;
    if sum(raise) > 0
        idx = min(range(raise));
    else
        idx = ceil(length(t) * 19 / 20);
    end
    prev = idx - 1;
    nValues = length(t) - prev + 1;
    pIsGen(prev : length(t)) = linspace(pIsGen(prev), 0, nValues);
    
    pIsImp = 1 - pIsGen; % distance scores
    
    t_transBack = t * sigma(j) + mu(j);
    LookupTable{j} = [t_transBack; pIsImp];
    Table = LookupTable{j};
      
    L = zeros(n); U = nSupportingPoints * ones(n); index = zeros(n);
    transTable = transTables{j};
    tooSmall = transTable <= Table(1,1);
    tooBig = transTable >= Table(1,nSupportingPoints);
    
    maskRight = not(tooSmall) & not(tooBig);
    range = Table(1,2) - Table(1,1);
    index(maskRight) = (ceil(transTable(maskRight) ./ range) * range - Table(1,1)) / range + 1;
    U(maskRight) = max(2,min(nSupportingPoints,round(index(maskRight))));   % upper index
    L(maskRight) = U(maskRight) - 1;                                        % lower index 
    
    matchingTrans = transTable;
    matchingTrans(tooSmall) = Table(2,1);
    matchingTrans(tooBig) = Table(2, nSupportingPoints);
    try 
        slope = (Table(2,U(maskRight)) - Table(2,L(maskRight))) ./ (Table(1,U(maskRight)) - Table(1,L(maskRight)));
    catch err
        disp(['min(U): ' min(min(U(maskRight)))])
        disp(['min(L): ' min(min(L(maskRight)))])
        disp(j)
        rethrow(err)
    end
    matchingTrans(maskRight) = slope .* (transTable(maskRight)' - Table(1,L(maskRight))) + Table(2,L(maskRight));
        
    Tables_TransLikelihoodRatio_withcentering{j} = matchingTrans;
  end

end