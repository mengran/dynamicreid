function [expntsBest, minOverlap] = getOptimalExponents(genuineScores, ...
    imposterScores, doPlot)
%GETOPTIMALEXPONENTS PROPER weighting for fusion with multiplication
% instead of sum to get fused score
% Parameters as in getOptimalWeights.m
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Copyright (c) 2015,                                                      
%     Neuroinformatics and Cognitive Robotics Labs at TU Ilmenau, Germany  
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% All rights reserved.                                                     
%                                                                          
% Copying, resale, or redistribution, with or without modification, is     
% strictly prohibited.                                                     
%                                                                          
% The academic use of this MATLAB code is free of charge. Any commercial   
% distribution or act related to the commercial usage of this source code  
% is strictly prohibited. The distribution of this source code to any      
% parties that have not read and agreed to the terms and conditions of     
% usage is strictly prohibited.                                            
%                                                                          
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS      
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT        
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT     
% OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, EVEN IF ADVISED OF THE     
% POSSIBILITY OF SUCH DAMAGE.                                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% This code is is publicly available at                                    
% http://www.tu-ilmenau.de/neurob/data-sets-code/score-level-fusion/       
% for academic use only.                                                   
%                                                                          
% When you use this code, you must cite the following paper:               
%                                                                          
% Eisenbach, M., Kolarow, A., Vorndran, A., Niebling, J., Gross, H.-M.:    
% Evaluation of Multi Feature Fusion at Score-Level for Appearance-based   
% Person Re-Identification. Int. Joint Conf. on Neural Networks (IJCNN),   
% Killarney, Ireland, pp. 469-476, IEEE 2015.                              
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          
% Author: M. Eisenbach                                                      
%                                                                          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3
    doPlot = 0;
end

nSupportingPoints = 50;

leftBorder = 0;
rightBorder = (pi/2);

step = (rightBorder - leftBorder) / 7;

if doPlot == 1
    pre = [];
    post = [];
end

while step > 0.001
    ref = cos(leftBorder:step:rightBorder);
    expnt = [ref; sin(leftBorder:step:rightBorder)];
    expnt = expnt ./ [sum(expnt); sum(expnt)];
    minOverlap = 1;
    bestPos = 0;
    
    if doPlot == 1
        areaPlot = [];
    end
    
    for i = 1 : length(expnt)
        genFus =  genuineScores(1, :) .^(expnt(1, i)) .* genuineScores(2, :) .^(expnt(2, i));
        impFus =  imposterScores(1, :) .^(expnt(1, i)) .* imposterScores(2, :) .^(expnt(2, i));
        area = fusion.areaPDFOverlap(genFus, impFus, nSupportingPoints);
        
        if doPlot == 1
            areaPlot = [areaPlot area];
        end
        
        if area < minOverlap
            minOverlap = area;
            bestPos = i;
        end
    end
    expntsBest = expnt(:, bestPos);
    
    if doPlot == 1
        pre = [pre [expnt(1, 1:(max(1,bestPos - 1))); areaPlot(1:(max(1,bestPos - 1)))]];
        post = [[expnt(1, (max(1,bestPos - 1)):length(expnt)); areaPlot((max(1,bestPos - 1)):length(expnt))] post];
    end
    
    leftBorder = acos(ref(max(1,bestPos - 1)));
    rightBorder = acos(ref(min(bestPos + 1,length(ref))));
    step = (rightBorder - leftBorder) / 7;
end

if doPlot == 1
    ges = [pre [expnt(1, :); areaPlot] post];

    figure
    plot(expnt(1,:), areaPlot, 'b-');
    hold on
    plot(expnt(1,:), areaPlot, 'b.');
    hold off

    figure
    plot(ges(1,:), ges(2,:), 'r-');
    hold on
    plot(ges(1,:), ges(2,:), 'r.');
    hold off
end

end