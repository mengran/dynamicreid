function [pwdist] = Salience_SvsS(data,expr,parCPS)
%SALIENCE_SVSS Wrapper for Unsupervised Salience Learning by Zhao et. al.
%   
% Unsupervised Salience Learning was created by Rui Zhao, on May 2013. 
%
% Please cite as
% Rui Zhao, Wanli Ouyang, and Xiaogang Wang. Unsupervised Salience Learning
% for Person Re-identification. In IEEE Conference of Computer Vision and
% Pattern Recognition (CVPR), 2013. 

import sdc.*;
import sdc.code.*;
import sdc.code.patchmatch.*;
import sdc.code.libsvm.*;
import sdc.code.densefeat.*;

%% Set project structure
% project directory
% project_dir = parCPS.Dir;
% location of dataset
% dataset_dir = data.Dir;
% cache directory
cache_dir = strcat(parCPS.Dir, '/salience/');
% dense feature directory
feat_dir = [parCPS.Dir '/features/densefeat/'];
% mutual distance map set
pwdist_dir = [parCPS.Dir '/matching/salience/'];
% salience directory
% salience_dir = strcat(cache_dir, 'salience/');
salience_dir = strcat(cache_dir, '');
% create directories
util.chkmkdir(cache_dir);
util.chkmkdir(feat_dir);
util.chkmkdir(pwdist_dir);
util.chkmkdir(salience_dir);

%% Parameters

switch expr.Name
    case {'ETHZ1','ETHZ2','ETHZ3','CAVIAR'}
        par = struct(...
            'dataset',              expr.Name, ...
            'baseExp',              'unsupervised_salience', ...
            'gridstep',             4, ... 
            'patchsize',            10, ...
            'sigma1',               1.6, ...
            'msk_thr',              0.2, ...
            'norm_data',            1, ...
            'new_feat',             0, ...
            'use_salience',         2, ...
            'alpha',                [-1, 0, 0, 0, 0], ... %[-1, 0.4, 1, 0.6, 0.5], ...
            'L2',                   0 ...
            );
    otherwise
        par = struct(...
            'dataset',              expr.Name, ...
            'baseExp',              'unsupervised_salience', ...
            'method',               'salience', ... % 'patchmatch', 'salience' ...
            'gridstep',             4, ...
            'patchsize',            10, ...
            'Nr',                   100, ...
            'sigma1',               2.8, ...
            'msk_thr',              0.2, ...
            'norm_data',            1, ...
            'new_feat',             0, ...
            'use_mask',             1, ...
            'use_salience',         2, ... % set 1 to use knn salience, and set 2 to use ocsvm salience
            'alpha',                [-1, 0, 0, 0, 0],  ... %[-1, 0.4, 1, 0.6, 0], ... %
            'L2',                   1 ...
            );
end

dataset     = par.dataset;
baseExp     = par.baseExp;
% TRIAL       = par.TRIAL;
gridstep    = par.gridstep;
patchsize   = par.patchsize;

%% initialize the contextual settings
fnames = {data.Files.name};
array_all = cell2mat(fnames');
% '0001001.png'
cell_all = mat2cell(array_all, ones(1, length(fnames)), [4, 3, 4]);
perIds = cell_all(:, 1);
% camIds = cell_all(:, 2);

nImages = length(fnames);
nPerson = length(unique(perIds));
ttsize = size(cell_all, 1);

% get labels of data
n = 0;
oldId = '';
pidx = cell(nPerson,1);
for i = 1:ttsize
    if ~strcmp(perIds{i}, oldId)
        n = n+1;
        pidx{n} = i;
        oldId = perIds{i};
    else
        pidx{n} = [pidx{n}, i];
    end
end

% get and normalize images of dataset
% imgData = data.RGB;
switch expr.Name
    case {'ETHZ1','ETHZ2','ETHZ3','CAVIAR'}
        imgData = zeros(64,32,3,nImages);
    otherwise
        imgData = zeros(128,48,3,nImages);
end
if par.norm_data
    hwait = waitbar(0,'Normalizing dataset');
    for i = 1:nImages
        tempImg = squeeze(data.RGB(:,:,:,i));
        if par.norm_data
            imgHSV = rgb2hsv(tempImg);
            imgHSV(:,:,3) = histeq(imgHSV(:,:,3));
            tempImg = hsv2rgb(imgHSV);
        end
        switch expr.Name
            case {'ETHZ1','ETHZ2','ETHZ3','CAVIAR'}
                imgData(:,:,:,i) = util.normalizeSize(tempImg,64,32,0);
            otherwise
                imgData(:,:,:,i) = util.normalizeSize(tempImg,128,48,0);
        end
        waitbar(i/nImages,hwait);
    end
    close(hwait);
end

% image and feature information
[h, w, ~] = size(imgData(:,:,:,1));
% feature information
feat1 = strcat(feat_dir, 'feat1.mat');
if exist(feat1, 'file')
    load(feat1,'densefeat');
    [dim, ~] = size(densefeat);
end

% partition image into dense local patches
nx = length(patchsize/2:gridstep:w-patchsize/2);
ny = length(patchsize/2:gridstep:h-patchsize/2);
grid_x = ceil(linspace(patchsize/2, w-patchsize/2, nx));
grid_y = ceil(linspace(patchsize/2, h-patchsize/2, ny));
X = repmat(grid_x, ny, 1);
Y = repmat(grid_y', 1, nx);
gridxy = [X(:), Y(:)];

%% extract dense feature
build_densefeature_general;

%% patch matching
hwait = waitbar(0, 'Computing patch matching ...');
for i = 1:ttsize
    fname = [pwdist_dir, 'pwmap', num2str(i), '.mat'];
    if ~exist(fname,'file')
%         densefeat = features(:, :, i);
        galleryFeat = util.loadFromFile([feat_dir, 'feat', num2str(i), '.mat'],'densefeat');
        parfor j = 1:ttsize
            probeFeat = util.loadFromFile([feat_dir, 'feat', num2str(j), '.mat'],'densefeat');
            [pwmap(j).forward, pwmap(j).fpos] = ...
                mutualmap(galleryFeat,probeFeat,ny,nx);
%             [pwmap(j).forward, pwmap(j).fpos] = ...
%                 mutualmap(densefeat, features(:, :, j),ny,nx);
        end
        save(fname, 'pwmap');
    end
    waitbar(i/ttsize, hwait, 'Computing patch matching ...');
end
close(hwait);

%% compute salience
switch par.use_salience
    case 0
        
    case 1 % KNN salience
        if exist([salience_dir, 'maxdist_knn.mat'], 'file')
            
            load([salience_dir, 'maxdist_knn.mat']);
            
        else
            hwait = waitbar(0, 'computing knn salience ...');
            for i = 1:ttsize
                load([pwdist_dir, 'pwmap', num2str(i), '.mat']);
                cellmap = struct2cell(pwmap);
                dists = cell2mat(cellmap(1, 1, :));
                pid = str2double(perIds{i});
                index = [];
                for p = 1:length(pidx)
                    nIm = length(pidx{p});
                    if p ~= pid
                        rp = randperm(nIm);
                        index(p) = pidx{p}(rp(1));
                    else
                        temp = setdiff(pidx{p}, i);
                        rp = randperm(nIm-1);
                        index(p) = temp(rp(1));
                    end
                end
                rdists = sort(dists(:, :, index), 3);
                maxdist(:, :, i) = rdists(:, :, floor(nPerson/2));
                waitbar(i/ttsize, hwait);
            end
            save([salience_dir, 'maxdist_knn.mat'], 'maxdist');
            close(hwait);
        end
        
    case 2 % OCSVM salience
        if exist([salience_dir, 'maxdist_ocsvm.mat'], 'file')
            fprintf('-> Load salience...')
            load([salience_dir, 'maxdist_ocsvm.mat']);
            fprintf('OK\n');
        else
            hwait = waitbar(0, 'computing ocsvm salience ...');
            for i = 1:ttsize
                load([pwdist_dir, 'pwmap', num2str(i), '.mat']);
                cellmap = struct2cell(pwmap);
                dists = cell2mat(cellmap(1, 1, :));
                fpos = squeeze(cellmap(2, 1, :));
                pid = str2double(perIds{i});
                index = [];
                for p = 1:length(pidx)
                    nIm = length(pidx{p});
                    if p ~= pid
                        rp = randperm(nIm);
                        index(p) = pidx{p}(rp(1));
                    else
                        temp = setdiff(pidx{p}, i);
                        rp = randperm(nIm-1);
                        index(p) = temp(rp(1));
                    end
                end
                xnn = {};
                for j = 1:length(index)
                    load([feat_dir, 'feat', num2str(index(j)), '.mat']);
                    feat_cell = reshape(mat2cell(densefeat, dim, ones(1, ny*nx)), ny, nx);
                    P = fpos{index(j)};
                    xnn = cat(3, xnn, feat_cell(sub2ind([ny, nx], repmat((1:ny)', 1, nx), double(P))));
                end
                xnn = reshape(xnn, ny*nx, length(index));
                dists_tmp = reshape(dists(:, :, index), ny*nx, length(index));
                maxidx = zeros(1, ny*nx);
                parfor j = 1:ny*nx
                    X = cell2mat(xnn(j, :))';
                    [~, maxidx(j)] = ocsvm_max(X);
                end
                maxdist(:, i) = dists_tmp(sub2ind([ny*nx, length(index)], 1:ny*nx, maxidx));
                waitbar(i/ttsize, hwait);
            end
            maxdist = reshape(maxdist, ny, nx, ttsize);
            save([salience_dir, 'maxdist_ocsvm.mat'], 'maxdist');
            close(hwait);
        end
        
    otherwise
        error('unknown option');
end



%% normalize salience map
lwdist = min(maxdist(:));
updist = max(maxdist(:));
maxdist_norm = (maxdist-lwdist)./(updist-lwdist);
salience = squeeze(mat2cell(maxdist_norm, ny, nx, ones(ttsize, 1)))';

%% Compute all-vs.-all matching
% % use masks from CPS framework
% if par.use_mask && 0
%     msk = util.loadFromFile([parCPS.Dir '/' expr.Mask 'masks.mat'],'msk');
%     mask = cell(nImages,1);
%     hh = waitbar(0,'Preparing masks');
%     for i=1:nImages
%         mask{i} = imresize(msk{i},[ny nx]) > par.msk_thr;
%         waitbar(i/nImages,hh);
%     end
%     if ishandle(hh)
%         close(hh);
%     end
%     clear msk;
% else
%     mask = repmat({ones(ny,nx)  > par.msk_thr},nImages);
% end

%% load all features for testing
fprintf('-> Compute matching scores...');
stime = clock;

if ~expr.clearMemory
    features = zeros(dim, ny*nx, ttsize);
    hwait = waitbar(0, 'Loading data for testing ...');
    for i = 1:ttsize
        densefeat = util.loadFromFile([feat_dir, 'feat', num2str(i), '.mat'],'densefeat');
        features(:, :, i) = densefeat;
        waitbar(i/ttsize, hwait);
    end
    close(hwait);
    features = squeeze(mat2cell(features, dim, ny*nx, ones(ttsize, 1)))';
    
    hwait = waitbar(0, 'Computing matching scores ...');
    pwdist_sal = zeros(ttsize, ttsize);
    for i = 1:ttsize
        load([pwdist_dir, 'pwmap', num2str(i), '.mat']);
        gallMask = repmat(mask{i}(:)',dim,1);
        for j=1:(i-1)
            probMask = repmat(mask{j}(:)',dim,1);
            pwdist_sal(i, j)  = ethzscorefun(features{i}.*gallMask, features{j}.*probMask, salience{i}, salience{j}, par);
        end
        waitbar(i/ttsize, hwait);
    end
    pwdist_sal = tril(pwdist_sal,-1)+tril(pwdist_sal,-1)';
    close(hwait);
    
else % low memory situation
    hwait = waitbar(0, 'Computing matching scores ...');
    pwdist_sal = zeros(ttsize, ttsize);
    for i = 1:ttsize
        load([pwdist_dir, 'pwmap', num2str(i), '.mat']);
        gallery = util.loadFromFile([feat_dir, 'feat', num2str(i), '.mat'],'densefeat');
        gallMask = repmat(mask{i}(:)',dim,1);
        parfor j=1:(i-1)
            probMask = repmat(mask{j}(:)',dim,1);
            probe = util.loadFromFile([feat_dir, 'feat', num2str(j), '.mat'],'densefeat');
            pwdist_sal(i, j)  = ethzscorefun(gallery.*gallMask, probe.*probMask, salience{i}, salience{j}, par);
        end
        waitbar(i/ttsize, hwait);
    end
    pwdist_sal = tril(pwdist_sal,-1)+tril(pwdist_sal,-1)';
    close(hwait);
    
end

fprintf('OK (%s)\n',util.getDurationString(stime));

pwdist = par.alpha(1).*pwdist_sal;

end