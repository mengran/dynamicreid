function trackletShow(data,label_s,subcolor)
% data: d X N matrix
% colorS = colormap('HSV');
% subcolor = linspace(1,size(colorS,1),size(data,2));
% subcolor = colorS(round(subcolor),:);
tmpX = data(1:2:end,:);
tmpY = data(2:2:end,:);

n_track = size(data,2);
figuresize = [800,600];

% nX = ceil(sqrt(n_track));
% nY = ceil(n_track/nX);
nY = numel(unique(label_s));
nX = numel(label_s)/nY;

offset_x = figuresize(1)/(2*nX)*(1:2:2*nX);
offset_y = figuresize(2)/(2*nY)*(1:2:2*nY);

figure('Color',[0 0 0],'OuterPosition',[100,100,figuresize(1)+100,figuresize(2)+100]);
hold on
axis on
set(gca,'Color',[0 0 0]);
set(gca,'YDir','reverse');
for i = 1:n_track
    [x,y] = ind2sub([nX,nY],i);
    showX = tmpX(:,i) + offset_x(x);
    showY = tmpY(:,i) + offset_y(y);
    plot(showX(:),showY(:),'Color',subcolor(i,:));
end