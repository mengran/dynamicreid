clc
clear 
%% Edge feature extraction from P Dollar
load 'Dataset/iLIDSVID_Images_Tracklets_l15.mat';

meanEdge = cell(1,numel(I));
workingPath = pwd;
% obtain the average human body shape by PDollar's edge toolbox
opts=edgesTrain();                % default options (good settings)
opts.modelDir='models/';          % model will be in models/forest
opts.modelFnm='modelBsds';        % model name
opts.nPos=5e5; opts.nNeg=5e5;     % decrease to speedup training
opts.useParfor=0;                 % parallelize if sufficient memory
tic, model=edgesTrain(opts); toc; % will load model if already trained
for i = 1:numel(I)
    i
    tmpSeq = I{i};
    tmpEdge = zeros(128,64,numel(tmpSeq));
    for f = 1:numel(tmpSeq)
        tmpEdge(:,:,f) = edgesDetect(tmpSeq{f},model);
    end
    meanEdge{i} = mean(tmpEdge,3); % naive mean along the time dimension
end
cd(workingPath)
save('Feature/iLIDSVID_Images_semEdge.mat','meanEdge');