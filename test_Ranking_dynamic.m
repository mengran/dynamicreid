clear
%% Parameters setting
% load('dataset\iLIDSVID_Images_Tracklets.mat');
% load('Feature\iLIDSVID_Partition_Random.mat');
% load('Feature\iLIDSVID_Partition_Random_archiveRandGal.mat');
addpath(genpath('Assistant Code/'));
algoname = 'unsuper';%'unsuper';'LFDA'
fname = 'PRID';
profix = [];%'_cam58';%'_allbk_89';%[];%'_allbk_150';
partition_name = 'DVR'; %'Random';
featname = 'HistLBP';
num_patch = 75;

dataname = [fname '_Images_Tracklets_l15' profix];
load(['Dataset/' dataname '.mat']);
load(['Feature/' fname '_Partition_' partition_name profix '.mat']);

loadfeat = 1; %load precomputed feautre, if exist
savefeat = 0;

% choose appearance feature
switch featname
    case {'LDFV'}
        appFeaturename = ['_LDFVmaskedge' num2str(num_patch) 'Patch64'];
    case {'HistLBP'}
        appFeaturename = ['_Hist' num2str(num_patch) 'Patch'];
    case {'ColorLBP'}
        appFeaturename = ['_ColorLBP'];
    case {'HOG3D'}
        appFeaturename = ['_HOG3D'];
end
load(fullfile('Feature',[fname '_semEdge' profix]));

% default algorithm option setting
AlgoOption.name = algoname;
AlgoOption.func = algoname; % note 'rPCCA' use PCCA function also.
AlgoOption.beta =3;  % different algorithm have different meaning, refer to PCCA and LFDA paper.
AlgoOption.d =40; % projection dimension
AlgoOption.epsilon =1e-4;
AlgoOption.lambda =0;
AlgoOption.dataname = fname;
AlgoOption.partitionname = partition_name;
AlgoOption.useDynamic = 0; %0----App only; 1----Fusion; 2----Dynamic Only
AlgoOption.weight = [1];%[0.82 0.18];
AlgoOption.num_rep = 1; % number of repeat for each partition

if AlgoOption.useDynamic == 1 
    % choose type of features to fuse
    appType = 'LDFV';
    mixDyn = 0; % fuse both HOG3D and DynFV
    dynType = 'DynFV';
end
    
% customize in different case
switch  algoname
    case {'LFDA'}
        AlgoOption.npratio =0; % npratio is not required.
        AlgoOption.beta =0.01;
        AlgoOption.d = 40;
        AlgoOption.LocalScalingNeighbor =6; % local scaling affinity matrix parameter.
        AlgoOption.num_itr= 10;
    case {'rPCCA'}
        AlgoOption.func = 'PCCA';
        AlgoOption.lambda =0.01;
        AlgoOption.beta =3;
    case {'MFA'}
        AlgoOption.Nw = 0;
        AlgoOption.Nb = 12;
        AlgoOption.d = 30;
        AlgoOption.beta = 0.01;
    case {'oLFDA'}
        AlgoOption.npratio =0; % npratio is not required.
        AlgoOption.beta =0.15; % regularization parameter
        AlgoOption.d = 40;
        AlgoOption.LocalScalingNeighbor =6; % local scaling affinity matrix parameter.
        AlgoOption.num_itr= 10;
end
rng('default');
%% %-------parameter tuning--------%
% split = [6 1];
Res_tp = [];
for wd = 0.2%:0.01:0.3%0:0.02:0.5
    for wt = 1
        if wt == 1 && wd ~= 1
            fprintf('Dynamic weight: %f \n',wd);
        elseif wt~=1 && wd ~=1
            fprintf('DynFV weight: %f; HOG3D weight: %f \n',wd, wt);       
        end
%%
Featurename = [fname appFeaturename profix];
% Load Appearance feature if possible
if loadfeat
    try
        load(fullfile('Feature',Featurename));
    catch
        disp('No precomputed feature founded')
        loadfeat = 0;
    end
end

if AlgoOption.useDynamic
    AlgoOption.dynOpt.nr = 6;%split(wd,1); % number of patches along the row (y) dimension
    AlgoOption.dynOpt.nc = 3;%split(wd,2); % number of patches along the col (x) dimension
    AlgoOption.dynOpt.PatchOverlap = 1; % Generated grids w/wo overlapping
    AlgoOption.dynOpt.ncenter = 12; % number of GMM learned
    AlgoOption.dynOpt.edgeMask = 1; % mask out the background by mean edge
    AlgoOption.dynOpt.w_dyn = wd;%0.1;%0.18;%0.25;
    AlgoOption.dynOpt.w_ts = wt;
    AlgoOption.dynOpt.slfashion = 1; % 1----One FV for each window size; 2----One FV for each short traj
    AlgoOption.dynOpt.RP = 0;%0-no projection; 1-hankel regressor
    AlgoOption.dynOpt.sl = [5 9 14];
    AlgoOption.dynOpt.reAug = 0; % 1----Augmented trajs with reverse direction; 0----No augment
    AlgoOption.dynOpt.thBiDiff = 0; %threshold for bilingual denoise; 0---skip this step
    Featurename = [Featurename '_wDynamic_ncenter' num2str(AlgoOption.dynOpt.ncenter) ...
        '_s' num2str(AlgoOption.dynOpt.nr*AlgoOption.dynOpt.nc) '_wdyn' num2str(AlgoOption.dynOpt.w_dyn)];
    if AlgoOption.useDynamic==1 % feature fusion
        featuse = {};
        featuse = [featuse, [fname '_HOG3D' profix]];
        featuse = [featuse, [fname '_dyn3sl' profix]];
        %         featuse = [featuse, [fname '_ColorLBP' profix]];
        %         featuse = [featuse, [fname '_LDFV75Patch64' profix]];
%         load(fullfile('Feature',featuse{1}));
%         load(fullfile('Feature',featuse{2}));
        %         load(fullfile('Feature',featuse{3}));
        Featurename = [cell2mat(featuse) '_wdyn' num2str(AlgoOption.dynOpt.w_dyn)];
        %         Featurename = [cell2mat(featuse) '_wdyn' num2str(AlgoOption.dynOpt.w_dyn)...
        %             '_wts' num2str(AlgoOption.dynOpt.w_ts)];
    end
    % augmented reverse
    if AlgoOption.dynOpt.reAug
        denseTrjRe = denseTrj;
        for n = 1:numel(denseTrj)
            tmpTrj = denseTrj{n};
            tmpX = tmpTrj(:,2:2:end);
            tmpY = tmpTrj(:,3:2:end);
            denseTrjRe{n}(:,2:2:end) = tmpX(:,end:-1:1);
            denseTrjRe{n}(:,3:2:end) = tmpY(:,end:-1:1);
%             nCol = cellfun(@size,I{n},'UniformOutput',0);
%             nCol = cell2mat(nCol');
%             nCol = nCol(:,2);
%             nCol_dense = nCol(denseTrj{n}(:,2));
%             denseTrjRe{n}(:,3:2:end) = bsxfun(@minus,nCol_dense,denseTrj{n}(:,3:2:end));
        end
        denseTrj = cellfun(@(x,y) [x;y], denseTrj,denseTrjRe,'UniformOutput',0);
    end
    if exist(['Feature/' fname '_dyn' num2str(numel(AlgoOption.dynOpt.sl)) 'sl' profix '.mat'],'file') && loadfeat
        load(['Feature/' fname '_dyn' num2str(numel(AlgoOption.dynOpt.sl)) 'sl' profix '.mat']);
    end
else
    Featurename = [Featurename '_woDynamic'];
end
kname={'linear'};

t1 = clock;
timstr =[ num2str(int32(t1(1))) '_'  num2str(int32(t1(2))) '_'  num2str(int32(t1(3)))...
    '_'  num2str(int32(t1(4))) '_'  num2str(int32(t1(5))) '_'  num2str(int32(t1(6)))];

%----------------debug-------------%
% personrank = cell(1,numel(unique(gID)));
%----------------------------------%
% if ~exist('FeatApp_Keep','var');
%     FeatApp_Keep = cell(1,length(Partition));
%     FeatDyn_Keep = cell(1,length(Partition));
% end

%%
for k = 1:numel(kname)
    AlgoOption.kernel = kname{k};
    savename = ['Result_' fname '_' AlgoOption.name '_' Featurename '_' partition_name '_' kname{k} '_' timstr '.mat'];
    display(savename);
    for idx_partition=1:length(Partition) % partition loop
        for rep = 1:AlgoOption.num_rep % repeat loop
%             iset = 1;
            display(['==============' kname{k} ' ' num2str(idx_partition),' set ============='])
            idx_train = Partition(idx_partition).idx_train ;
            idx_test = Partition(idx_partition).idx_test ;
            ix_test_gallery =Partition(idx_partition).ix_test_gallery;
            %% Feature extraction
            switch AlgoOption.useDynamic
                case 1
                    % Feature fushion fashion
                    switch appType
                        case {'LDFV'}
                            tmpFeat = normc_safe(FeatApp_Keep{idx_partition}');
                        case {'ColorLBP'}
                            tmpFeat = normc_safe(FeatureAppearence');
                        case {'HistLBP'}
                            tmpFeat = normc_safe(FeatureAppearence');
                    end
                    feat_app = tmpFeat';
                    
                    if mixDyn
                        dynType = 'DynFV';
                    end
                       
                    switch dynType
                        case {'HOG3D'}
                            tmpFeat = normc_safe(feat_hog3d');
                        case {'DynFV'}
                            tmpFeat = normc_safe(FeatDyn_Keep{idx_partition}');
                    end
                    feat_dyn = tmpFeat';
                    
                    if mixDyn 
                        tmpFeat = normc_safe(feat_hog3d');
                        feat_ts = tmpFeat';
                    end
                    %                 FeatureAppearence_aug = [FeatureAppearence; app_aug.FeatureAppearence];
                    %                 tmpFeat = normc_safe(FeatureAppearence_aug');
%                     feat_hog3d_aug = [feat_hog3d; hog_aug.feat_hog3d];
%                     tmpFeat = normc_safe(feat_hog3d_aug');
%                     tmpFeat = normc_safe(FeatApp_Keep{idx_partition}');
                    

                    %                 feat_hog3d_aug = [feat_hog3d; hog_aug.feat_hog3d];
                    %                 tmpFeat = normc_safe(feat_hog3d_aug');
                    %                 feat_dyn = tmpFeat';
%                     tmpFeat = normc_safe(FeatDyn_Keep{idx_partition}');
%                     feat_dyn = tmpFeat';

                    %                 feat_hog3d_aug = [feat_hog3d; hog_aug.feat_hog3d];
                    %                 tmpFeat = normc_safe(feat_hog3d_aug');
                    %                 feat_ts = tmpFeat';

                    w_dyn = AlgoOption.dynOpt.w_dyn;
                    w_ts = AlgoOption.dynOpt.w_ts;
                    if mixDyn
                        Feature = [(1-w_dyn-w_ts).*feat_app, w_dyn.*feat_dyn,...
                                                w_ts.*feat_ts];
                    else 
                        Feature = [(1-w_dyn).*feat_app, w_dyn.*feat_dyn];
%                         Feature = [{feat_app}, {feat_dyn}];
                    end
                    %                 


                    %                 % cross validation to learn w_dyn
                    %                 feat_dyn = FeatDyn_Keep{idx_partition};
                    % %                 FeatureAppearence = FeatApp_Keep{idx_partition};
                    %                 % normalize
                    %                 tmpFeat = normc_safe(FeatureAppearence');
                    %                 FeatureAppearence = tmpFeat';
                    %                 tmpFeat = normc_safe(feat_dyn');
                    %                 feat_dyn = tmpFeat';
                    %                 w_dyn = AlgoOption.dynOpt.w_dyn;
                    %                 if ~isempty(strfind(Featurename, 'HOG3D'))
                    %                     tmpFeat = normc_safe(feat_hog3d');
                    %                     feat_dyn = tmpFeat';
                    %                 end
                    % %                 Feature{1} = FeatureAppearence;
                    % %                 Feature{2} = feat_dyn;
                    %                 Feature = [(1-w_dyn).*FeatureAppearence, w_dyn.*feat_dyn];
                    %------------------------------------------------------------------------%
                    %                 gID_cv = gID(idx_train); % use training data
                    %                 uID_cv = unique(gID_cv);
                    %                 uID_train_cv = uID_cv(randsample(numel(uID_cv),round(numel(uID_cv)/2)));
                    %                 uID_test_cv = setdiff(uID_cv,uID_train_cv);
                    %                 idx_train_cv = find(ismember(gID,uID_train_cv));
                    %                 idx_test_cv = find(ismember(gID,uID_test_cv));
                    %                 % build gallery
                    %                 camID_cv = camID(idx_test_cv);
                    %                 ucamID = unique(camID_cv);
                    %                 for cam = 1:numel(ucamID)
                    %                     ix_test_gallery_cv(cam,:) = camID_cv ~= ucamID(cam);
                    %                 end
                    %                 w_dyn = 0:0.01:1;
                    %                 PUR = zeros(1,numel(w_dyn));
                    %                 for w = 1:numel(w_dyn) % loop over the parameters
                    %                     Feature = [(1-w_dyn(w)).*FeatureAppearence, w_dyn(w).*feat_dyn];
                    %                     tmpFeature = normc_safe(Feature');
                    %                     Feature = tmpFeature';
                    %         %             feature_set{c} = bsxfun(@times, Feature{c}, 1./sum(Feature{c},2));
                    %                     train_cv = Feature(idx_train_cv,:); % training set
                    %                     test_cv = Feature(idx_test_cv,:); % test set
                    %                     [algo, V]= LFDA(double(train_cv),gID(idx_train_cv)' ,AlgoOption);
                    %                     [r, ~, ~] = compute_rank2({algo}, {train_cv}, {test_cv}, ix_test_gallery_cv, gID(idx_test_cv), AlgoOption.weight);
                    %                     [a, ~] = hist(r' ,1: size(r,2));
                    %                     a = a./repmat(ones(1,size(a,2))*size(r,2), size(a,1),1);
                    %                     if size(a,1) == size(ix_test_gallery_cv,1)
                    %                         a = a';
                    %                     end
                    %                     prob =mean(a,2);
                    %                     prob = cumsum(prob);
                    %                     PUR(w) = CalculatePUR(prob',sum(ix_test_gallery_cv(1,:)));
                    % %                     PUR(w) = prob(1);
                    %                 end
                    %                 [~,idmax] = max(PUR);
                    %                 idmax = 20;
                    %                 AlgoOption.dynOpt.w_dyn = w_dyn(idmax);
                    %----------------------------------------------------------------------%
                case 0
                    if loadfeat
                        if ~isempty(strfind(Featurename, 'HOG3D'))
                            tmpFeat = normc_safe(feat_hog3d');%
                        else 
                            tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
                        end
                    else 
                        switch featname 
                            case {'LDFV'}
                                FeatureAppearence = appFeatExtract_FV(I, idx_train, camID, num_patch,meanEdge);
                                FeatApp_Keep{idx_partition,rep} = FeatureAppearence;
                                tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
                            case {'HistLBP'}
                                FeatureAppearence = appFeatExtract_HistLBP(I, idx_train, camID, num_patch,meanEdge);
                                FeatApp_Keep{idx_partition,rep} = FeatureAppearence;
                                tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
                            case {'ColorLBP'}
                                FeatureAppearence = appFeatExtract_ColorLBP(I, idx_train, camID, num_patch,meanEdge);
                                FeatApp_Keep{idx_partition,rep} = FeatureAppearence;
                                tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
                        end
                    end
                        
%                     if ~isempty(strfind(Featurename, 'HOG3D'))
%                         tmpFeat = normc_safe(feat_hog3d');%
%                     elseif  ~isempty(strfind(Featurename, 'LDFV'))
%                         if exist(fullfile('Feature',[fname appFeaturename profix '.mat']),'file') && loadfeat
%                             tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
%                         else 
%                             FeatureAppearence = appFeatExtract_FV(I, idx_train, camID, num_patch,meanEdge);
%                             FeatApp_Keep{idx_partition,rep} = FeatureAppearence;
%                             tmpFeat = normc_safe(FeatApp_Keep{idx_partition,rep}');
%                         end
%                         
%                     else
%                         tmpFeat = normc_safe(FeatureAppearence');
%                         %                     FeatureAppearence_aug = [FeatureAppearence; app_aug.FeatureAppearence];
%                         %                     tmpFeat = normc_safe(FeatureAppearence_aug');
%                     end
                    feat_app = tmpFeat';
                    Feature = feat_app;
                case 2 % dynamic only
%                     load(fullfile('Feature',[fname '_semEdge' profix]));
                    savename = ['Result_' fname '_' AlgoOption.name '_' fname '_dyn' num2str(numel(AlgoOption.dynOpt.sl)) 'sl' profix '_' partition_name '_' kname{k} '_' timstr '.mat'];
                    if exist(['Feature/' fname '_dyn' num2str(numel(AlgoOption.dynOpt.sl)) 'sl' profix '.mat'],'file') && loadfeat
                        tmpFeat = normc_safe(FeatDyn_Keep{idx_partition,rep}');
                    else
                        tic
                        if ~exist('endP','var') || ~exist('startP','var')
                            endP = cellfun(@(x) x(:,end-1:end),denseTrj,'uni',0);
                            startP = cellfun(@(x) x(:,2:3),denseTrj,'uni',0);
                        end
                        [feat_dyn,dynwords] = dynaFeatExtract_sl(denseTrj,idx_train,idx_test,gID,camID,endP,startP,meanEdge,AlgoOption.dynOpt);
                        toc;
                        FeatDyn_Keep{idx_partition,rep} = feat_dyn;
                        tmpFeat = normc_safe(feat_dyn');
                    end
                    feat_dyn = tmpFeat';
                    Feature = feat_dyn;
            end
            % cell???
            if ~iscell(Feature)
                tmpF{1} = Feature;
                Feature = tmpF;
            end
            % normalize
            feature_set = cell(1,numel(Feature));
            train = cell(1,numel(Feature));
            test = cell(1,numel(Feature));
            algo = cell(1,numel(Feature));
            for c = 1:numel(Feature)
                if numel(gID)~=size(Feature{c},1) % make sure the feature is N-by-d
                    Feature{c} = double(Feature{c})';
                end
                feature_set{c} = Feature{c};
                train{c} = feature_set{c}(idx_train,:); % training set
                test{c} = feature_set{c}(idx_test,:); % test set
            end
            if  strcmp(AlgoOption.name, 'PCCA') % setting the training pair samples
                Nneg = min(AlgoOption.npratio* length(ix_train_pos_pair), length(ix_train_neg_pair));
                ix_pair = [ix_train_pos_pair ; ix_train_neg_pair(1:Nneg,:) ]; % both positive and negative pair index
                y = [ones(size(ix_train_pos_pair,1), 1); -ones(Nneg,1)]; % annotation of positive and negative pair
            end
            %% Train
            for c = 1:numel(train)
                switch AlgoOption.func
                    case {'PCCA'}
                        [algo{c}, L, AKA] = PCCA(double(train{c}), ix_pair, y, AlgoOption);
                    case {'LFDA'}
                        [algo{c}, V]= LFDA(double(train{c}),gID(idx_train)' ,AlgoOption);
                    case {'MFA'}
                        [algo{c}, V] = MFA(double(train{c}), gID(idx_train)', AlgoOption);
                    case {'oLFDA'}
                        [algo{c}, V]= oLFDA(single(train{c}),gID(idx_train)' ,AlgoOption);
                    case {'rankSVM'}
                        % rearrange the feature and gID to fillfile rankSVM
                        % code
                        gID_train = gID(idx_train);
                        gID_rearr = zeros(size(gID_train));
                        train_rearr = zeros(size(train{c}));
                        uID = unique(gID_train);
                        num_perI = hist(gID_train,uID);
                        for u = 1:numel(uID)
                            tmpID = uID(u);
                            gID_rearr(sum(num_perI(1:u-1))+1:sum(num_perI(1:u)))...
                                = repmat(tmpID,1,num_perI(u));
                            train_rearr(sum(num_perI(1:u-1))+1:sum(num_perI(1:u)),:) ...
                                = train{c}(gID_train==tmpID,:);
                        end
                        parm.TrainFeatures = train_rearr';
                        parm.TrainImagesNumPerPerson = num_perI;
                        algo{c}.name = 'rankSVM';
                        algo{c}.kernel= [];%option.kernel;
                        algo{c}.Prob = [];
                        algo{c}.Dataname = AlgoOption.dataname;
                        algo{c}.Ranking = [];
                        algo{c}.Dist = [];
                        algo{c}.Trainoption=AlgoOption;                        
                        algo{c}.P = svm_struct_learn('-c 0.001', parm);
%                         algo{c}.P = ones(size(train{1},2),1);
                    case {'unsuper'}
                        algo{c}.name = 'unsuper';
                        algo{c}.kernel= [];%option.kernel;
                        algo{c}.Prob = [];
                        algo{c}.Dataname = AlgoOption.dataname;
                        algo{c}.Ranking = [];
                        algo{c}.Dist = [];
                        algo{c}.Trainoption=AlgoOption;                        
%                         algo{c}.P = svm_struct_learn('-c 0.01', parm);
                        algo{c}.P = ones(size(train{1},2),1);
                end
            end
            %% Test
            %         [r, dis, Qxx] = compute_rank2(algo(2), train(2), test(2), ix_test_gallery, gID(idx_test),AlgoOption.weight);
                    % generate CMC
            if strcmp(AlgoOption.func,'unsuper')
                confusionMat = pdist2(test{1},test{1});
                gID_test = gID(idx_test);
                for ix = 1:size(ix_test_gallery,1)
                    ID_gal = find(ix_test_gallery(ix,:)==1);
                    ID_prob = find(ix_test_gallery(ix,:)==0);
                    tmpr = [];
                    for g = 1:numel(ID_prob)
                        [~,sortx] = sort(confusionMat(ID_prob(g),ID_gal));
            %             [~,sortx] = sort(testKdis{1}(ID_gal(g),ID_prob));
                        tmpr(g) = find(gID_test(ID_gal(sortx))==gID_test(ID_prob(g)));
                    end
                    r(ix,:) = tmpr;
                end
                dis = confusionMat;
            else
                [r, dis, Qxx] = compute_rank2(algo, train, test, ix_test_gallery, gID(idx_test), AlgoOption.weight);
            end
            pdtable = pairwiseDist(algo,train,test);
            %-----------------debug----------------%
            % keep the rank for each person
            gID_test = gID(idx_test);
            %         for p = 1:size(ix_test_gallery,1)
            %             gID_prob = gID_test(~ix_test_gallery(p,:));
            %             for pi = 1:numel(gID_prob)
            %                 personrank{unique(gID)==gID_prob(pi)} = [personrank{unique(gID)==gID_prob(pi)}, r(p,pi)];
            %             end
            %         end
            %--------------------------------------%
            %% Show the result
            [a, b] = hist(r',1:sum(ix_test_gallery(1,:)==1));
            if min(min(double(ix_test_gallery)))<0
                a = cumsum(a)./repmat(sum(ix_test_gallery==-1,2)', size(a,1),1);
            else
                a = cumsum(a)./repmat(sum(ix_test_gallery==0,2)', size(a,1),1);
            end
            if size(a,1) ~= size(ix_test_gallery,1)
                a = a';
            end
            for itr =1: size(a,1)
                rr(itr,:)= [a(itr,1) a(itr,5) a(itr,10) a(itr,20) a(itr,25)];
                display(['itration ' num2str(itr) ' Rank 1 5 10 20 25 accuracy ===>' num2str(rr(itr,:)) ' ====']);
            end
%             display(num2str([mean(rr,1)]));
            %% Save the result
            CMC{idx_partition,rep} = a;
            PUR(idx_partition,rep) = CalculatePUR(mean(a,1),sum(ix_test_gallery(1,:)))*100;
            display(num2str([mean(rr(:,1:4),1)*100 PUR(idx_partition,rep)]));
            Method{idx_partition, rep} = algo{1};
            for c = 1:numel(algo)
                tmpP = algo{c}.P;
            end
            Method{idx_partition, rep}.P = tmpP;
            Method{idx_partition, rep} = algo{1};
            Method{idx_partition, rep}.Prob = a;
            Method{idx_partition, rep}.Ranking = r;
            Method{idx_partition, rep}.Dist = dis;
            Method{idx_partition, rep}.pdtable = pdtable;
            %         Method{idx_partition, iset}.time = elTime;
            save(savename, 'AlgoOption','Method','CMC','PUR');
        end
    end
end

if savefeat
    if exist('FeatApp_Keep','var') && ~isempty(FeatApp_Keep{1})
        save([fname '_LDFV' num2str(num_patch) 'Patch64' profix], 'FeatApp_Keep','-v7.3');
    end
    if exist('FeatDyn_Keep','var') && ~isempty(FeatDyn_Keep{1})
        save([fname '_dyn' num2str(numel(AlgoOption.dynOpt.sl)) 'sl' profix], 'FeatDyn_Keep','-v7.3');
    end
end
% Res_tp = [Res_tp; mean(rr,1)];
    end
end
%