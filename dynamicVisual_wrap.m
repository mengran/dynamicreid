function [ dynMat ] = dynamicVisual_wrap(trjMat,trainCenter,num_strip)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% visualize the dynamic word
centerMat = cell2mat(trainCenter);
label_centerS = reshape(repmat(1:num_strip,size(centerMat,2)/num_strip,1),1,[]);
DFcenter = pairwiseDynamicsDistance(centerMat);
% 6 main classes: R,G,B,Y,C,M
[label_centerC,~,~] = ncutW(1+DFcenter,6);
% generate color space
lowB = 0.2;
colorCluster = zeros(size(centerMat,2),3);
for l = 1:6
    num_withinC = sum(label_centerC(:,l));
    tmpColor = repmat([l/6 0 1],num_withinC,1);
    tmpColor(:,2) = linspace(lowB,1,num_withinC);
    tmpColor = hsv2rgb(tmpColor);
    colorCluster(logical(label_centerC(:,l)),:) = tmpColor;
end

dynMat = zeros([32 16 3]);
dynCount = zeros(32,16);
yrange = linspace(0,129,num_strip+1);
endY = trjMat(:,end);
label_strip = zeros(1,numel(endY));
for i = 1:num_strip
    label_strip(endY>=yrange(i) & endY<yrange(i+1)) = i;
end
params.num_km_init_word = 3;
params.MaxInteration = 3;
params.labelBatchSize = 200000;
params.actualFilterThreshold = -1;
params.find_labels_mode = 'DF';
% visualization
for s = 1:num_strip
    % label each tracklet
    tmpTrj = trjMat(label_strip==s,:)';
    tmpTrj = tmpTrj(2:end,:);
    tmpTrj(1:2:end,:) = bsxfun(@minus,tmpTrj(1:2:end,:),mean(tmpTrj(1:2:end,:),1));
    tmpTrj(2:2:end,:) = bsxfun(@minus,tmpTrj(2:2:end,:),mean(tmpTrj(2:2:end,:),1)); 
    if isempty(tmpTrj)
        continue;
    end
    [tmplabel, ~, ~] = find_weight_labels_df_HHp_newProtocal(trainCenter(s), tmpTrj, params);
%     tmplabel = label_word(label_trajID==i & label_strip==s);
    tmpTrj = trjMat(label_strip==s,:)';
    tmpMat = dynamicVisual(tmpTrj(2:end,:),tmplabel,[128 64],colorCluster(label_centerS==s,:));
    tmpCount = tmpMat(:,:,1)>0|tmpMat(:,:,2)>0|tmpMat(:,:,3)>0;
    [confr,confc] = find(tmpCount&logical(dynCount));
    tmpMat(confr,confc,:) = 0;
    dynCount = tmpCount|logical(dynCount);
    dynMat = dynMat + tmpMat;
end


end

