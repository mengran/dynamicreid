% dynamic feature extraction
function [F,trainCenter] = dynaFeatExtract_sl(...
    denseTrj,train,test,gID,camID,endp,dynOpt)

% addpath(genpath('C:\Users\Mengran\Dropbox\Research\Code\hankelet-master\hankelet-master'));
% run('C:\Users\mgou\Documents\util\vlfeat-0.9.17\toolbox\vl_setup.m');
% label all trajecteries

% label each trajectory with the gID it belongs to 
[numTrjPerI,~] = cellfun(@size, denseTrj,'UniformOutput',1);
index = cumsum(numTrjPerI);
index = [0,index];
label_trajID = zeros(1,index(end));
for i = 1:numel(index)-1
    label_trajID(index(i)+1:index(i+1)) = i;
end
ind_train = ismember(label_trajID,train); % training trajs label

% assign spatial label for each traj based on its ending point position
warning off
nr = 6;
nc = 3;
normSz = [128 64]; % rxc
[~, BBox, ~] = GenerateGridBBox_numP(normSz, nr, nc );
BBox = ceil(BBox);
endp = cell2mat(endp');
label_strip = zeros(numel(label_trajID),nr*nc);
for g = 1:nr*nc
    xv = [BBox(g,1),BBox(g,1),BBox(g,3),BBox(g,3),BBox(g,1)];
    yv = [BBox(g,2),BBox(g,4),BBox(g,4),BBox(g,2),BBox(g,2)];  
    label_strip(:,g) = inpolygon(endp(:,1),endp(:,2),xv,yv);
end
label_strip = logical(label_strip);
num_strip = size(label_strip,2);

% two extraction mode
% 1----bag of dynamic words (not use)
% 2----fisher vector
switch dynOpt.mode
    case 1
        tmpTrj = cell2mat(denseTrj');
        tmpTrj = tmpTrj(:,2:end);   
        % calculate the speed
        tmpX = tmpTrj(:,1:2:end);
        tmpY = tmpTrj(:,2:2:end);
        velX = tmpX(:,1:end-1)-tmpX(:,2:end);
        velY = tmpY(:,1:end-1)-tmpY(:,2:end);  
        normfact = sqrt(velX.^2 + velY.^2);
        normfact = sum(normfact,2);
        velX = bsxfun(@times,velX,1./normfact);
        velY = bsxfun(@times,velY,1./normfact);
        tmpTrj = zeros(size(velX,1),size(velX,2)*2);
        tmpTrj(:,1:2:end) = velX;
        tmpTrj(:,2:2:end) = velY;
        trjMat = tmpTrj;
        clear tmpTrj
        
        Ndist = [];
%         F = {};
        trainCenter = {};   
%         load('centerVel_iLIDSVID_p1.mat');
        % parameters setting
        ncenter = dynOpt.ncenter;
        params.num_km_init_word = 1;
        params.MaxInteration = 3;
        params.labelBatchSize = 200000;
        params.actualFilterThreshold = -1;
        params.find_labels_mode = 'DF';
        trainCenter = cell(1,num_strip);
        paramCell = cell(1,num_strip);
        pool_turner;
%         ncut_subnum = 5000;
        for s = 1:num_strip
            tmpTrj = trjMat(ind_train & label_strip(:,s),:);
%             trjPool = trjMat(ind_train & label_strip==s,:);
%             trjPool = trjPool(:,2:end);
%             % remove mean
%             trjPool(:,1:2:end) = bsxfun(@minus,trjPool(:,1:2:end),mean(trjPool(:,1:2:end),2));
%             trjPool(:,2:2:end) = bsxfun(@minus,trjPool(:,2:2:end),mean(trjPool(:,2:2:end),2));
            % calculate the speed
%             tmpX = trjPool(:,1:2:end);
%             tmpY = trjPool(:,2:2:end);
%             velX = tmpX(:,1:end-1)-tmpX(:,2:end);
%             velY = tmpY(:,1:end-1)-tmpY(:,2:end);  
%             normfact = sqrt(velX.^2 + velY.^2);
%             normfact = sum(normfact,2);
%             velX = bsxfun(@times,velX,1./normfact);
%             velY = bsxfun(@times,velY,1./normfact);
%             tmpTrj = zeros(size(velX,1),size(velX,2)*2);
%             tmpTrj(:,1:2:end) = velX;
%             tmpTrj(:,2:2:end) = velY;
%             trjPool = tmpTrj;            
            % compute the pairwise distances
%             randind = randsample(size(trjPool,1),min(size(trjPool,1),ncut_subnum));
%             subtrjPool = trjPool(randind,:)';
%             DF = pairwiseDynamicsDistance(subtrjPool);
            % compute the pairwise similarity matrix
            %         DF = -DF;
            %         scale_sig = 0.05*max(DF(:));
            %         tmp = (DF).^2;
            %         sMat = exp(-tmp);
            % Ncut to get the clusters
%             [NcutDiscrete,~,NcutEigenvalues] = ncutW(1+DF,dynOpt.ncenter);
            % Extract the word of each cluster
%             trainCenter = zeros(size(trjPool,2),ncenter);
%             for c = 1:ncenter
%                 data_cluster = subtrjPool(:,logical(NcutDiscrete(:,c)));
%                 [cluster.XHHp,cluster.XHHpFrob,m] = calHHp(data_cluster);
%                 w = [ones(1, 2 * m) 2 * ones(1, length(2*m+1 : size(cluster.XHHp, 1)))];
%                 DFc = relativeDynamicsDistance_HHp(cluster.XHHp, cluster.XHHpFrob,...
%                     1:size(data_cluster,2),w);
%                 tmpdist = sum(DFc);
%                 [~,cind] = max(tmpdist);
%                 trainCenter{s}(:,c) = data_cluster(:,cind);
%             end
            %         % extract dynamic word               
            for k = 1:params.num_km_init_word
                [~, trainCenter{s}, trainClusterMu, trainClusterSigma, ...
                    trainClusterNum] = litekmeans_subspace(tmpTrj', ncenter,params);
                params.trainClusterInfo{k}.mu = trainClusterMu;
                params.trainClusterInfo{k}.sigma = trainClusterSigma;
                params.trainClusterInfo{k}.num = trainClusterNum;

                params.trainClusterNum{k} = size(trainCenter{s}, 2);
                params = cal_cluster_info(params);
                paramCell{s} = params;
            end
%             [label, trainCenter, ~, ~, histTrain] = litekmeans_subspace(trjPool', ncenter,params);
        end
        label_word = zeros(1,index(end));
        % calculate the histgram for each person    
        F = zeros(numel(denseTrj),ncenter*num_strip);
        for i = 1:numel(denseTrj)
            tmpF = zeros(ncenter, num_strip);
            id = i;%train(i);
            for s = 1:num_strip
                tmpTrj = trjMat(label_trajID==id & label_strip(:,s),:)';
                if isempty(tmpTrj)
                    tmpF(:,s) = zeros(1,ncenter);
                    continue;
                end
%                 tmpTrj = tmpTrj(2:end,:);
%                 tmpTrj(1:2:end,:) = bsxfun(@minus,tmpTrj(1:2:end,:),mean(tmpTrj(1:2:end,:),1));
%                 tmpTrj(2:2:end,:) = bsxfun(@minus,tmpTrj(2:2:end,:),mean(tmpTrj(2:2:end,:),1));

%                 % calculate the speed
%                 tmpX = tmpTrj(1:2:end,:);
%                 tmpY = tmpTrj(2:2:end,:);
%                 velX = tmpX(1:end-1,:)-tmpX(2:end,:);
%                 velY = tmpY(1:end-1,:)-tmpY(2:end,:);  
%                 normfact = sqrt(velX.^2 + velY.^2);
%                 normfact = sum(normfact,2);
%                 velX = bsxfun(@times,velX,1./normfact);
%                 velY = bsxfun(@times,velY,1./normfact);
%                 tmpTrj = zeros(size(velX,1)*2,size(velX,2));
%                 tmpTrj(1:2:end,:) = velX;
%                 tmpTrj(2:2:end,:) = velY; 
%                 [tmplabel, tmpNdist, tmpHist] = find_weight_labels_df_HHp_newProtocal(trainCenter(s), tmpTrj, params);

                [LABEL, ~, CLASS_HIST] = find_weight_labels_df_HHp_newProtocal({trainCenter{s}}, tmpTrj, paramCell{s});
                tmpF(:,s) = CLASS_HIST;
                label_word(label_trajID==id & label_strip(:,s)) = LABEL;
            end
%             F(i,:) = normblock(tmpF);
            tmpF = normc_safe(tmpF);
            F(i,:) = tmpF(:)';
%             F{id} = tmpF;
            %             F = bsxfun(@times, F, 1./sum(F,2));
            %             F(isnan(F))=0;           
        end
        %% Bilingual denoise  
        if dynOpt.thBiDiff > 0
            diffHist = {};
            bihist = {};
            for i = 1:numel(train)/2
                hist1 = F{train(i)};
                hist2 = F{train(i)+300};
                diffHist{i} = abs(hist1-hist2);
                bihist{i} = hist1.*hist2;
            end
            tmpdiffhist = cell2mat(diffHist);
            tmpbihist = cell2mat(bihist);
            diffMat = zeros(num_strip,ncenter);
            zeroCount = zeros(num_strip,ncenter);
            for s = 1:num_strip
                for w = 1:ncenter
                    tmpsw = tmpdiffhist(s,w:16:end);
                    diffMat(s,w) = median(tmpsw);
                    zeroCount(s,w) = sum(tmpsw==0);
                end
            end
            label_bilingual_mat = zeroCount<0.3*numel(train)/2; % not bilingual for more than 30% samples
            for f = 1:numel(F)
                tmpF = F{f};
                tmpF(~label_bilingual_mat) = 0;
                Feat(f,:) = normblock(tmpF);
            end
            F = Feat;
            label_bilingual = ones(1,numel(label_word));
            for s = 1:num_strip
                label_bilingual(label_strip(:,s) & ismember(label_word,find(label_bilingual_mat(s,:)==0))) = 0;
            end
        end
        
        %% Fisher kernel encoding
        if 0
        % whitening 
        Fw = cell(1,num_strip);
        for s = 1:num_strip
            tmpF = F(:,(s-1)*ncenter+1:s*ncenter);
            tmpF = whitening(tmpF);
            Fw{s} = tmpF;
        end
        F = cell2mat(Fw);
%         F = F';
        ncenter = size(Fw{1},2);
        GMMnode = 8;
        F_train = F(train,:);
        for s = 1:num_strip
            tmpF = F_train(:,(s-1)*ncenter+1:s*ncenter);
            [means{s}, covariances{s}, priors{s},~,~] = vl_gmm(tmpF',GMMnode,'NumRepetitions',10);            
        end
        F_fv = zeros(size(F,1),size(F,2)*2*GMMnode);
        for f = 1:size(F,1)
            tmpF = F(f,:);
            tmpF_fv = zeros(ncenter*2*GMMnode,num_strip);
            for s = 1:num_strip
                tmpF_fv(:,s) = vl_fisher(tmpF((s-1)*ncenter+1:s*ncenter)',...
                    means{s}, covariances{s}, priors{s},'Normalized','SquareRoot');
            end
            F_fv(f,:) = tmpF_fv(:)';
        end
        % discrimnative analysis
%         hist_mean = mean(F(train,:));
%         hist_std = std(F(train,:));
%         F = bsxfun(@minus,F,hist_mean);
%         % std normalization
%         F = bsxfun(@times,F,1./(hist_std+eps));
        F_fv = normc_safe(F_fv');
        F = F_fv';
        end
    case 2
        tmpTrj = cell2mat(denseTrj');
        tmpTrj = tmpTrj(:,2:end);        
        
        % calculate the normalized speed
        tmpX = tmpTrj(:,1:2:end);
        tmpY = tmpTrj(:,2:2:end);
        velX = tmpX(:,2:end)-tmpX(:,1:end-1);
        velY = tmpY(:,2:end)-tmpY(:,1:end-1);
        normfact = sqrt(velX.^2 + velY.^2);
        normfact = sum(normfact,2);
        velX = bsxfun(@times,velX,1./normfact);
        velY = bsxfun(@times,velY,1./normfact);
%         tmpTrj = zeros(size(velX,1),size(velX,2)*2);
%         tmpTrj(:,1:2:end) = velX;
%         tmpTrj(:,2:2:end) = velY;

% %         remove mean
%         tmpTrj(:,1:2:end) = bsxfun(@minus,tmpTrj(:,1:2:end),mean(tmpTrj(:,1:2:end),2));
%         tmpTrj(:,2:2:end) = bsxfun(@minus,tmpTrj(:,2:2:end),mean(tmpTrj(:,2:2:end),2));
        
        % sliding window augment feature
        clear tmpTrj tmpX tmpY      
        sl_win_m = dynOpt.sl;
        if 1 % given the length of the short trajs, fully overlapped
            sl_win_m = dynOpt.sl;
            for sl = 1:numel(sl_win_m) % loop on different length
                sl_win = sl_win_m(sl);
                tmpTrj{sl} = zeros(size(velX,1)*(size(velX,2)-sl_win+1),sl_win*2,'single');     
                label_sl{sl} = [];
                for slw = 1:size(velX,2)-sl_win+1 % loop on slides
                    tmpTrj{sl}((slw-1)*size(velX,1)+1:slw*size(velX,1),1:2:end) = velX(:,slw:slw+sl_win-1);
                    tmpTrj{sl}((slw-1)*size(velX,1)+1:slw*size(velX,1),2:2:end) = velY(:,slw:slw+sl_win-1);
                    label_sl{sl} = [label_sl{sl}, slw*ones(1,size(velX,1))]; 
                end
                label_trajID_sl{sl} = repmat(label_trajID,1,size(velX,2)-sl_win+1);
                label_strip_sl{sl} = repmat(label_strip,size(velX,2)-sl_win+1,1);
                ind_train_sl{sl} = repmat(ind_train,1,size(velX,2)-sl_win+1);
            end
        else % givin the numver of number segments per layer, no overlapping
            sh_num = dynOpt.sl; 
            for sh = 1:numel(sh_num) % loop on different layers
                shn = sh_num(sh);
                sl_win = floor(size(velX,2)/shn);
                curP = 1;                
                tmpTrj{sh} = zeros(size(velX,1)*shn,sl_win*2,'single');
                label_sl{sh} = [];
                for tmplabel = 1:shn % loop on segments
                    tmpTrj{sh}((tmplabel-1)*size(velX,1)+1:tmplabel*size(velX,1),1:2:end) = velX(:,curP:curP+sl_win-1);
                    tmpTrj{sh}((tmplabel-1)*size(velX,1)+1:tmplabel*size(velX,1),2:2:end) = velY(:,curP:curP+sl_win-1);
                    label_sl{sh} = [label_sl{sh}, tmplabel*ones(1,size(velX,1))]; 
                    curP = curP+sl_win;                    
                end
                label_trajID_sl{sh} = repmat(label_trajID,1,shn);
                label_strip_sl{sh} = repmat(label_strip,1,shn);
                ind_train_sl{sh} = repmat(ind_train,1,shn);
            end 
        end
        
        switch dynOpt.RP
            case 0      
                for sl = 1:numel(tmpTrj)
                    tmpfeature = tmpTrj{sl}';
                    feature{sl} = tmpfeature;
%                     feature{sl} = normc_safe(tmpfeature);
                end
                clear tmpTrj;
            case 1
                % random projection hankel
                disp('Begin to random project...')
                randp = randn(dynOpt.num_w,floor((size(tmpTrj,2)/2+1)/2));
                randp = bsxfun(@times,randp,1./sqrt(sum(randp.^2,2)));
                wrapTrj = zeros(size(tmpTrj,1),size(tmpTrj,2)/2,2,'single');
                wrapTrj(:,:,1) = single(tmpTrj(:,1:2:end));
                wrapTrj(:,:,2) = single(tmpTrj(:,2:2:end));
                clear tmpTrj;
                [XX, YY] = meshgrid(1:dynOpt.num_w, 1:size(wrapTrj,1));
                idx_pair = [YY(:),XX(:)]; 
                [R] = gpu_HankelMultiRegressor_pair(wrapTrj, randp, idx_pair);
                feature = reshape(R,size(wrapTrj,1),dynOpt.num_w);        
                clear R;
                feature = feature';
                 % only keep the most intensive ones
                giniInd = gini(feature');
                [~,idg] = sort(giniInd,'descend');
                feature = feature(idg(1:dynOpt.num_w/2),:);
            case 2
                % random projection hankel
                disp('Begin to random project...')
                randp = randn(dynOpt.num_w,2*floor((size(tmpTrj,2)/2+1)/2));
                randp = bsxfun(@times,randp,1./sqrt(sum(randp.^2,2)));
                H = buildhankel(tmpTrj);
                feature = randproj(H, single(randp));
                feature = feature';            
                 % only keep the most intensive ones
                giniInd = gini(feature');
                [~,idg] = sort(giniInd,'descend');
                feature = feature(idg(1:dynOpt.num_w/2),:);
        end        
%         feature = normc_safe(feature);
       
%%      OPTIONAL: Whitening PCA (at least Whitening might be needed because of the assumption on covariance matrix)
%         % feature is colomn wise        
%         if ~iscell(feature)
%             feature = {feature};
%         end
%         for c = 1:numel(feature)            
%             feature{c} = whitening(feature{c});
% % %             feature = feature(1:round(size(feature,1)/2),:); % only keep partial feature
%         end
%%      Build GMM
        % EM, learning GMM model for each strip/patch
        disp('Begin to extract FV...')
        for s = 1:num_strip
            for sl = 1:numel(sl_win_m)
                switch dynOpt.slfashion
                    case 1 %each sl window size bulid one FV (hankelize);
                        feature_tmp = feature{sl}(:,ind_train_sl{sl} & label_strip_sl{sl}(:,s)');
                        subind = randsample(size(feature_tmp,2),min(size(feature_tmp,2),50000)); % subsample to speed up
                        feature_train = feature_tmp(:,subind);        
                        [means{sl}{s}, covariances{sl}{s}, priors{sl}{s},~,~] = vl_gmm(feature_train,dynOpt.ncenter,'NumRepetitions',10);
                    case 2 %each sl bulid one FV
                        for slw = 1:max(label_sl{sl})
                            feature_tmp = feature{sl}(:,ind_train_sl{sl} & label_strip_sl{sl}(:,s)' & label_sl{sl}==slw);
                            subind = randsample(size(feature_tmp,2),min(size(feature_tmp,2),50000)); % subsample to speed up
                            feature_train = feature_tmp(:,subind);        
                            [means{sl}{s}{slw}, covariances{sl}{s}{slw}, priors{sl}{s}{slw},~,~] = vl_gmm(feature_train,dynOpt.ncenter,'NumRepetitions',10);                            
                        end
                end
            end 
        end
%             [means{s}, covariances{s}, priors{s}] = vl_gmm(feature(:,ind_train&label_strip==s),dynOpt.ncenter,'NumRepetitions',5);
%             [means, covariances, priors] = vl_gmm(feature(:,label_bilingual&ind_train),dynOpt.ncenter);

%%      Binlingual Denoise (not use)
        if dynOpt.thBiDiff > 0
            gID_train = gID(train);
            camID_train = camID(train);
            uID = unique(gID_train);
            diffPrior = cell(1,numel(uID));
            meanPrior = cell(1,numel(uID));
            for i = 1:numel(uID)
                id = uID(i);
                tmpPair = find(gID==id);
                tmpPrior = cell(1,2);
                for p = 1:2
                    tmpFeature = feature(:,label_trajID==tmpPair(p));
                    % compute gaussian priors
                    tmpPrior{p} = zeros(num_strip,dynOpt.ncenter);
                    for s = 1:num_strip
                        auxDis = zeros(size(tmpFeature,2),dynOpt.ncenter);
                        for c = 1:dynOpt.ncenter
                            auxDis(:,c) = priors{s}(c).*normpdf2(tmpFeature,means{s}(:,c),diag(covariances{s}(:,c)));
                        end                
        %                 auxDis(isnan(auxDis)) = 0;
                        auxDis = bsxfun(@times,auxDis,1./sum(auxDis,2));
                        auxDis(isnan(auxDis)) = 0;
                        auxDis(auxDis == inf) = realmax;
                        tmpPrior{p}(s,:) = sum(auxDis)./sum(auxDis(:));
                    end
                end
                diffPrior{i} = abs(tmpPrior{1} - tmpPrior{2});
                meanPrior{i} = (tmpPrior{1} + tmpPrior{2})/2;
            end
%             load('prior_w200_s6_vel.mat');
            matdiffPrior = cell2mat(diffPrior);
            matmeanPrior = cell2mat(meanPrior);
            diffMat = zeros(num_strip,dynOpt.ncenter);
            meanMat = zeros(num_strip,dynOpt.ncenter);
            zeroCount = zeros(num_strip,dynOpt.ncenter);
            for s = 1:num_strip
                for w = 1:dynOpt.ncenter
                    tmpsw = matdiffPrior(s,w:dynOpt.ncenter:end);
                    zeroCount(s,w) = sum(tmpsw>dynOpt.thBiDiff);
                    diffMat(s,w) = mean(tmpsw);
                    tmpsw = matmeanPrior(s,w:dynOpt.ncenter:end);
                    meanMat(s,w) = mean(tmpsw);                
                end
            end
            
            label_bilingual_mat = ones(size(diffMat));
            for s = 1:num_strip
                [~,tmpidx] = sort(diffMat(s,:));
                label_bilingual_mat(s,tmpidx(1:dynOpt.thBiDiff))=0;             
            end
%             label_bilingual_mat = diffMat < dynOpt.thBiDiff; %& meanMat > dynOpt.thBiMean;    
        end
%% Fisher vector encoding
        for i = 1:numel(denseTrj)
            clear tmpF;
            switch dynOpt.slfashion
                case 1
                    for sl = 1:numel(sl_win_m)
                        tmpF{sl} = zeros(dynOpt.ncenter*size(feature{sl},1)*2,num_strip);
                    end
                    for s = 1:num_strip                
                        for sl = 1:numel(sl_win_m)                            
                            tmpFeature = feature{sl}(:, label_trajID_sl{sl}==i & label_strip_sl{sl}(:,s)');
                            % encoding
                            % TODO: adding gassian weights
                            tmpF{sl}(:,s) = vl_fisher(tmpFeature, means{sl}{s}, covariances{sl}{s}, priors{sl}{s});                            
                        end
                    end
                    % Normalize    
                    for sl = 1:numel(sl_win_m)
                        tmpF{sl} = powerNormalization(tmpF{sl}); % power
                        tmpF{sl} = normc_safe(tmpF{sl});
                    end
                case 2 
                    for sl = 1:numel(sl_win_m)
                        tmpF{sl} = zeros(max(label_sl{sl})*dynOpt.ncenter*size(feature{sl},1)*2,num_strip);
                    end
                    for s = 1:num_strip                
                        for sl = 1:numel(sl_win_m)
                            tmpF_sl = zeros(dynOpt.ncenter*size(feature{sl},1)*2,max(label_sl{sl}));
                            for slw = 1:max(label_sl{sl}) % for every sliding window
                                tmpFeature = feature{sl}(:,label_trajID_sl{sl}==i & label_strip_sl{sl}(:,s)' & ...
                                    label_sl{sl}==slw);
                                %             tmpFeature = feature(:,label_trajID==i&label_bilingual);
                                % encoding
                                % TODO: adding gassian weights
                                tmpF_sl(:,slw) = vl_fisher(tmpFeature, means{sl}{s}{slw}, covariances{sl}{s}{slw}, priors{sl}{s}{slw});
                                %                     tmpF(:,s) = vl_fisher(tmpFeature, means{s}, covariances{s}, priors{s});
                            end
                            tmpF_sl = powerNormalization(tmpF_sl);
                            tmpF_sl = normc_safe(tmpF_sl);
                            tmpF{sl}(:,s) = tmpF_sl(:);
                        end
                        
                    end
            end
            tmpF = cell2mat(tmpF');
            if dynOpt.thBiDiff > 0
                %throw away the non-bilingal gaussian
                nonBi = find(label_bilingual_mat(s,:)==0);
                idxrm = [];
                for nb = 1:numel(nonBi)
                    tmpidxrm = [size(feature,1)*(nonBi(nb)-1)+1:size(feature,1)*nonBi(nb)];
                    tmpidxrm = [tmpidxrm, tmpidxrm+size(feature,1)*dynOpt.ncenter];
                    idxrm = [idxrm, tmpidxrm];
                end
                tmpF(idxrm,:) = [];
            end
            F(i,:) = tmpF(:);
        end
        
        trainCenter = means;
end
end


function X = l2Normalization(X)

D = size(X,1);

L2 = sum(X.^2,1).^0.5;
X = X./bsxfun(@times,L2,ones(D,1));

end

function X = powerNormalization(X)

X = sign(X).*(abs(X).^0.5);

end
function H = buildhankel(X)

hankelWindowSize = floor((size(X,2)/2+1)/2);
    parfor n = 1:size(X,1)
        tmpX = X(n,:);
        feature = [tmpX(1:2:end);tmpX(2:2:end)];
        H{n} = hankelConstruction(feature, hankelWindowSize);
        H{n} = H{n}*H{n}';
    end
end

function kHankel = hankelConstruction(FrameFeatures, HankelWindowSize)
% Input:    
% FrameFeature:     d x n , d is dimension of the input features. such as [
%                   x, y] location then d = 2;
%                   n: number of feature points.
% HankelWindowSize: as Named
% Binlong Li        2/7/2012

idx = hankel(1:HankelWindowSize, HankelWindowSize:size(FrameFeatures, 2));
k = FrameFeatures(:, idx);
kHankel = reshape(k, size(idx, 1) * size(k, 1), size(idx, 2));
end


function feature = randproj(H, randpro)
% Normalize random projection
% randpro = randpro./repmat(sum(randpro.^2,2),1,size(randpro,2));
randpro = normc_safe(randpro');
randpro = randpro';

% Generate features
feature = zeros(length(H),size(randpro,1));
parfor i = 1:length(H)
    feature(i,:) = diag(randpro*H{i}*randpro')';
end
end

function featW = whitening(feat)
feat = feat';
feat = bsxfun(@minus, feat,mean(feat,2)); % remove mean
[pcaEigVec,pcaCenter,pcaEigVal] = pca(feat); % pca
featW = diag(pcaEigVal.^-0.5)*pcaEigVec'*feat; % whitening 
featW = featW(1:round(size(featW,1)/2),:); % only keep partial feature
featW = featW';
end
    