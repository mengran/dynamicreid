clc
clear 
CMC = [];
%% SAIVT
name = {
'HistLBP'
'Color&LBP'
'LDFV'
'DFV'
'HOG3D'
'HOG3D+DFV'
'HOG3D+Color&LBP'
'DFV+Color&LBP'
'HOG3D+LDFV'
'DFV+LDFV'
'DFV+HOG3D+Color&LBP'
'DFV+HOG3D+LDFV'
};
% plot(CMC');
rankshow = 20; 

figure,hold on
p_color = [0.64 0.08 0.18;
           0 0.45 0.74;
           0 0.5 0;
           1 0.84 0;
           0.85 0.7 1];
makerpool = ['^', 'o', 's', 'v', '*'];
idsub = {[1 2 3],[4],[5 6],[7 8 9 10],[11 12]}; 
for sub = 1:numel(idsub)
    for str = 1:numel(idsub{sub})
        plot(CMC(idsub{sub}(str),1:rankshow),['-' makerpool(str)],'Color',p_color(sub,:));
    end
end

xlim([1 rankshow]);
grid on
xlabel('Rank', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('Matching Rate (%)','FontSize', 12, 'FontWeight', 'bold');
legend (name,'FontSize',10,'Location','southeast');
% saveas(gcf,'CMC_SAIVT.png');
%% iLIDSVID/PRID BK
% CMC = [];
name = {
'HistLBP'
'Color&LBP'
'LDFV'
'DFV'
'HOG3D'
'HOG3D+DFV'
'DVR'
'HOG3D+Color&LBP'
'DFV+Color&LBP'
'DVR+Color&LBP'
'HOG3D+LDFV'
'DFV+LDFV'
'DFV+HOG3D+Color&LBP'
'DFV+HOG3D+LDFV'
};
idsub = {[1 2 3],[4],[5 6 7],[8 9 10 11 12],[13 14]}; 

rankshow = 20;

figure,hold on
p_color = [0.64 0.08 0.18;
           0 0.45 0.74;
           0 0.5 0;
           1 0.84 0;
           0.85 0.7 1];
makerpool = ['^', 'o', 's', 'v', '*'];
for sub = 1:numel(idsub)
    for str = 1:numel(idsub{sub})
        plot(CMC(idsub{sub}(str),1:rankshow),['-' makerpool(str)],'Color',p_color(sub,:));
    end
end

xlim([1 rankshow]);
grid on
xlabel('Rank', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('Matching Rate (%)','FontSize', 12, 'FontWeight', 'bold');
legend (name,'FontSize',10,'Location','southeast');
saveas(gcf,'CMC_iLIDSVID.png');

%% iLIDSVID/PRID
% CMC = [];
name = {
'HistLBP'
'Color&LBP'
'LDFV'
'DFV'
'HOG3D'
'HOG3D+DFV'
'DVR'
'HOG3D+Color&LBP'
'DFV+Color&LBP'
'HOG3D+LDFV'
'DFV+LDFV'
'DFV+HOG3D+Color&LBP'
'DFV+HOG3D+LDFV'
};
idsub = {[1 2 3],[4],[5 6 7],[8 9 10 11],[12 13]}; 
rankshow = 20;

figure,hold on
p_color = [0.64 0.08 0.18;
           0 0.45 0.74;
           0 0.5 0;
           1 0.84 0;
           0.85 0.7 1];
makerpool = ['^', 'o', 's', 'v', '*'];
for sub = 1:numel(idsub)
    for str = 1:numel(idsub{sub})
        plot(CMC(idsub{sub}(str),1:rankshow),['-' makerpool(str)],'Color',p_color(sub,:));
    end
end

xlim([1 rankshow]);
grid on
xlabel('Rank', 'FontSize', 12, 'FontWeight', 'bold');
ylabel('Matching Rate (%)','FontSize', 12, 'FontWeight', 'bold');
legend (name,'FontSize',10,'Location','southeast');
saveas(gcf,'CMC_iLIDSVID.png');
