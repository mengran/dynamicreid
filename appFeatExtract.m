% Appearce feature extraction based on PDollar toolbox.
%%
function F = appFeatExtract(I, numPatch)
camID = [];
nbin = 16;
HOG_short = 0;  % each oritation only has 1 bin
imsz = [128 48; % set the image size.
        91 34;
        64 24]; % For multiscale
gridoverlap = 1;
num_scale = 1;  % multi-scale setting, maximum is 3
fix_numP = 0;   % fix number of patches for each scale, 0: fix size
scale = [1 0.7102 0.5]; 
grid_clip = 1-eps;

switch gridoverlap
    case 0
        maskNum = [6 1; 6 2; 15 5; 10 30]; % number of soft gaussian grid
        numP = [6 12 75 300];
    case 1
        step =flipud([4, 4; 8 8; 16 16;  21 48]); % set the moving step size of the region.
        min_step = [4,4];
        BBoxsz =flipud([8,8; 16 16; 32 32; 22 48]); % set the region size.
        min_box = [8,8];
        numP = [6 14 75];
end
kk = find(numP==numPatch);
% for kk = 1:numel(numP)
%     kk        
    pPyramid = chnsPyramid();
    pPyramid.pChns.shrink = 1;
    pPyramid.nPerOct = 2;
    pPyramid.nApprox = 0;
    pPyramid.pChns.pColor.colorSpace = 'rgb';
    pPyramid.pChns.pGradMag.enabled = 0;
    switch gridoverlap
        case 0
            M = maskNum(kk,:);
            HOG_short = 0;
        case 1 
%             if num_scale > 1
%                 error('cannot support multiscale with overlap feature');
%             end
            for m = 1:num_scale 
                if fix_numP
                    tmpB = floor(max(BBoxsz(kk, :).*scale(m),min_box));
                    tmpStep = floor(max(step(kk, :).*scale(m),min_step));
                else
                    tmpB = BBoxsz(kk, :);
                    tmpStep = step(kk, :);
                end
                [~, BBox, region_mask{m}] =GenerateGridBBox(imsz(m,:), tmpB, tmpStep);
            end
                DataSet.region = region_mask;
                DataSet.BBox =BBox;
    end
    
    
%     
%     pChns = chnsCompute();
%     pChns.shrink = 1; % do not use any shrink
    for i = 1:numel(I)
        if mod(i,round(numel(I)/10))==0
            fprintf('.');
        end

        if iscell(I{i}) % one tracklet for one person
            tmpI = I{i};
            tmpF = cell(numel(tmpI),num_scale);
            for n = 1:numel(tmpI)
                tmp_im = imresize( tmpI{n},imsz(1,:));        
                % get the channel feature         
                pyramid = chnsPyramid( tmp_im, pPyramid );

                for ms = 1:num_scale
                    tmp_data = {};
                    tmp_data_ch = pyramid.data{ms,1};
                    tmp_data{1} = tmp_data_ch(:,:,1:3);                 % RGB
                    tmp_data{2} = rgbConvert(tmp_data{1},'luv',1);      % LUV
                    tmp_data{3} = rgbConvert(tmp_data{1},'hsv',1);      % HSV
                    tmp_data{4} = tmp_data_ch(:,:,4:9);                 % Gradient

        %         tmp_data{1} = single(imresize( im2double(I{i}),imsz)); % RGB
        %         tmp_data{2} = chns.data{1};                            % LUV
        %         tmp_data{3} = rgbConvert(imresize( I{i},imsz),'hsv',1);% HSV
        %         tmp_data{4} = chns.data{3};                            % HOG

                    % clip the gradient channle to fix the maximum value
                    for o = 1:size(tmp_data{4},3)
                        tmp_ch = tmp_data{4}(:,:,o);
                        tmp_ch(tmp_ch > grid_clip) = grid_clip;
                        tmp_data{4}(:,:,o) = tmp_ch;
                    end
                    % get the histogram of the channel feature
                    if ~HOG_short
                        tmp_data = cat(3,tmp_data{:});
                    else
                        tmp_OG = tmp_data{4};
                        tmp_data = cat(3,tmp_data{1:3});
                    end
                    tmp_feat = [];
            %         tmp_feat = zeros(1,nbin*size(tmp_data,3)*numP(kk));
                    switch gridoverlap
                        case 0
                            % non-overlap seperation                
                            for c = 1:size(tmp_data,3)
                                h = histcImLoc(tmp_data(:,:,c),0:1/nbin:1,{M,.6,0,0},[],0);
                                tmp_feat = [tmp_feat h(:)'];
                            end                                            
                        case 1
                            % overlap seperation
                            for c = 1:size(tmp_data,3)
                                for r = 1:size(region_mask{ms},2)
                                    tmp_ch = tmp_data(:,:,c);
                                    h = histc2(tmp_ch(:),0:1/nbin:1,region_mask{ms}(:,r));
                                    tmp_feat = [tmp_feat h(:)'];
                                end
                            end
                            if HOG_short
                                feat_hog_ch = [];
                                feat_hog_p = [];
                                for c = 1:size(tmp_OG,3)
                                    tmp_OG_ch = tmp_OG(:,:,c);
                                    for r = 1:size(region_mask{ms},2)
                                        feat_hog_p(r) = sum(tmp_OG_ch(logical(region_mask{ms}(:,r))));
                                    end
                                    feat_hog_ch(c,:) = feat_hog_p;
                                end
                                feat_hog_ch = bsxfun(@times, feat_hog_ch, 1./sum(feat_hog_ch,1));
                            else
                                feat_hog_ch = [];
                            end
                            tmp_feat = [tmp_feat feat_hog_ch(:)'];
                    end
                    tmpF_traj{ms} = tmp_feat;
                end                
                tmpF(n,:) = tmpF_traj;
            end
            % naive mean 
            % TODO: more intelligent way for appearance model
            for ms = 1:num_scale
                F{ms}(i,:) = mean(cell2mat(tmpF(:,ms)),1);
            end
        else            
            tmp_im = imresize( I{i},imsz(1,:));        
            % get the channel feature         
            pyramid = chnsPyramid( tmp_im, pPyramid );

            for ms = 1:num_scale
                tmp_data = {};
                tmp_data_ch = pyramid.data{ms,1};
                tmp_data{1} = tmp_data_ch(:,:,1:3);                 % RGB
                tmp_data{2} = rgbConvert(tmp_data{1},'luv',1);      % LUV
                tmp_data{3} = rgbConvert(tmp_data{1},'hsv',1);      % HSV
                tmp_data{4} = tmp_data_ch(:,:,4:9);                 % Gradient

    %         tmp_data{1} = single(imresize( im2double(I{i}),imsz)); % RGB
    %         tmp_data{2} = chns.data{1};                            % LUV
    %         tmp_data{3} = rgbConvert(imresize( I{i},imsz),'hsv',1);% HSV
    %         tmp_data{4} = chns.data{3};                            % HOG

                % clip the gradient channle to fix the maximum value
                for o = 1:size(tmp_data{4},3)
                    tmp_ch = tmp_data{4}(:,:,o);
                    tmp_ch(tmp_ch > grid_clip) = grid_clip;
                    tmp_data{4}(:,:,o) = tmp_ch;
                end
                % get the histogram of the channel feature
                if ~HOG_short
                    tmp_data = cat(3,tmp_data{:});
                else
                    tmp_OG = tmp_data{4};
                    tmp_data = cat(3,tmp_data{1:3});
                end
                tmp_feat = [];
        %         tmp_feat = zeros(1,nbin*size(tmp_data,3)*numP(kk));
                switch gridoverlap
                    case 0
                        % non-overlap seperation                
                        for c = 1:size(tmp_data,3)
                            h = histcImLoc(tmp_data(:,:,c),0:1/nbin:1,{M,.6,0,0},[],0);
                            tmp_feat = [tmp_feat h(:)'];
                        end                                            
                    case 1
                        % overlap seperation
                        for c = 1:size(tmp_data,3)
                            for r = 1:size(region_mask{ms},2)
                                tmp_ch = tmp_data(:,:,c);
                                h = histc2(tmp_ch(:),0:1/nbin:1,region_mask{ms}(:,r));
                                tmp_feat = [tmp_feat h(:)'];
                            end
                        end
                        if HOG_short
                            feat_hog_ch = [];
                            feat_hog_p = [];
                            for c = 1:size(tmp_OG,3)
                                tmp_OG_ch = tmp_OG(:,:,c);
                                for r = 1:size(region_mask{ms},2)
                                    feat_hog_p(r) = sum(tmp_OG_ch(logical(region_mask{ms}(:,r))));
                                end
                                feat_hog_ch(c,:) = feat_hog_p;
                            end
                            feat_hog_ch = bsxfun(@times, feat_hog_ch, 1./sum(feat_hog_ch,1));
                        else
                            feat_hog_ch = [];
                        end
                        tmp_feat = [tmp_feat feat_hog_ch(:)'];
                end
                F{ms}(i,:) = tmp_feat;
            end        

        end        
    end
    fprintf('\n%d patch feature extracting DONE!\n',numPatch);
%     DataSet.data = F;
%     DataSet.name = [dataset_name '_HistPD' num2str(numP(kk)) 'Patch']    
%     DataSet.ID = gID;
%     DataSet.camID = camID;
%     if num_scale > 1
%         savename = ['Feature/' DataSet.name '_MS'];
%         if fix_numP
%             savename = [savename '_fixNP'];
%         else
%             savename = [savename '_fixSiz'];
%         end
% %         save(['Feature/' DataSet.name '_MS.mat'], 'DataSet','-v7.3');
%     else
%         savename = ['Feature/' DataSet.name];
%     end
%     if HOG_short
%         savename = [savename, '_HOG6bin'];
%     end        
%     if gridoverlap
%         save([savename '_wOverlap.mat'], 'DataSet','-v7.3');
%     else
%         save([savename '_woOverlap.mat'], 'DataSet','-v7.3');
%     end
end