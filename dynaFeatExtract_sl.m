% dynamic feature extraction
function [F,codebook] = dynaFeatExtract_sl(...
    denseTrj,train,test,gID,camID,endp,startP,imEdge,dynOpt)

% addpath(genpath('C:\Users\Mengran\Dropbox\Research\Code\hankelet-master\hankelet-master'));
% run('C:\Users\mgou\Documents\util\vlfeat-0.9.17\toolbox\vl_setup.m');
if dynOpt.edgeMask
    uCam = unique(camID); % one mask per camera
    for c = 1:numel(uCam)
        idxC = find(camID==uCam(c));
        idxC_train = intersect(train,idxC);
        % filter out background based on edge mask 
        meanEdge = cell2mat(imEdge(idxC_train));
        meanEdge = reshape(meanEdge,128,64,[]);
        meanEdge = mean(meanEdge,3);
        weightEdge{c} = meanEdge./sum(meanEdge(:));

        meanEdge(meanEdge<mean(meanEdge(:)))=0;
        meanEdge(meanEdge>0) = 1;
        se = strel('disk',5,4);
        tmpEdge = imdilate(meanEdge,se);
        tmpEdge([1 128],:) = 0;
        tmpEdge(:,[1 64]) = 0;
        tmpEdge = imfill(tmpEdge,'holes');
        meanEdge = tmpEdge;
        % meanEdge = imerode(tmpEdge,se);
        fgzone = find(meanEdge)';

        for i = 1:numel(idxC)
            tmpendP = endp{idxC(i)};
%             tmpendP = denseTrj{idxC(i)}(:,end-1:end);
            intendP = round(tmpendP);
            intendP(intendP<1) = 1;
            intendP(intendP(:,1)>64,1)=64;
            intendP(intendP(:,2)>128,2)=128;
            endPidx = sub2ind([128 64],round(intendP(:,2)),round(intendP(:,1)));
    %         label_fg = ismember(endPidx,fgzone);

%             tmpstartP = denseTrj{idxC(i)}(:,2:3);
            tmpstartP = startP{idxC(i)};
            intstartP = round(tmpstartP);
            intstartP(intstartP<1) = 1;
            intstartP(intstartP(:,1)>64,1)=64;
            intstartP(intstartP(:,2)>128,2)=128;
            startPidx = sub2ind([128 64],round(intstartP(:,2)),round(intstartP(:,1)));

            label_fg = ismember(endPidx,fgzone) & ismember(startPidx,fgzone);

            weightEndCell{idxC(i)} = weightEdge{c}(endPidx);
            weightStartCell{idxC(i)} = weightEdge{c}(startPidx);
            denseTrj{idxC(i)} = denseTrj{idxC(i)}(label_fg,:);
            endp{idxC(i)} = endp{idxC(i)}(label_fg,:);
            startP{idxC(i)} = startP{idxC(i)}(label_fg,:);
        end
    end
    weightEnd = cell2mat(weightEndCell');
    weightStart = cell2mat(weightStartCell');
    weight = max(weightEnd, weightStart);
else
    [numTrjPerI,~] = cellfun(@size, denseTrj,'UniformOutput',1);
    weight = ones(sum(numTrjPerI),1);
end

% % remove pure horizontal lines
% for i = 1:numel(denseTrj)
%     tmpTrj = denseTrj{i};
%     tmpX = tmpTrj(:,2:2:end);
%     tmpY = tmpTrj(:,3:2:end);
%     velX = tmpX(:,2:end)-tmpX(:,1:end-1);
%     velY = tmpY(:,2:end)-tmpY(:,1:end-1);
%     idremove = sum(abs(velY),2) < 2;
%     stdY = std(tmpY,[],2);
%     idrandom = stdY > 25;
%     denseTrj{i} = tmpTrj(~idremove&~idrandom,:);
%     endp{i} = endp{i}(~idremove&~idrandom,:);
% end
    

% label each trajectory with the gID it belongs to
[numTrjPerI,~] = cellfun(@size, denseTrj,'UniformOutput',1);
index = cumsum(numTrjPerI);
index = [0,index];
label_trajID = zeros(1,index(end));
for i = 1:numel(index)-1
    label_trajID(index(i)+1:index(i+1)) = i;
end
ind_train = ismember(label_trajID,train); % training trajs label

% assign spatial label for each traj based on its ending point position
warning off
nr = dynOpt.nr;
nc = dynOpt.nc;

normSz = [128 64]; % rxc
[~, BBox, ~] = GenerateGridBBox_numP(normSz, nr, nc, dynOpt.PatchOverlap);
BBox = ceil(BBox);
endp = cell2mat(endp');
% startP = cellfun(@(X) X(:,2:3),denseTrj,'uni',false);
startP = cell2mat(startP');
label_strip = zeros(numel(label_trajID),nr*nc);
for g = 1:nr*nc
    xv = [BBox(g,1),BBox(g,1),BBox(g,3),BBox(g,3),BBox(g,1)];
    yv = [BBox(g,2),BBox(g,4),BBox(g,4),BBox(g,2),BBox(g,2)];
%     label_strip(:,g) = inpolygon(endp(:,1),endp(:,2),xv,yv);
    label_strip(:,g) = inpolygon(startP(:,1),startP(:,2),xv,yv);
end
label_strip = logical(label_strip);
num_strip = size(label_strip,2);


%% Normalization and sliding window partition

tmpTrj = cell2mat(denseTrj');
tmpTrj = tmpTrj(:,2:end);

% calculate the normalized speed
tmpX = tmpTrj(:,1:2:end);
tmpY = tmpTrj(:,2:2:end);
velX = tmpX(:,2:end)-tmpX(:,1:end-1);
velY = tmpY(:,2:end)-tmpY(:,1:end-1);
% normfact = sqrt(velX.^2 + velY.^2);
% normfact = sum(normfact,2);
% velX = bsxfun(@times,velX,1./normfact);
% velY = bsxfun(@times,velY,1./normfact);

% sliding window augment feature
clear tmpTrj tmpX tmpY
sl_win_m = dynOpt.sl;
if 1 % given the length of the short trajs, fully overlapped
    sl_win_m = dynOpt.sl;
    for sl = 1:numel(sl_win_m) % loop on different length
        sl_win = sl_win_m(sl);
        tmpTrj{sl} = zeros(size(velX,1)*(size(velX,2)-sl_win+1),sl_win*2,'single');
        label_sl{sl} = [];
        for slw = 1:size(velX,2)-sl_win+1 % loop on slides
            tmpTrj{sl}((slw-1)*size(velX,1)+1:slw*size(velX,1),1:2:end) = velX(:,slw:slw+sl_win-1);
            tmpTrj{sl}((slw-1)*size(velX,1)+1:slw*size(velX,1),2:2:end) = velY(:,slw:slw+sl_win-1);
            label_sl{sl} = [label_sl{sl}, slw*ones(1,size(velX,1))];
        end
        label_trajID_sl{sl} = repmat(label_trajID,1,size(velX,2)-sl_win+1);
        label_strip_sl{sl} = repmat(label_strip,size(velX,2)-sl_win+1,1);
        ind_train_sl{sl} = repmat(ind_train,1,size(velX,2)-sl_win+1);
        weight_sl{sl} = repmat(weight,size(velX,2)-sl_win+1,1);
    end
else % givin the numver of number segments per layer, no overlapping
    sh_num = dynOpt.sl;
    for sh = 1:numel(sh_num) % loop on different layers
        shn = sh_num(sh);
        sl_win = floor(size(velX,2)/shn);
        curP = 1;
        tmpTrj{sh} = zeros(size(velX,1)*shn,sl_win*2,'single');
        label_sl{sh} = [];
        for tmplabel = 1:shn % loop on segments
            tmpTrj{sh}((tmplabel-1)*size(velX,1)+1:tmplabel*size(velX,1),1:2:end) = velX(:,curP:curP+sl_win-1);
            tmpTrj{sh}((tmplabel-1)*size(velX,1)+1:tmplabel*size(velX,1),2:2:end) = velY(:,curP:curP+sl_win-1);
            label_sl{sh} = [label_sl{sh}, tmplabel*ones(1,size(velX,1))];
            curP = curP+sl_win;
        end
        label_trajID_sl{sh} = repmat(label_trajID,1,shn);
        label_strip_sl{sh} = repmat(label_strip,1,shn);
        ind_train_sl{sh} = repmat(ind_train,1,shn);
    end
end

% switch dynOpt.RP
for sl = 1:numel(tmpTrj)
    tmpfeature = tmpTrj{sl}';
    feature{sl} = tmpfeature;
    %                     feature{sl} = normc_safe(tmpfeature);
end
clear tmpTrj;

%%      OPTIONAL: Whitening PCA (seems down grade the performance)
%         % feature is colomn wise
%         if ~iscell(feature)
%             feature = {feature};
%         end
%         for c = 1:numel(feature)
%             feature{c} = whitening(feature{c});
% % %             feature = feature(1:round(size(feature,1)/2),:); % only keep partial feature
%         end
%%      Build GMM
% EM, learning GMM model for each strip/patch
disp('Begin to extract FV...')
LL = [];
for s = 1:num_strip
    for sl = 1:numel(sl_win_m)
        switch dynOpt.slfashion
            case 1 %each sl window size bulid one FV (hankelize);
                feature_tmp = feature{sl}(:,ind_train_sl{sl} & label_strip_sl{sl}(:,s)');
                subind = randsample(size(feature_tmp,2),min(size(feature_tmp,2),50000)); % subsample to speed up
                feature_train = feature_tmp(:,subind);
                if size(feature_train,2) < dynOpt.ncenter
                    continue;
                end
                [means{sl}{s}, covariances{sl}{s}, priors{sl}{s},LL(sl,s),~] = vl_gmm(feature_train,dynOpt.ncenter,'NumRepetitions',1);
%                 [inimean,U] = fcm(feature_train',dynOpt.ncenter,[1.5,100,NaN,0]);
%                 inipri = single(sum(U,2)/sum(sum(U,2)));
%                 [means{sl}{s}, covariances{sl}{s}, priors{sl}{s},LL(sl,s),~] = vl_gmm(feature_train,dynOpt.ncenter,'InitMeans',inimean',...
%                     'InitPriors',inipri', 'InitCovariances', single(1e-3*ones(size(inimean'))),'NumRepetitions',1);
            case 2 %each sl bulid one FV
                for slw = 1:max(label_sl{sl})
                    feature_tmp = feature{sl}(:,ind_train_sl{sl} & label_strip_sl{sl}(:,s)' & label_sl{sl}==slw);
                    subind = randsample(size(feature_tmp,2),min(size(feature_tmp,2),50000)); % subsample to speed up
                    feature_train = feature_tmp(:,subind);
                    [means{sl}{s}{slw}, covariances{sl}{s}{slw}, priors{sl}{s}{slw},~,~] = vl_gmm(feature_train,dynOpt.ncenter,'NumRepetitions',10);
                end
        end
    end
end
% display(mean(LL(:)));
%%      Binlingual Denoise (not use)
% if dynOpt.thBiDiff > 0
%     gID_train = gID(train);
%     camID_train = camID(train);
%     uID = unique(gID_train);
%     diffPrior = cell(1,numel(uID));
%     meanPrior = cell(1,numel(uID));
%     for i = 1:numel(uID)
%         id = uID(i);
%         tmpPair = find(gID==id);
%         tmpPrior = cell(1,2);
%         for p = 1:2
%             tmpFeature = feature(:,label_trajID==tmpPair(p));
%             % compute gaussian priors
%             tmpPrior{p} = zeros(num_strip,dynOpt.ncenter);
%             for s = 1:num_strip
%                 auxDis = zeros(size(tmpFeature,2),dynOpt.ncenter);
%                 for c = 1:dynOpt.ncenter
%                     auxDis(:,c) = priors{s}(c).*normpdf2(tmpFeature,means{s}(:,c),diag(covariances{s}(:,c)));
%                 end
%                 %                 auxDis(isnan(auxDis)) = 0;
%                 auxDis = bsxfun(@times,auxDis,1./sum(auxDis,2));
%                 auxDis(isnan(auxDis)) = 0;
%                 auxDis(auxDis == inf) = realmax;
%                 tmpPrior{p}(s,:) = sum(auxDis)./sum(auxDis(:));
%             end
%         end
%         diffPrior{i} = abs(tmpPrior{1} - tmpPrior{2});
%         meanPrior{i} = (tmpPrior{1} + tmpPrior{2})/2;
%     end
%     %             load('prior_w200_s6_vel.mat');
%     matdiffPrior = cell2mat(diffPrior);
%     matmeanPrior = cell2mat(meanPrior);
%     diffMat = zeros(num_strip,dynOpt.ncenter);
%     meanMat = zeros(num_strip,dynOpt.ncenter);
%     zeroCount = zeros(num_strip,dynOpt.ncenter);
%     for s = 1:num_strip
%         for w = 1:dynOpt.ncenter
%             tmpsw = matdiffPrior(s,w:dynOpt.ncenter:end);
%             zeroCount(s,w) = sum(tmpsw>dynOpt.thBiDiff);
%             diffMat(s,w) = mean(tmpsw);
%             tmpsw = matmeanPrior(s,w:dynOpt.ncenter:end);
%             meanMat(s,w) = mean(tmpsw);
%         end
%     end
%     
%     label_bilingual_mat = ones(size(diffMat));
%     for s = 1:num_strip
%         [~,tmpidx] = sort(diffMat(s,:));
%         label_bilingual_mat(s,tmpidx(1:dynOpt.thBiDiff))=0;
%     end
%     %             label_bilingual_mat = diffMat < dynOpt.thBiDiff; %& meanMat > dynOpt.thBiMean;
% end
%% Fisher vector encoding
for i = 1:numel(denseTrj)
    clear tmpF;
    switch dynOpt.slfashion
        case 1
            for sl = 1:numel(sl_win_m)
                tmpF{sl} = zeros(dynOpt.ncenter*size(feature{sl},1)*2,num_strip);
            end
            for s = 1:num_strip
                for sl = 1:numel(sl_win_m)
                    tmpFeature = feature{sl}(:, label_trajID_sl{sl}==i & label_strip_sl{sl}(:,s)');
                    if size(tmpFeature,2) < dynOpt.ncenter
                        continue;
                    end
                    % encoding
                    tmpF{sl}(:,s) = vl_fisher(tmpFeature, means{sl}{s}, covariances{sl}{s}, priors{sl}{s},'Normalized','SquareRoot');
%                     % weighted FV with edge mask
%                     tmpWeight = weight_sl{sl}(label_trajID_sl{sl}==i & label_strip_sl{sl}(:,s)');
%                     tmpf = zeros(size(tmpF{sl},1),size(tmpFeature,2));
%                     for f = 1:size(tmpFeature,2)
%                         tmpf(:,f) = vl_fisher(tmpFeature(:,f), means{sl}{s}, covariances{sl}{s}, priors{sl}{s},'Normalized','SquareRoot');
%                     end
%                     tmpf = bsxfun(@times, tmpf,tmpWeight');
%                     tmpF{sl}(:,s) = sum(tmpf,2)./sum(tmpWeight);
                end
            end
%             % Normalize
%             for sl = 1:numel(sl_win_m)
%                 tmpF{sl} = powerNormalization(tmpF{sl}); % power
%                 tmpF{sl} = normc_safe(tmpF{sl});
%             end
        case 2
            for sl = 1:numel(sl_win_m)
                tmpF{sl} = zeros(max(label_sl{sl})*dynOpt.ncenter*size(feature{sl},1)*2,num_strip);
            end
            for s = 1:num_strip
                for sl = 1:numel(sl_win_m)
                    tmpF_sl = zeros(dynOpt.ncenter*size(feature{sl},1)*2,max(label_sl{sl}));
                    for slw = 1:max(label_sl{sl}) % for every sliding window
                        tmpFeature = feature{sl}(:,label_trajID_sl{sl}==i & label_strip_sl{sl}(:,s)' & ...
                            label_sl{sl}==slw);
                        % encoding
                        tmpF_sl(:,slw) = vl_fisher(tmpFeature, means{sl}{s}{slw}, covariances{sl}{s}{slw}, priors{sl}{s}{slw},'Normalized','SquareRoot');
                    end
%                     tmpF_sl = powerNormalization(tmpF_sl);
%                     tmpF_sl = normc_safe(tmpF_sl);
                    tmpF{sl}(:,s) = tmpF_sl(:);
                end
                
            end
    end
    tmpF = cell2mat(tmpF');
%     if dynOpt.thBiDiff > 0
%         %throw away the non-bilingal gaussian
%         nonBi = find(label_bilingual_mat(s,:)==0);
%         idxrm = [];
%         for nb = 1:numel(nonBi)
%             tmpidxrm = [size(feature,1)*(nonBi(nb)-1)+1:size(feature,1)*nonBi(nb)];
%             tmpidxrm = [tmpidxrm, tmpidxrm+size(feature,1)*dynOpt.ncenter];
%             idxrm = [idxrm, tmpidxrm];
%         end
%         tmpF(idxrm,:) = [];
%     end
    F(i,:) = tmpF(:);
end
codebook.means = means;
codebook.cov = covariances;
codebook.prior = priors;
end

function X = powerNormalization(X)

X = sign(X).*(abs(X).^0.5);

end


function featW = whitening(feat)
feat = feat';
feat = bsxfun(@minus, feat,mean(feat,2)); % remove mean
[pcaEigVec,pcaCenter,pcaEigVal] = pca(feat); % pca
featW = diag(pcaEigVal.^-0.5)*pcaEigVec'*feat; % whitening
featW = featW(1:round(size(featW,1)/2),:); % only keep partial feature
featW = featW';
end
